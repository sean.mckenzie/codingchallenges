package main

data class Location(val id: Int, val name: String) {
    fun getAvailableSpace(bookingDate:String, locationRepository: LocationRepository ):LocationOnADate {
        val availableSpace = locationRepository.spacesAtLocation(this, bookingDate)
        val uniqueSpaces = availableSpace.spaces.distinct()
        return LocationOnADate(this, bookingDate, uniqueSpaces)
    }
}

data class LocationOnADate(val location:Location, val bookingDate:String, val spaces:List<Space>) {
    fun getAvailableSlots(space:Space, bookingRepository: BookingRepository):List<AvailableSlot> = bookingRepository.availableSlots(location, bookingDate, space).filter{ availableSlot -> availableSlot.quantity > 0 }

    fun getSlotsForEachSpace(bookingRepository: BookingRepository):List<AvailableSpace> =
        spaces.map { space ->
            AvailableSpace( space, getAvailableSlots(space, bookingRepository))
        }.filter{availableSpace -> availableSpace.availableSlots.size > 0}

    fun contains(space:Space)  = spaces.contains(space)
}

class AvailableSpace(val space:Space, val availableSlots:List<AvailableSlot> )

data class Space(val name:String, val capacity:Int) {
    fun canTake(visitors: List<Person>)  = (visitors.size <= capacity)
}

data class Slot(val description:String) {
    fun isAvailable(availableSlots: List<AvailableSlot>) = availableSlots.filter{it.quantity > 0}.map{it.slot}.contains(this)
}

data class AvailableSlot(val slot:Slot, val quantity:Int)

fun getLocations(locationRepository: LocationRepository):List<Location> = locationRepository.locations()

sealed class Person(val name:String, val emailAddress: String) {
    class Partner(name: String, emailAddress: String, val employeeNumber:Int):Person(name, emailAddress)
    class Attendee(name: String, emailAddress: String):Person(name, emailAddress)
    class ExternalBooker(name: String, emailAddress: String, val contactDetails:String):Person(name, emailAddress)
}
data class Booking(val reference:Int, val location:Location, val bookingDate:String, val space:Space, val slots:List<Slot>, val booker:Person, val vistiors:List<Person>)

sealed class BookingResult { //TODO giving a value to each type and error handling
    object NotAPartnerOrExternalBooker:BookingResult()
    object SpaceNotAvailableAtTheLocation:BookingResult()
    object SlotNotAvailableForTheSpace:BookingResult()
    object TooManyVisitorsForTheSpace:BookingResult()
    class Booked(val booking: Booking):BookingResult()
}

fun makeBooking(locationOnADate: LocationOnADate, space: Space, slots: List<Slot>, booker: Person, bookingRepository: BookingRepository, visitors: List<Person> = listOf(booker)
):BookingResult {
    if (booker !is Person.Partner && booker !is Person.ExternalBooker ) return BookingResult.NotAPartnerOrExternalBooker
    if (!space.canTake(visitors)) return  BookingResult.TooManyVisitorsForTheSpace

    if (!locationOnADate.contains(space)) return BookingResult.SpaceNotAvailableAtTheLocation

    val availableSlots = locationOnADate.getAvailableSlots(space, bookingRepository)

    slots.forEach { slot -> if (!slot.isAvailable( availableSlots)) return BookingResult.SlotNotAvailableForTheSpace  }

    return BookingResult.Booked(requestBooking(locationOnADate, space, slots, booker, bookingRepository, visitors))
}

private fun requestBooking(locationOnADate: LocationOnADate, space: Space, slots: List<Slot>, booker: Person, bookingRepository: BookingRepository, visitors: List<Person>):Booking {
    val reference = bookingRepository.getNextReference()
    val booking = Booking(reference, locationOnADate.location, locationOnADate.bookingDate, space, slots, booker, visitors )
    bookingRepository.saveBooking(booking)
    return booking
}

interface LocationRepository { //TODO error handling
    fun locations():List<Location>
    fun spacesAtLocation(location:Location, bookingDate:String):LocationOnADate
}

interface BookingRepository { //TODO error handling
    fun availableSlots(location:Location, bookingDate:String, space:Space):List<AvailableSlot>
    fun getNextReference():Int
    fun saveBooking(booking:Booking)
}

/*
Challenge 30 - Mike N solution

This is the domain model for reserving rooms or desks in any Partnership building. I tried to use a DDD type approach.

After much experimentation I decided the Bounded Context was the customer reservation rather than the whole room and desk management system.

This resulted in these principle classes:

Location : Each Partnership Location where people can reserve a space. Just contains an id and description.

LocatinForADate: Contains each space in a Location on a particular date

Space: This is the specification for a space. It contains a string and the amount available. It does not describe the actual physical entity so has no id.

Slot: This is a time slot, but its just a description. I originally gave it an id but it served no purpose in the model although probably is needed to allow for the booking system changing the text.

Booking: This describes the reservation. Maybe it should be called Reservation! Customers can reserve a space (e.g. Room for 8 people) and one or more time slots (e.g. "9am - 1pm" and "1pm -5pm") for a date/location.

A booking does not indicate which particular room or desk has been earmarked for the customer. It is assumed that is in a different Bounded Context, or maybe when the people turn up someone just walks them to an empty desk or room of the right specifacation.

The functions that make the model useful are:

getLocations() - does what it says

Location.getAvailableSpace() - gets a LocationForADate which contains all the spaces in a location on a particular date.

LocationOnADate.getAvailableSlots(space) - gets the slots that can be reserved for a particular space for a LocationDate

LocationOnADate.getSlotsForEachSpace() - gets every possible slot for every space for a LocationOnADate

makeBooking(location, date, space, customer etc.) - makes a reservation for a customer. Maybe should be part of LocationOnADate.

I've ignored the "desk bank" facility as that seems optional. I don't think it would be problematic adding that to the model.

The model defines interfaces for repositories that would contain information about locations and bookings.

The model ignores error handling!
 */