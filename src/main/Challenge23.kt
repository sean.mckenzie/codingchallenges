package main

import kotlin.random.Random

typealias Grid = Array<String>

//fun chunkArray(input: Array<String>) = input.//.chunk("\n").filter { it.isNotEmpty() }.toTypedArray()

data class StoreDetails(
    val store: String,
    val postcode: String,
    val lng: Double?,
    val lat: Double?
)
val endGrid: Grid  = arrayOf(
    "D...A",
    ".....",
    ".....",
    ".....",
    "C...B")

val fullGrid: Grid  = arrayOf(
    "AXYZB",
    "Z.Y.X",
    "YZ.ZY",
    "X.Z.Z",
    "DZYXC")

fun processGrid(addWords: Int, rawList: String, gridRows: Int, gridCols: Int): Grid {
    val grid = emptyGrid(gridRows, gridCols)
    val randomStores = selectRandomStoreNames(rawList, addWords)
    val xyzTimes8 = listOf("XYZ","XYZ","XYZ","XYZ","XYZ","XYZ").toMutableList()
//    val xyzTimes8 = listOf("ABC","DEF","GHI","JKL","MNO","PQR","STU","VWX").toMutableList()
    
    
    var workingGrid = endGrid
    var count = 0

    while (xyzTimes8.count() > 0 && count < 50) {
        println("xyz count = " +xyzTimes8.count())
        val xyzcount = xyzTimes8.count()
        if (xyzTimes8.count() > 0) {
            var notFoundHorizontal = true
            for (i in 0..workingGrid.lastIndex) {
                if  (xyzTimes8.count() == xyzcount) notFoundHorizontal = true
                println("horizontal row= $i")

                while (notFoundHorizontal) {
                    if (workingGrid[i].contains(".")) {
                        println("horizontal add = " + workingGrid[i])
                        val wordFitsCol = doesWordFit(workingGrid[i], xyzTimes8[0])
                        if (wordFitsCol != -1) {
                            workingGrid = replaceHorizontal(workingGrid, xyzTimes8[0], i, wordFitsCol)
                            println(workingGrid[i])
                            xyzTimes8.removeAt(0)
                            notFoundHorizontal = false
                        }
                    } else notFoundHorizontal = false
                }
            }
        }

        if (xyzTimes8.count() > 0) {
            var notFoundDiagonal = true
            var count = 0
//        while (notFoundDiagonal) {

            for (row in 0..workingGrid.lastIndex) {
                for (col in 0..workingGrid[0].length - 1) {
//                while (notFoundDiagonal && count < 20) {
                    println("row = $row   col = $col count = $count")

                    val diagonal = diagonalStrings(workingGrid, row, col)
                    println("check the diagonal string = $diagonal")

                    val wordFitsDiag = doesWordFit(diagonal, xyzTimes8[0])
                    println("wor fit = $wordFitsDiag ")

//                    if (wordFitsDiag != -1) {
//                        workingGrid = replaceDiagonal(workingGrid, xyzTimes8[0], row + wordFitsDiag, col+ wordFitsDiag)
//                        println("success on replace diag")
//                        xyzTimes8.removeAt(0)
//                        notFoundDiagonal = false
//                    } else count++
//                }
                }
            }
        }
        
        workingGrid.forEach { println(it) }
//        xyzTimes8.removeAt(0)
        workingGrid = workingGrid.rotateClockwise()
        count++
    }
        
    
    
    
    
    //until all words applied
    
    //get empty grid
    //find horizontal strings = Grid
    //find where word fits
    //apply word = new grid
    //stop if all words applied
    
    //find diagonal strings
    //find where word fits
    //apply word = new grid
    //stop if all words applied
    //rotate grid
    
    // once all words applied return grid
    println("returning")
    workingGrid.forEach { println(it) }

    return workingGrid
}


fun splitStoreDetails(string: String): List<StoreDetails> =
    string.split(",").chunked(4).map { it }.map { x ->
        StoreDetails(x[0], x[1], x[2].toDoubleOrNull(), x[3].toDoubleOrNull())
    }

fun selectRandomStoreNames(listStores: String, select: Int) : List<String>{
    val splitStores = splitStoreDetails(listStores)
    val regex = Regex("[^A-Za-z ]")
    val output = emptyList<String>().toMutableList()

    for (i in 1..select) {
        var keepTrying = true
        while (keepTrying) {
            val randomStoreNumber: Int = Random.nextInt(0, splitStores.size)
            var store = splitStores[randomStoreNumber].store
            store = regex
                .replace(store, "")
                .toUpperCase()
                .replace(" ", "")
                .replace("JOHNLEWIS","")
                .replace("JLFOODHALL","")
                .replace("PETROLSTATION","")
                .replace("PETROLFILLINGSTATION","")
                .replace("LITTLEWAITROSEAT","")
                .replace("COOKERYSCHOOL","")
                .replace("ROAD","")
                .replace("SERVICES","")
                .replace("LONDON","")
                .replace("ATHOME","")

            if (output.contains(store)) {
                println("already have $store")
            } else if (store.length >= 14) {
                println("store name too long $store")
            } else {
                output.add(store)
                keepTrying = false
            }
        }
    }

    output.forEach {
        if (it.length >= 14) println(it+it.length)
    }
    return output
}

fun emptyGrid(across: Int, down: Int): Grid { //MutableList<MutableList<String>> {
    fun emptyLine() = ".".repeat(across)//(1..across).map { "." }.toTypedArray()
    val grid = (1..down).map { emptyLine() }.toTypedArray()

//    fun blankLine() = (1..across*down).map { "." }.toTypedArray()

    return grid
    }

fun Grid.rotateClockwise(): Grid {
    val rowLength = this[0].length
    return (0 until rowLength).map { col ->
        this.map { it[col] }.joinToString(separator = "").reversed()
    }.toTypedArray()
}

fun diagonalStrings(grid: Grid, row: Int, col: Int): String {
    val lastIndex = grid.lastIndex
    var output = ""
    var column = col
    for (i in row..lastIndex) {
        println("index = $row  lastIndex= $lastIndex i = $i output = $output  grid[i] = ${grid[i]} column = $column")
        if (column <= lastIndex) {
            output += grid[i].get(column)
            column++
        }
    }
    return output
}


fun doesWordFit(lineFromGrid: String, word: String): Int {
    val countDots = lineFromGrid.count{ ".".contains(it) }
    val gridLineChars = lineFromGrid.toCharArray()
    val wordChars = word.toCharArray()
    val maxValidPositionIndex = lineFromGrid.length - word.length //10 letter grid - 8 letter word = max index of 2  012...3456789

    println("gridLine = $lineFromGrid ${lineFromGrid.length}  word = $word ${word.length}  countDots = $countDots maxIndex = $maxValidPositionIndex ")

    for (i in 0..maxValidPositionIndex) { // 012...3456789  i.e. for 0,1,2 try to match the word 0,1,2 etc
        var rememberIndex = i
        var gridCount = i
        var wordCount = 0

        while (wordCount < word.length) {
            println("gridLineChars[gridCount] = ${gridLineChars[gridCount]} wordChars[wordCount] = ${wordChars[wordCount]} wordCount=$wordCount gridCount=$gridCount")
            if (gridLineChars[gridCount] != wordChars[wordCount] && gridLineChars[gridCount].toString() != ".") {
//                println("stop checking....clash")
                wordCount = word.length
            } else {
//                println("match....carry on")
                gridCount++
                wordCount++
//                println("wordCount:$wordCount == word.length:${word.length} ${wordCount == word.length}")
                if (wordCount == word.length) {
                    println("XXX success word fits starting at GridLine index=$rememberIndex")
                    return rememberIndex
                }
            }
        }
    }

    println("does not fit")
    return -1
}

fun replaceHorizontal(startGrid: Grid, word: String, row: Int, col: Int): Grid {
    val grid = mutableListOf<String>()
    var count = 0
    
    startGrid.forEach {line ->
        var newline = ""
        if (count == row) {
            try {
                newline = line.substring(0, col) + word + line.substring(col + word.length)
            } catch(e: Exception) {
                println("error replacing row $row = $line with word = $word in col $col")
                newline = line
            }
        } else {
            newline = line    
        }
        grid.add(newline)
        count++
    }
    
    return grid.toTypedArray()
}

fun replaceDiagonal(startGrid: Grid, word: String, row: Int, col: Int): Grid {
    val grid = mutableListOf<String>()
    var count = 0
    var wordReplace = false
    var wordCount = 0
    println("\nbuilding:")

    startGrid.forEach {line ->
//        println("count $count; line = $line word = $word in col $col wordreplace = $wordReplace  wordcount = $wordCount")

        var newline = ""
        if (count == row) wordReplace = true

        if (wordReplace) {
            try {
                newline = line.substring(0, col + wordCount) + word.substring(wordCount,wordCount+1) + line.substring(col + wordCount + 1)
                if (wordCount == word.length - 1) wordReplace = false else wordCount++

            } catch(e: Exception) {
                println("error replacing row $count = $line with word = $word in col $col")
                newline = line
            }
        } else {
            newline = line
        }
        println(newline)
        grid.add(newline)
        count++
    }
    println("\nreturn:")
    grid.forEach { println(it) }

    return grid.toTypedArray()
}

//
////This is the code to create random puzzle.
//const val WIDTH = 14
//const val SIZE = WIDTH * WIDTH
//val RANDOMLETTERS = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ".toList().map{it.toString()}[(0..25).random()]}
//
//typealias WordSearchGrid = MutableMap<Position, String>
//
//data class Position(val x:Int, val y:Int) {
//
//    val  isValid = (x in 0 until WIDTH && y in 0 until WIDTH)
//
//    operator fun plus(other:Position) = Position(x + other.x, y + other.y)
//    operator fun times(num:Int) = Position(x * num, y * num)
//
//    companion object {
//        fun inRandomOrder(randomGenerator: List<Int> = listOf()):List<Position> {
//            val positions = (0 until SIZE).map{it.asPosition}
//            return if (randomGenerator.isNotEmpty()) randomGenerator.map{positions[it]} else (0 until SIZE).shuffled().map{positions[it]}
//        }
//    }
//}
//
//val Int.asPosition:Position get() = Position(this % WIDTH, this / WIDTH)
//
//enum class Direction(val step:Position) {
//    HorizontalRight(Position(1,0)),
//    HorizontalLeft(Position(-1,0)),
//    VerticalDown(Position(0,1)),
//    VerticalUp(Position(0,-1)),
//    DiagonalDownRight(Position(1,1)),
//    DiagonalDownLeft(Position(-1,1)),
//    DiagonalUpRight(Position(1,-1)),
//    DiagonalUpLeft(Position(-1,-1));
//
//    companion object {
//        fun inRandomOrder(randomGenerator: List<Int> = listOf()) =
//            if (randomGenerator.isNotEmpty()) randomGenerator.map { values()[it] } else (0 until values().size).shuffled().map {values()[it] }
//    }
//}
//
//fun parseIntoClues(locationString: String): List<String> {
//    return locationString.split(",")
//        .chunked(4)
//        .map{it[0].replace(" ","")}
//        .filter{it.isNotEmpty() && it.length <= 10 } //not part of the spec but only using location names with length of ten or less
//        .map{it.toUpperCase()}
//}
//
//fun WordSearchGrid.clueConflictsWithGrid(clue:String, position:Position, direction:Direction) =
//    clue.toList()
//        .mapIndexed{ndx, letter ->
//            val positionOnGrid = position + (direction.step * ndx)
//            Pair(positionOnGrid,letter.toString())}
//        .any{(position,letter) ->
//            containsKey(position)  && this[position] != letter  }
//
//fun WordSearchGrid.addClue(clue:String, position:Position, direction:Direction) =
//    clue.toList()
//        .forEachIndexed { ndx, letter ->
//            val positionOnGrid = position + (direction.step * ndx)
//            this[positionOnGrid] = letter.toString()}
//
//fun String.willFit(startPosition: Position, direction: Direction): Boolean {
//    val endPosition= startPosition + (direction.step * (length - 1))
//    return startPosition.isValid && endPosition.isValid
//}
//
//fun WordSearchGrid.placeClueOnGrid(clue:String, positions:List<Position>? = null, directions:List<Direction>? = null) {
//    val randomPositions = positions ?: Position.inRandomOrder()
//    val randomDirections = directions ?: Direction.inRandomOrder()
//
//    val validPositionsAndDirections = randomPositions.asSequence().flatMap { position ->
//        randomDirections.asSequence().mapNotNull { direction ->
//            if (clue.willFit(position, direction)) {
//                if (clueConflictsWithGrid(clue, position, direction)) null else Pair(position, direction)
//            } else null
//        }
//    }.take(1)
//    if (validPositionsAndDirections.toList().isNotEmpty()) {
//        val (validPosition, validDirection) = validPositionsAndDirections.first()
//        addClue(clue, validPosition, validDirection)
//    }
//}
//
//fun WordSearchGrid.placeCluesOnGrid(listOfClues:List<String>, positions:List<Position>? = null, directions:List<Direction>? = null) {
//    listOfClues.forEach { clue ->
//        placeClueOnGrid(clue, positions, directions)
//    }
//}
//
//fun createPuzzle(locations: String, randomLetter:()->String = RANDOMLETTERS):Pair<List<String>,List<String>> {
//    val wordSeachGrid:WordSearchGrid = mutableMapOf()
//    val clues = parseIntoClues(locations).shuffled().take(10)
//    wordSeachGrid.placeCluesOnGrid(clues)
//    val puzzle = (0 until SIZE).map { wordSeachGrid[it.asPosition] ?: randomLetter()}
//    return Pair(puzzle, clues)
//}
//
//fun printResult(listOfLetters:List<String>, clues:List<String>){
//    println("Puzzle and clues")
//    println("----------------")
//    val rows = listOfLetters.chunked(WIDTH)
//    rows.forEach { row -> println(row.joinToString("")) }
//
//    println("\n Clues:")
//    println(clues)
//
//}