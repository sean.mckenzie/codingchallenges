package main

fun checkPontoonWinner(playerCards: Array<String>, dealerCards: Array<String>): String {
    val (playerScore, playerHand) = processCardsForScoreAndHandType(playerCards)
    val (dealerScore, dealerHand) = processCardsForScoreAndHandType(dealerCards)

    val resultWinner = confirmWinner(playerScore, playerHand, dealerScore, dealerHand)
    val resultDetail =
        """:
            Player cards: ${playerCards.joinToString(",")}  
            Score: $playerScore  
            Hand: $playerHand 
            - vs - 
            Dealer cards: ${dealerCards.joinToString(",")}  
            Score: $dealerScore  
            Hand: $dealerHand
        """

    return resultWinner + resultDetail
}

fun confirmWinner(playerScore: Int, playerHand: String, dealerScore: Int, dealerHand: String): String {
    return when {
        playerHand == "invalid hand" -> "The dealer wins"
        dealerHand == "invalid hand" -> "The player wins"
        playerHand == "bust" -> "The dealer wins"
        dealerHand == "bust" -> "The player wins"
        playerHand == "pontoon" && dealerHand == "pontoon" -> "The dealer wins"
        playerHand == "five card trick" && dealerHand == "five card trick" -> "The dealer wins"
        dealerHand == "pontoon" -> "The dealer wins"
        playerHand == "pontoon" -> "The player wins"
        dealerHand == "five card trick" -> "The dealer wins"
        playerHand == "five card trick" -> "The player wins"
        playerScore <= dealerScore -> "The dealer wins"
        else -> "The player wins"
    }
}

fun processCardsForScoreAndHandType(cards: Array<String>): Pair<Int, String> {
    var total = 0
    var numberOfAces = 0
    var numberOfPictures = 0
    var invalidHand = 0

    cards.forEach {
        val value = valueOfCard(it.take(1))
        total += value
        //check for aces
        if (value == 1) numberOfAces += 1
        //check for picture cards
        if (value == 10 && it.take(1) != "T") numberOfPictures += 1
        // check if any cards were invalid
        val isValidSuit: Boolean = it.substring(1, 2) in arrayOf("H", "C", "D", "S")
        if (value == 0 ||
            it.length != 2 ||
            isValidSuit.not()
        ) invalidHand += 1
    }
    // if cards contained an ace - check if it can be high
    if (numberOfAces > 0 && total < 12) total += 10

    // define the hand
    val hand = when {
        invalidHand > 0 -> "invalid hand"
        total > 21 -> "bust"
        total == 21 && numberOfAces == 1 && numberOfPictures == 1 -> "pontoon"
        cards.size == 5 -> "five card trick"
        else -> "score"
    }

    return Pair(total, hand)
}

fun valueOfCard(card: String): Int {
    return when {
        card == "A" -> 1
        card == "T" -> 10
        card == "J" -> 10
        card == "Q" -> 10
        card == "K" -> 10
        else -> cardNumberValue(card)
    }
}

fun cardNumberValue(card: String): Int {
    var value = 0
    try {
        value = card.toInt()
    } catch (e: Exception) {
        println("$e: " + card + " is not a valid card")
    }
    return value
}
