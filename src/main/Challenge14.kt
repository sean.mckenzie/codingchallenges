package main

fun integerFold(input: List<Int>, acc: Int, function: (Int, Int) -> Int): Int {
    var result = acc
    for (i in input) {
        result = function(result, i)
    }
    return result
}

fun myFold(input: List<Int>, acc: String, function: (String, Int) -> String): String {
    var result = acc
    input.forEach {
        result.run { result = function(result, it) }
    }
    return result
}


fun <InputType, OutputType> myFold(input: List<InputType>, acc: OutputType, function: (OutputType, InputType) -> OutputType): OutputType {
    var result = acc
    input.forEach {
        result.run { result = function(result, it) }
    }
    return result
}

