package main

import java.util.concurrent.TimeUnit
import kotlin.math.*

data class Store(
    val store: String,
    val postcode: String,
    val lng: Double?,
    val lat: Double?,
    val distance: Double?
)

const val hourInSeconds = 60 * 60
const val dayInSeconds = 24 * hourInSeconds
const val startLat: Double = -0.141499
const val startLong: Double = 51.496466
const val timeSpentInStoreInSeconds = 20 * 60
const val totalTimeAvailableInADayInSeconds = 8 * hourInSeconds
const val averageSpeedMph = 30

fun calculateJourneyTime(fullStoreList: String): Int {
    val invalidAllStores = nullStores(splitStores(fullStoreList))
    println("Cannot include the following: " + invalidAllStores.count() + " stores")
    println(invalidAllStores.toString())
    println()
    println("Itinerary for the following Valid Stores")
    val finalAllStores = returnStoreDistancesFile(startLat, startLong, validStores(splitStores(fullStoreList)))

    val answer = journeyTime(finalAllStores)
    println()
    println("Final Time in seconds: $answer for Store Count: " + finalAllStores.count())

    return answer
}


fun journeyTime(finalAllStores: List<Store>): Int {
    //Reset all counts, Calculate & Print Itinerary

    val total = finalAllStores.count()
    var daysSpentTraveling = 0
    var secondsSpentTraveling = 0//-timeSpentInStoreInSeconds
    var totalTravelTimeSeconds = 0
    var count = 0
    finalAllStores.forEach {
        if (count != 0) {
            totalTravelTimeSeconds += secondsSpentTraveling
        }
        val travelTimeToNextStore = calculateTravelTimeInSeconds(it.distance!!) + timeSpentInStoreInSeconds
        when {
            travelTimeToNextStore > totalTimeAvailableInADayInSeconds -> {
                    println("Cannot make it to next store:" + it.store + " in 8hrs: missed number of stores: " + (total - count))
                    println(finalAllStores.subList(count, total))
                return ((daysSpentTraveling * dayInSeconds) + secondsSpentTraveling)
            }
            secondsSpentTraveling + travelTimeToNextStore > totalTimeAvailableInADayInSeconds -> {
                    println("cannot make the journey today - overnight stop")
                daysSpentTraveling += 1
                secondsSpentTraveling = 0
                count += 1
                    println(it.store + " travel time = " + travelTimeToNextStore + " = (" + secondsToHMS(travelTimeToNextStore) + ", total travel = " + secondsToHMS((daysSpentTraveling * dayInSeconds) + secondsSpentTraveling))
            }
            else -> {
                secondsSpentTraveling += travelTimeToNextStore
                    println(it.store + " travel time = " + travelTimeToNextStore + " = (" + secondsToHMS(travelTimeToNextStore) + ", total travel = " + secondsToHMS((daysSpentTraveling * dayInSeconds) + secondsSpentTraveling))
                count += 1
            }
        }
    }
    return ((daysSpentTraveling * dayInSeconds) + secondsSpentTraveling)
}


fun splitStores(string: String): List<Store> =
    string.split(",").chunked(4).map { it }.map { x ->
        Store(x[0], x[1], x[2].toDoubleOrNull(), x[3].toDoubleOrNull(), null?.toDouble())
    }


fun nullStores(allStores: List<Store>): List<Store> {
    return allStores.filter { m -> m.lng == null }
}


fun validStores(allStores: List<Store>): List<Store> {
    return allStores
        .mapNotNull { p -> p.lng?.let { p } }
        .mapNotNull { p -> p.lat?.let { p } }
}


fun returnStoreDistances(lat1: Double, long1: Double, validStores: List<Store>): List<Store> {
    return validStores.map { store ->
        store.copy(
            distance =
            distanceBetweenGeolocations(lat1, long1, store.lng, store.lat)
        )
    }
        .sortedBy { it.distance }
}


fun calculateTravelTimeInSeconds(distMiles: Double): Int = ((distMiles / averageSpeedMph) * hourInSeconds).toInt()


fun returnStoreDistancesFile(lat1: Double, long1: Double, validStores: List<Store>): List<Store> {
    var latitude = lat1
    var longitude = long1
    val finalStoreList = emptyList<Store>().toMutableList()
    var latestList = validStores

    for (x in 1..validStores.count()) {
        latestList = latestList.run {
            map {store ->
                store.copy(distance = distanceBetweenGeolocations(latitude, longitude, store.lng, store.lat))
            }.sortedBy { it.distance }
        }

        latitude = latestList.first().lng!!
        longitude = latestList.first().lat!!
        finalStoreList += latestList.take(1)
        latestList = latestList.drop(1)
    }

    return finalStoreList.also(::println)
}


fun distanceBetweenGeolocations(lat1: Double, long1: Double, lat2: Double?, long2: Double?): Double? {
    return if (lat2 != null && long2 != null) {
        val earthRadius = 3958.75 // miles (or 6371.0 kilometers)
        val dLat = Math.toRadians(long2 - long1)
        val dLng = Math.toRadians(lat2 - lat1)
        val sindLat = sin(dLat / 2)
        val sindLng = sin(dLng / 2)
        val a = sindLat.pow(2.0) + (sindLng.pow(2.0)
                * cos(Math.toRadians(long1)) * cos(Math.toRadians(long2)))
        val c = 2 * atan2(sqrt(a), sqrt(1 - a))

        earthRadius * c
    } else null
}


fun secondsToHMS(secondsToConvert: Int): String {
    val inputSecs = secondsToConvert.toLong()
    val days = String.format("%02d", TimeUnit.SECONDS.toDays(inputSecs))
    val hours = String.format(
        "%02d",
        TimeUnit.SECONDS.toHours(inputSecs) - TimeUnit.DAYS.toHours(TimeUnit.SECONDS.toDays(inputSecs))
    )
    val minutes = String.format(
        "%02d",
        TimeUnit.SECONDS.toMinutes(inputSecs) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(inputSecs))
    )
    val seconds = String.format(
        "%02d",
        TimeUnit.SECONDS.toSeconds(inputSecs) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(inputSecs))
    )

    return "d:$days h:$hours m:$minutes s:$seconds"
}


