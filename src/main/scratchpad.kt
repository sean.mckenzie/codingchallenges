package main

class myClass {
    // property (data member)
    private var name: String = "Tutorials.point"

    // member function
    fun printMe() {
        print("You are at the best Learning website Named-" + name)
    }
}

class Outer {
    class Nested {
        fun foo() = " => Welcome to The TutorialsPoint.com"
    }
}




sealed class Operation {
    class Add(val value: Int) : Operation()
    class Substract(val value: Int) : Operation()
    class Multiply(val value: Int) : Operation()
    class Divide(val value: Int) : Operation()
    object Increment : Operation()
    object Decrement : Operation()
}

fun execute(x: Int, op: Operation) = when (op) {
    is Operation.Add -> x + op.value
    is Operation.Substract -> x - op.value
    is Operation.Multiply -> x * op.value
    is Operation.Divide -> x / op.value
    Operation.Increment -> x + 1 //no need for is as object - not keeping state
    Operation.Decrement -> x - 1
}



fun main(args: Array<String>) {
//    val obj = myClass() // create obj object of myClass class
//    obj.printMe()
//
//    val demo = Outer.Nested().foo() // calling nested class method
//    print(demo)

    XXFooSub().printExt()
    println()
    FooImpl().printExt()
    println()
//    ForwardingFoo.printExt()
    FooWrapper(FooImpl()).printExt()




}

interface Foo {
    fun bar(): String
    fun barTwice(): String
    fun anotherFunction()
    fun andAnotherOne()
}

class FooImpl: Foo {
    override fun bar() = "Bar" //+ "Ext"
    override fun barTwice() = bar().repeat(2) //+ "Ext"
    override fun anotherFunction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun andAnotherOne() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

fun printExt() = println("${bar()} & ${barTwice()}" )
}

open class ForwardingFoo(private val foo: Foo): Foo {
    override fun bar() = foo.bar()
    override fun barTwice() = foo.barTwice()
    override fun anotherFunction() = foo.anotherFunction()
    override fun andAnotherOne() = foo.andAnotherOne()

}

//class FooWrapper(private val foo: Foo) : ForwardingFoo(foo) { //so no need for Forwarding class
class FooWrapper(private val foo: Foo) : Foo by foo {
    override fun bar() = foo.bar() + "Ext"
    override fun barTwice() = foo.barTwice() + "Ext"
    fun printExt() = println("${bar()} & ${barTwice()}")
}



open class XXFoo {
    open fun bar() = "Bar"
    open fun barTwice() = bar().repeat(2)
//    open fun anotherFunction() = ...
//    open fun andAnotherOne() = ...
}

class XXFooSub: XXFoo() {
    override fun bar() = super.bar()  + "Ext"
//    override fun barTwice() = super.barTwice() + "Ext" // -> BarExtBarBarExt
    override fun barTwice() = super.bar().repeat(2) + "Ext"
    fun printExt() = println("${bar()} & ${barTwice()}" )
}



