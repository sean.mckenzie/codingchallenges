package main

fun addTwoRomanNumerals(numeralOne: String, numeralTwo: String): String {
    return integerToRomanNumeral(romanNumeralToInteger(numeralOne) + romanNumeralToInteger(numeralTwo))
}

fun romanNumeralToInteger(romanNumeral: String): Int {
    var value = 0
    var numeral = if (romanNumeral.length < 16) romanNumeral else "" //cap max limit to Roman Numeral length 15
    while (numeral.isNotEmpty()) {
        value += convertRomanToInt(numeral)
        numeral = numeral.dropLast(1)
    }
    return value
}

fun convertRomanToInt(numeral: String): Int {
    val lastTwo = if (numeral.length == 1) numeral.takeLast(1) else numeral.takeLast(2)
    val last = numeral.takeLast(1)
    return when {
//lastTwo return amount reduced by preceding value as it is processed next iteration
        lastTwo == "IV" -> 3
        last == "V" -> 5
        lastTwo == "IX" -> 8
        last == "X" -> 10
        lastTwo == "XL" -> 30
        last == "L" -> 50
        lastTwo == "XC" -> 80
        last == "C" -> 100
        lastTwo == "CD" -> 300
        last == "D" -> 500
        lastTwo == "CM" -> 800
        last == "M" -> 1000
        else -> 1
    }
}

fun addTwoRomanNumeralsInteger(numeralOne: String, numeralTwo: String): Int {
    return (romanNumeralToInteger(numeralOne) + romanNumeralToInteger(numeralTwo))
}

fun integerToRomanNumeral(input: Int): String {
    var value = ""
    var number = input
    val numeralAndValue: List<Pair<String, Int>> = listOf(
        Pair("M", 1000),
        Pair("CM", 900),
        Pair("D", 500),
        Pair("CD", 500),
        Pair("C", 100),
        Pair("XC", 90),
        Pair("L", 50),
        Pair("XL", 40),
        Pair("X", 10),
        Pair("IX", 9),
        Pair("V", 5),
        Pair("IV", 4),
        Pair("I", 1)
    )
    numeralAndValue.forEach { (numeral, score) ->
        val units = number / score
        value += addToNumeral(units, numeral)
        number -= (units * score)
    }
    return value
}

fun addToNumeral(units: Int, numeral: String): String {
    var count = units
    var string = ""

    while (count > 0) {
    string += numeral
        count -= 1
    }
return string
}

