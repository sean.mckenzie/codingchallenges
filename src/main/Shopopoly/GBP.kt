package main.Shopopoly

class GBP(gbp: Int) {
    var amount:Int = gbp

    init {
     if (gbp < 1) amount = 0 else gbp
    }

    operator fun plus(other:GBP):GBP{
        return GBP(this.amount + other.amount)
    }

    operator fun minus(other:GBP):GBP{
        return GBP(this.amount - other.amount)
    }

    override fun equals(other: Any?): Boolean {
        return other is GBP && other.amount == amount
    }

    override fun toString() = "£$amount"
}