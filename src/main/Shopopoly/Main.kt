package main.Shopopoly

import main.Shopopoly.DevelopmentStatus.*
import main.Shopopoly.Group.*
import main.Shopopoly.Location.*

fun main() {

    val locationCosts = mapOf<DevelopmentStatus, LocationCost>(
        Undeveloped to LocationCost(
            GBP(20),
            GBP(100)
        ),
        Ministore to LocationCost(
            GBP(20),
            GBP(120)
        ),
        Supermarket to LocationCost(
            GBP(30),
            GBP(140)
        ),
        Megastore to LocationCost(
            GBP(40),
            GBP(160)
        )
    )

    val locationCosts2 = mapOf<DevelopmentStatus, LocationCost>(
        Undeveloped to LocationCost(
            GBP(30),
            GBP(120)
        ),
        Ministore to LocationCost(
            GBP(30),
            GBP(140)
        ),
        Supermarket to LocationCost(
            GBP(40),
            GBP(160)
        ),
        Megastore to LocationCost(
            GBP(50),
            GBP(180)
        )
    )

    val retailGroup1 = Brown
    val retailGroup2 = Green

    val board = listOf(
        Go,
        FreeParking,
        FactoryOrWarehouse("Factory1"),
        FactoryOrWarehouse("Warehouse1"),
        RetailSite(
            "Retail1",
            retailGroup1,
            GBP(888),
            RetailRentalValues(
                GBP(100),
                GBP(200),
                GBP(300),
                GBP(400)
            ),
            RetailBuildingCosts(
                GBP(200),
                GBP(300),
                GBP(400)
            )
        ),
        RetailSite(
            "Retail2",
            retailGroup2,
            GBP(666),
            RetailRentalValues(
                GBP(150),
                GBP(250),
                GBP(350),
                GBP(450)
            ),
            RetailBuildingCosts(
                GBP(250),
                GBP(350),
                GBP(450)
            )
        )
    )

    for (location in board) {
        println(location)
        if (location is Purchaseable) {
            println("cost: ${location.cost}")
        }
        if (location is Rentable) {
            println("rent: ${locationCosts.getValue(Undeveloped).rentAmount}")
        }
        if (location is Developable) {
            println("dev status: ${locationCosts.getValue(Undeveloped).purchaseCost}")
        }
        if (location is RetailSite) {
            println("group: ${location.group.name}")
        }
    }
}