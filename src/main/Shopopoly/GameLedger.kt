package main.Shopopoly

import main.Shopopoly.TransactionType.*


object GameLedger {

    data class Transaction(
        val debitAccount: Player,
        val creditAccount: Player,
        val amount: GBP,
        val transactionType: TransactionType
    )

    val transactions: MutableList<Transaction> = mutableListOf()

    fun addNewPlayer(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, StartingBalance))

    fun feeForPlayerPassingGo(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, BankFeeToPlayer))

    fun feeForPlayerBuyingLocation(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, PlayerPaysBankForLocation))

    fun feeForPlayerBuyingMinistore(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, PlayerPaysBankForMinistore))

    fun feeForPlayerBuyingSupermarket(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, PlayerPaysBankForSupermarket))

    fun feeForPlayerBuyingMegastore(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, PlayerPaysBankForMegastore))

    fun feeForPlayerPayingRent(debitAccount: Player, creditAccount: Player, amount: GBP) =
        transactions.add(Transaction(debitAccount, creditAccount, amount, PlayerPaysRent))

    fun playerCredit(player: Player, ledger: GameLedger): GBP {
        var credit = GBP(0)
        ledger.transactions.forEach {
            if (it.creditAccount == player) {
                    println("credit amount = + ${it.amount}")
                credit += it.amount
                    println("${player.name} credit subtotal = $credit")
            } else if (it.debitAccount == player) {
                    println("debit amount = - ${it.amount}")
                credit -= it.amount
                    println("${player.name} credit subtotal = $credit")
            }
        }
        if (credit.amount > 0) {
            return credit
        } else {
            return GBP(0)
        }
    }
}

enum class TransactionType {
    StartingBalance,
    BankFeeToPlayer,
    PlayerPaysRent,
    PlayerPaysBankForLocation,
    PlayerPaysBankForMinistore,
    PlayerPaysBankForSupermarket,
    PlayerPaysBankForMegastore
}


interface Player {
    val name: String
    var boardLocation: Int

    fun updateLocation(boardSize: Int, diceThrow: Int): Boolean {
        val previousLocation = this.boardLocation
        val potentialLocation = previousLocation + diceThrow

        if (potentialLocation > boardSize) {
            this.boardLocation = potentialLocation - boardSize
        } else {
            this.boardLocation = potentialLocation
        }

        return (this.boardLocation < previousLocation)
    }
}

//    interface Transaction {
//        val amount: Int
//    }
//
//    interface CreditTransaction : Transaction {
//        val playerCredited: Player
//    }
//
//    interface DebitTransaction : Transaction {
//        val playerDebited: Player
//    }
/*
fun balance()
balance - credit = 0 -> xxx
money always +ve .... if none = zero credit

fun ownedLocationsAndBuildings() LIST
only last location added (as built smallest to largest)
empty list if none owned
each building upgrade of previous (so if sell downgrades...)

fun locationOwnerAndRent()
only rent of highest property - last built
if no owner - indicate such
rent = 0 for freeparking?   rent = 0 if no owner (& is purchasable)?

Game Ledger dumb - just list of transactions....


acconthlders (bank, players)
debitaccountholder
creditaccountholder



 */

