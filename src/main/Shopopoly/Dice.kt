package main.Shopopoly

import kotlin.random.Random

class Dice {
    val diceOne:Int = Random.nextInt(1, 7)
    val diceTwo:Int = Random.nextInt(1, 7)

    val totalScore:Int

    init {
        totalScore = diceOne + diceTwo
    }

    override fun toString(): String {
        return "You threw a $diceOne and a $diceTwo"
    }
}

