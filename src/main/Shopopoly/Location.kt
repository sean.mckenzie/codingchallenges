package main.Shopopoly

interface Rentable {
    val rent: GBP
}

interface Purchaseable {
    val cost: GBP
}

interface Rewardable {
    val reward: GBP
}

interface Developable {
    val developmentStatus: DevelopmentStatus
    val locationCosts: Map<DevelopmentStatus, Purchaseable>
}

data class RetailRentalValues(
    val rentUndeveloped: GBP,
    val rentMinistore: GBP,
    val rentSupermarket: GBP,
    val rentMegastore: GBP
)

data class RetailBuildingCosts(
    val costMinistore: GBP,
    val costSupermarket: GBP,
    val costMegastore: GBP
)

data class LocationCost(
    val purchaseCost: GBP,
    val rentAmount: GBP
)

sealed class Location {
    object FreeParking : Location() {
    }

    object Go : Rewardable, Location() {
        override val reward: GBP = GBP(100)
    }

    class FactoryOrWarehouse(val name: String) : Rentable, Purchaseable, Location() {
        override val cost: GBP = GBP(100)
        override val rent: GBP = GBP(20)
    }

    class RetailSite(
        val name: String,
        val group: Group,
        override val cost: GBP,
        val retailRentalValues: RetailRentalValues,
        val retailBuildingCosts: RetailBuildingCosts
    ) : Developable, Rentable, Purchaseable, Location() {
        override val locationCosts: Map<DevelopmentStatus, Purchaseable>
            get() = TODO("not implemented")
        override val rent: GBP
            get() = TODO("not implemented")//GBP(999)
        override val developmentStatus: DevelopmentStatus
            get() = TODO("not implemented")//DevelopmentStatus.Undeveloped//()//TODO("not implemented")
    }
}

enum class Group {
    Red, Green, Blue, Brown, Orange, Purple
}


