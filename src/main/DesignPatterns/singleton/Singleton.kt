package main.DesignPatterns.singleton


//EAGERLY CREATED INSTANCE
class Singleton2 private constructor() {
    // other useful methods here
    val description: String
        get() = "I'm a statically initialized Singleton!"

    companion object {
        private val uniqueInstance = Singleton2() //i.e. val not var & NOT NULL!!!!
                                                  //create instance in static initializer - guaranteed threadsafe 
        val instance: Singleton2?
            get() = uniqueInstance                //already got an instance - just return it
    }
}
//EAGERLY CREATED INSTANCE- relies on JVM to create unique instance when class is loaded
//                          JVM guarantees instance is created before any thead accesses static var: uniqueInstance 


//
// DOUBLE-CHECKED LOCKING                         //if perf an issue - this reduces overhead!!!
object Singleton3 {                               //OBJECT!!!
    @Volatile                                     //ensure multiple threads handle uniqueInstance correctly
    private var uniqueInstance: Singleton3? = null
    val instance: Singleton3?
        get() {
            if (uniqueInstance == null) {                  //check for instance else enter sync block
                synchronized(Singleton3::class.java) {     //only sync 1st time thru
                    if (uniqueInstance == null) {
                        uniqueInstance = Singleton3     //() no need for brackets?  remove invokation?
                    }
                }
            }
            return uniqueInstance
        }

        // other useful methods here
    val description: String
        get() = "I'm a volatile Singleton!"
    
}
// DOUBLE-CHECKED LOCKING



// NOTE: This is not thread safe!  until @get:Synchronized added!!
class Singleton private constructor() {
    // other useful methods here
    val description: String
        get() = "I'm a classic Singleton!"

    companion object {
        private var uniqueInstance: Singleton? = null

        @get:Synchronized //no two threads can enter the method together
                          //care - can be x100 less efficient 
        val instance: Singleton? 
            get() {
                if (uniqueInstance == null) {
                    uniqueInstance = Singleton() //lazy instantiation
                }
                return uniqueInstance
            }
    }
}

fun main(args: Array<String>) {
    println(Singleton) //unique
    println(Singleton.instance!!.description)

    println(Singleton) //same instance as != null

    
    var singleton = Singleton.instance
    println(singleton)
    println(singleton!!.description)

    singleton = Singleton.instance
    println(singleton) //same instance as != null
    println(singleton!!.description)


}

//class SingletonThreadSafe  // other useful instance variables here
//private constructor() {
//    // other useful methods here
//    val description: String
//        get() = "I'm a thread safe Singleton!"
//
//    companion object {
//        private val uniqueInstances: SingletonThreadSafe? = null
//
//        @get:Synchronized
//        val instance: SingletonThreadSafe?
//            get() {
//                if (uniqueInstances == null) {
//                    uniqueInstances = SingletonThreadSafe()
//                }
//                return uniqueInstances
//            }
//    }
//}

