package main.DesignPatterns.singleton

class ChocolateBoiler private constructor() {
    var isEmpty = true
        private set //code only started when the boiler is empty / un-boiled
    var isBoiled = false
        private set //does not work just with 'private var' - as public but privately set

    fun fill() {
        if (isEmpty) {
            isEmpty = false
            isBoiled = false
            // fill the boiler with a milk/chocolate mixture
        }
    }

    fun drain() {
        if (!isEmpty && isBoiled) {
            // drain the boiled milk and chocolate
            isEmpty = true
        }
    }

    fun boil() {
        if (!isEmpty && !isBoiled) {
            // bring the contents to a boil
            isBoiled = true
        }
    }

    companion object {
        private var uniqueInstance: ChocolateBoiler? = null

        @get:Synchronized //no two threads can enter the method together = THREAD SAFE!!
        val instance: ChocolateBoiler?
            get() {
                if (uniqueInstance == null) {
                    println("ONCE - Creating unique instance of Chocolate Boiler")
                    uniqueInstance = ChocolateBoiler()
                }
                println("MANY - Returning instance of Chocolate Boiler")
                return uniqueInstance
            }
    }

}

object ChocolateController {
    @JvmStatic
    fun main(args: Array<String>) {
        val boiler: ChocolateBoiler? = ChocolateBoiler.instance

            println("\nstart pre fill:  boiler = $boiler")
            println("boiler.isEmpty:${boiler!!.isEmpty} boiler.isBoiled:${boiler!!.isBoiled}")
        boiler.fill()
            println("\npost fill")
            println("boiler.isEmpty:${boiler!!.isEmpty} boiler.isBoiled:${boiler!!.isBoiled}")
        boiler.fill()
            println("\npost fill AGAIN!!!! DANGER - THREAD PROBLEM?  IF two instantate v close together may get two objects!")
            println("boiler.isEmpty:${boiler!!.isEmpty} boiler.isBoiled:${boiler!!.isBoiled}")
        boiler.boil()
            println("\npost boil")
            println("boiler.isEmpty:${boiler!!.isEmpty} boiler.isBoiled:${boiler!!.isBoiled}")
        boiler.drain()
            println("\npost drain")
            println("boiler.isEmpty:${boiler!!.isEmpty} boiler.isBoiled:${boiler.isBoiled}")
            println("end\n")

        // will return the existing instance
        val boiler2: ChocolateBoiler? = ChocolateBoiler.instance        
            println("\nnew boiler:  boiler = $boiler2  boiler==boiler2: ${boiler==boiler2}")

    }
}