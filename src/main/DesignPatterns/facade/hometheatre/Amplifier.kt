package main.DesignPatterns.facade.hometheatre

class Amplifier(var description: String) {
    private var tuner: Tuner? = null
    private var dvd: DvdPlayer? = null
    private var cd: CdPlayer? = null
    fun on() {
        println("$description on")
    }

    fun off() {
        println("$description off")
    }

    fun setStereoSound() {
        println("$description stereo mode on")
    }

    fun setSurroundSound() {
        println("$description surround sound on (5 speakers, 1 subwoofer)")
    }

    fun setVolume(level: Int) {
        println("$description setting volume to $level")
    }

    fun setTuner(tuner: Tuner?) {
        println("$description setting tuner to $dvd")
        this.tuner = tuner
    }

    fun setDvd(dvd: DvdPlayer) {
        println("$description setting DVD player to $dvd")
        this.dvd = dvd
    }

    fun setCd(cd: CdPlayer) {
        println("$description setting CD player to $cd")
        this.cd = cd
    }

    override fun toString(): String {
        return description
    }
}


class PopcornPopper(var description: String) {
    fun on() {
        println("$description on")
    }

    fun off() {
        println("$description off")
    }

    fun pop() {
        println("$description popping popcorn!")
    }

    override fun toString(): String {
        return description
    }
}

class Projector(var description: String, var dvdPlayer: DvdPlayer) {
    fun on() {
        println("$description on")
    }

    fun off() {
        println("$description off")
    }

    fun wideScreenMode() {
        println("$description in widescreen mode (16x9 aspect ratio)")
    }

    fun tvMode() {
        println("$description in tv mode (4x3 aspect ratio)")
    }

    override fun toString(): String {
        return description
    }
}

class Screen(var description: String) {
    fun up() {
        println("$description going up")
    }

    fun down() {
        println("$description going down")
    }

    override fun toString(): String {
        return description
    }
}

class TheaterLights(var description: String) {
    fun on() {
        println("$description on")
    }

    fun off() {
        println("$description off")
    }

    fun dim(level: Int) {
        println("$description dimming to $level%")
    }

    override fun toString(): String {
        return description
    }
}
