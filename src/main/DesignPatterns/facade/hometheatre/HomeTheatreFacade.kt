package main.DesignPatterns.facade.hometheatre

class HomeTheaterFacade( //composition - all the components of the subsystem
    var amp: Amplifier,  //facade is passed a ref to each component in its constructor 
    var tuner: Tuner,    //facade assigns each to the corresponsing instance variable
    var dvd: DvdPlayer,  // each task delegated to appropriate component of subsystem
    var cd: CdPlayer,
    var projector: Projector,
    var screen: Screen,
    var lights: TheaterLights,
    var popper: PopcornPopper
) {
    fun watchMovie(movie: String?) {  //wraps in a handy method to do all the work
        println("Get ready to watch a movie...")
        popper.on()
        popper.pop()
        lights.dim(10)
        screen.down()
        projector.on()
        projector.wideScreenMode()
        amp.on()
        amp.setDvd(dvd)
        amp.setSurroundSound()
        amp.setVolume(5)
        dvd.on()
        dvd.play(movie!!)
    }

    fun endMovie() {
        println("Shutting movie theater down...")
        popper.off()
        lights.on()
        screen.up()
        projector.off()
        amp.off()
        dvd.stop()
        dvd.eject()
        dvd.off()
    }

    fun listenToCd(cdTitle: String?) {
        println("Get ready for an audiopile experence...")
        lights.on()
        amp.on()
        amp.setVolume(5)
        amp.setCd(cd)
        amp.setStereoSound()
        cd.on()
        cd.play(cdTitle!!)
    }

    fun endCd() {
        println("Shutting down CD...")
        amp.off()
        amp.setCd(cd)
        cd.eject()
        cd.off()
    }

    fun listenToRadio(frequency: Double) {
        println("Tuning in the airwaves...")
        tuner.on()
        tuner.setFrequency(frequency)
        amp.on()
        amp.setVolume(5)
        amp.setTuner(tuner)
    }

    fun endRadio() {
        println("Shutting down the tuner...")
        tuner.off()
        amp.off()
    }
}

