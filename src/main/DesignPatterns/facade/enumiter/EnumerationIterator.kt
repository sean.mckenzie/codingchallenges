package main.DesignPatterns.facade.enumiter

import java.util.*

object EI {
    @JvmStatic
    fun main(args: Array<String>) {
        val v = Vector(Arrays.asList(*args))
        val enumeration = v.elements()
        while (enumeration.hasMoreElements()) {
            println(enumeration.nextElement())
        }
        val iterator: Iterator<String> = v.iterator()
        while (iterator.hasNext()) {
            println(iterator.next())
        }
    }
}


class EnumerationIterator(var enumeration: Enumeration<*>) : MutableIterator<Any?> {
    override fun hasNext(): Boolean {
        return enumeration.hasMoreElements()
    }

    override fun next(): Any {
        return enumeration.nextElement()
    }

    override fun remove() {
        throw UnsupportedOperationException()
    }
}

object EnumerationIteratorTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val v = Vector(Arrays.asList(*args))
        val iterator: Iterator<*> = EnumerationIterator(v.elements())
        while (iterator.hasNext()) {
            println(iterator.next())
        }
    }
}

class IteratorEnumeration(var iterator: Iterator<*>) : Enumeration<Any> {
    override fun hasMoreElements(): Boolean {
        return iterator.hasNext()
    }

    override fun nextElement(): Any {
        return iterator.next()!!
    }
}

object IteratorEnumerationTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val l = ArrayList(Arrays.asList(*args))
        val enumeration: Enumeration<*> = IteratorEnumeration(l.iterator())
        while (enumeration.hasMoreElements()) {
            println(enumeration.nextElement())
        }
    }
}
