package main.DesignPatterns.facade.ducks

import java.util.*

interface Duck {
    fun quack()
    fun fly()
}

class MallardDuck : Duck {
    override fun quack() {
        println("Quack")
    }

    override fun fly() {
        println("I'm flying")
    }
}

interface Turkey {
    fun gobble()
    fun fly()
}

class WildTurkey : Turkey {
    override fun gobble() {
        println("Gobble gobble")
    }

    override fun fly() {
        println("I'm flying a short distance")
    }
}


//need to implement interface of type adapting to!!  returns Duck (interface client expects to see)
class TurkeyAdapter(var turkey: Turkey) : Duck {   //ref to object adapting - thru constructor = give Turkey return Duck
    
    override fun quack() {   //implement methods in Duck interface quack() calls gobble()
        turkey.gobble()
    }

    override fun fly() {
        for (i in 0..4) {  //fly 5 times to equate to Duck fly()
            turkey.fly()
        }
    }
}

object DuckTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val duck = MallardDuck()
        val turkey = WildTurkey()
        val turkeyAdapter: Duck = TurkeyAdapter(turkey)
        println("The Turkey says...")
        turkey.gobble()
        turkey.fly()
        println("\nThe Duck says...")
        testDuck(duck)
        println("\nThe TurkeyAdapter says...")
        testDuck(turkeyAdapter)
    }

    fun testDuck(duck: Duck) {  //get the Duck and call its quak & fly methods
        duck.quack()
        duck.fly()
    }
}


class DuckAdapter(var duck: Duck) : Turkey {
    var rand: Random
    override fun gobble() {
        duck.quack()
    }

    override fun fly() {
        if (rand.nextInt(5) == 0) {
            duck.fly()
        }
    }

    init {
        rand = Random()
    }
}

object TurkeyTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val duck = MallardDuck()
        val duckAdapter: Turkey = DuckAdapter(duck)
        for (i in 0..9) {
            println("The DuckAdapter says...")
            duckAdapter.gobble()
            duckAdapter.fly()
        }
    }
}
