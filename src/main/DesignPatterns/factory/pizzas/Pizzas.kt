package main.DesignPatterns.factory.pizzas

import java.util.*


abstract class Pizza {
    var name: String? = null
    var dough: String? = null
    var sauce: String? = null
    var toppings = ArrayList<String>()

    fun prepare() {
        println("Preparing $name")
    }

    fun bake() {
        println("Baking $name")
    }

    fun cut() {
        println("Cutting $name")
    }

    fun box() {
        println("Boxing $name")
    }

    override fun toString(): String {
        // code to display pizza name and ingredients
        val display = StringBuffer()
        display.append("---- $name ----\n")
        display.append(
            """
                $dough
                
                """.trimIndent()
        )
        display.append(
            """
                $sauce
                
                """.trimIndent()
        )
        for (topping in toppings) {
            display.append(
                """
                    $topping
                    
                    """.trimIndent()
            )
        }
        return display.toString()
    }

    companion object {
        fun getName(pizza: Pizza): String? {
            return pizza.name
        }
    }
}

class SimplePizzaFactory {
    fun createPizza(type: String): Pizza? {
        var pizza: Pizza? = null
        if (type == "cheese") {
            pizza = CheesePizza()
        } else if (type == "pepperoni") {
            pizza = PepperoniPizza()
        } else if (type == "clam") {
            pizza = ClamPizza()
        } else if (type == "veggie") {
            pizza = VeggiePizza()
        }
        return pizza
    }
}
object PizzaTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val factory = SimplePizzaFactory()
        val store = PizzaStore(factory)
        var pizza: Pizza = store.orderPizza("cheese")
        System.out.println(
            """
                We ordered a ${Pizza.getName(pizza).toString()}
                
                """.trimIndent()
        )
        System.out.println(pizza)
        pizza = store.orderPizza("veggie")
        System.out.println(
            """
                We ordered a ${Pizza.getName(pizza).toString()}
                
                """.trimIndent()
        )
        System.out.println(pizza)
    }
}

class CheesePizza : Pizza() {
    init {
        name = "Cheese Pizza"
        dough = "Regular Crust"
        sauce = "Marinara Pizza Sauce"
        toppings.add("Fresh Mozzarella")
        toppings.add("Parmesan")
    }
}
class ClamPizza : Pizza() {
    init {
        name = "Clam Pizza"
        dough = "Thin crust"
        sauce = "White garlic sauce"
        toppings.add("Clams")
        toppings.add("Grated parmesan cheese")
    }
}
class PepperoniPizza : Pizza() {
    init {
        name = "Pepperoni Pizza"
        dough = "Crust"
        sauce = "Marinara sauce"
        toppings.add("Sliced Pepperoni")
        toppings.add("Sliced Onion")
        toppings.add("Grated parmesan cheese")
    }
}
class VeggiePizza : Pizza() {
    init {
        name = "Veggie Pizza"
        dough = "Crust"
        sauce = "Marinara sauce"
        toppings.add("Shredded mozzarella")
        toppings.add("Grated parmesan")
        toppings.add("Diced onion")
        toppings.add("Sliced mushrooms")
        toppings.add("Sliced red pepper")
        toppings.add("Sliced black olives")
    }
}


class PizzaStore(factory: SimplePizzaFactory) {
    var factory: SimplePizzaFactory
    fun orderPizza(type: String?): Pizza {
        val pizza: Pizza
        pizza = type?.let { factory.createPizza(it) }!!
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza
    }

    init {
        this.factory = factory
    }
}
