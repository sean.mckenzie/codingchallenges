package main.DesignPatterns.factory.pizzas
//
//abstract class PizzaStores {
//    protected abstract fun createPizza(item: String?): Pizzas
//    fun orderPizza(type: String?): Pizzas {
//        val pizza = createPizza(type)
//        System.out.println("--- Making a " )//+ pizza.getName().toString() + " ---")
//        pizza.prepare()
//        pizza.bake()
//        pizza.cut()
//        pizza.box()
//        return pizza
//    }
//}
//
//
//abstract class Pizzas {
//    var name: String? = null
//    var dough: Dough? = null
//    var sauce: Sauce? = null
//    var veggies: Array<Veggies>? = emptyArray()
//    var cheese: Cheese? = null
//    var pepperoni: Pepperoni? = null
//    var clam: Clams? = null
//    abstract fun prepare()
//
//    fun getName(): String? {
//        return name
//    }
//
////    fun setName(value: String?) {
////            name = value
////        }
//
//    fun bake() {
//        println("Bake for 25 minutes at 350")
//    }
//
//    fun cut() {
//        println("Cutting the pizza into diagonal slices")
//    }
//
//    fun box() {
//        println("Place pizza in official PizzaStore box")
//    }
//
//    override fun toString(): String {
//        val result = StringBuffer()
//        result.append("---- $name ----\n")
//        if (dough != null) {
//            result.append(dough)
//            result.append("\n")
//        }
//        if (sauce != null) {
//            result.append(sauce)
//            result.append("\n")
//        }
//        if (cheese != null) {
//            result.append(cheese)
//            result.append("\n")
//        }
//        if (veggies != null) {
//            for (i in veggies!!.indices) {
//                result.append(veggies!![i])
//                if (i < veggies!!.size - 1) {
//                    result.append(", ")
//                }
//            }
//            result.append("\n")
//        }
//        if (clam != null) {
//            result.append(clam)
//            result.append("\n")
//        }
//        if (pepperoni != null) {
//            result.append(pepperoni)
//            result.append("\n")
//        }
//        return result.toString()
//    }
//
//
//    class CheesePizzas(var ingredientFactory: PizzaIngredientFactory) : Pizza() {
//        override fun prepare() {
//            System.out.println("Preparing $name")
//            dough = ingredientFactory.createDough()
//            sauce = ingredientFactory.createSauce()
//            cheese = ingredientFactory.createCheese()
//        }
//
//    }
//
//
//}
//
//interface Veggies {
//    override fun toString(): String
//}
//
//interface Sauce {
//    override fun toString(): String
//}
//
//interface Dough {
//    override fun toString(): String
//}
//
//interface Cheese {
//    override fun toString(): String
//}
//interface Pepperoni {
//    override fun toString(): String
//}
//interface Clams {
//    override fun toString(): String
//}
//
//
//interface PizzaIngredientFactory {
//    fun createDough(): Dough?
//    fun createSauce(): Sauce?
//    fun createCheese(): Cheese?
//    fun createVeggies(): Array<Veggies?>?
//    fun createPepperoni(): Pepperoni?
//    fun createClam(): Clams?
//}
//
//object PizzaTestDriveF {
//    @JvmStatic
//    fun main(args: Array<String>) {
//        val nyStore: PizzaStores = NYPizzaStore()
//        val chicagoStore: PizzaStores = ChicagoPizzaStore()
//        var pizza = nyStore.orderPizza("cheese")
//        println("Ethan ordered a $pizza\n")
//        pizza = chicagoStore.orderPizza("cheese")
//        println("Joel ordered a $pizza\n")
//        pizza = nyStore.orderPizza("clam")
//        println("Ethan ordered a $pizza\n")
//        pizza = chicagoStore.orderPizza("clam")
//        println("Joel ordered a $pizza\n")
//        pizza = nyStore.orderPizza("pepperoni")
//        println("Ethan ordered a $pizza\n")
//        pizza = chicagoStore.orderPizza("pepperoni")
//        println("Joel ordered a $pizza\n")
//        pizza = nyStore.orderPizza("veggie")
//        println("Ethan ordered a $pizza\n")
//        pizza = chicagoStore.orderPizza("veggie")
//        println("Joel ordered a $pizza\n")
//    }
//}
//
//
//class NYPizzaStore : PizzaStores() {
//    protected fun createPizza(item: String): Pizza? {
//        var pizza: Pizzas? = null
//        val ingredientFactory: PizzaIngredientFactory = NYPizzaIngredientFactory()
//        if (item == "cheese") {
//            pizza = CheesePizzas(ingredientFactory)
//            pizza.setName("New York Style Cheese Pizza")
//        } else if (item == "veggie") {
//            pizza = VeggiePizza(ingredientFactory)
//            pizza.setName("New York Style Veggie Pizza")
//        } else if (item == "clam") {
//            pizza = ClamPizza(ingredientFactory)
//            pizza.setName("New York Style Clam Pizza")
//        } else if (item == "pepperoni") {
//            pizza = PepperoniPizza(ingredientFactory)
//            pizza.setName("New York Style Pepperoni Pizza")
//        }
//        return pizza
//    }
//}
//
//class CheesePizzas(var ingredientFactory: PizzaIngredientFactory) : Pizzas() {
//    override fun prepare() {
//        System.out.println("Preparing $name")
//        dough = ingredientFactory.createDough()
//        sauce = ingredientFactory.createSauce()
//        cheese = ingredientFactory.createCheese()
//    }
//
//}
//
//class NYPizzaIngredientFactory : PizzaIngredientFactory {
//    override fun createDough(): Dough {
//        return ThinCrustDough()
//    }
//
//    override fun createSauce(): Sauce {
//        return MarinaraSauce()
//    }
//
//    override fun createCheese(): Cheese {
//        return ReggianoCheese()
//    }
//
//    override fun createVeggies(): Array<Veggies> {
//        return arrayOf(Garlic(), Onion(), Mushroom(), RedPepper())
//    }
//
//    override fun createPepperoni(): Pepperoni {
//        return SlicedPepperoni()
//    }
//
//    override fun createClam(): Clams {
//        return FreshClams()
//    }
//}
