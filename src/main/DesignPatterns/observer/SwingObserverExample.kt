package main.DesignPatterns.observer

import java.awt.BorderLayout
import java.awt.event.ActionEvent
import javax.swing.JButton
import javax.swing.JFrame


class SwingObserverExample {
    var frame: JFrame? = null
    fun go() {
        frame = JFrame()
        val button = JButton("Should I do it?")

        // Without lambdas
//        button.addActionListener(AngelListener())
//        button.addActionListener(DevilListener())

        // With lambdas
        button.addActionListener { event: ActionEvent? -> println("Don't do it, you might regret it!") }
        button.addActionListener { event: ActionEvent? -> println("Come on, do it!") }
        frame!!.contentPane.add(BorderLayout.CENTER, button)

        // Set frame properties
        frame!!.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        frame!!.contentPane.add(BorderLayout.CENTER, button)
        frame!!.setSize(300, 300)
        frame!!.isVisible = true
    }

	/*
	 * Remove these two inner classes to use lambda expressions instead.
	 *

    internal inner class AngelListener : ActionListener {
        override fun actionPerformed(event: ActionEvent?) {
            println("Don't do it, you might regret it!")
        }
    }

    internal inner class DevilListener : ActionListener {
        override fun actionPerformed(event: ActionEvent?) {
            println("Come on, do it!")
        }
    }
*/
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val example = SwingObserverExample()
            example.go()
        }
    }
}


typealias Observable = (Int) -> Unit

class MyThingToObserve() {
    private var counter: Int = 0
    val observers: MutableList<Observable> = mutableListOf()
    fun incrementCounter() {
        counter++
        observe(counter)
    }
    private fun observe(newValue: Int) {
        observers.forEach { observer->
            observer(newValue)
        }
    }
    fun addObserver(o: Observable) {
        observers.add(o)
    }
}
fun main() {
    val thing = MyThingToObserve()
    thing.addObserver { n: Int->
        println(n)
    }
    thing.incrementCounter()
}
