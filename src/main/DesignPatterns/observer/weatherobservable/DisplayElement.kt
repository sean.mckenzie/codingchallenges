package main.DesignPatterns.observer.weatherobservable

interface DisplayElement {
    fun display()
}
