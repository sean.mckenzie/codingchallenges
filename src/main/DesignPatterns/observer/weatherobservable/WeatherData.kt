package headfirst.designpatterns.observer.weatherobservable

import java.util.*

class WeatherData : Observable() {
    var temperature = 0f
    var humidity = 0f
    var pressure = 0f

    fun measurementsChanged() {
        setChanged()
        notifyObservers()
    }

    fun setMeasurements(temperature: Float, humidity: Float, pressure: Float) {
        this.temperature = temperature
        this.humidity = humidity
        this.pressure = pressure
        measurementsChanged()
    }
}
