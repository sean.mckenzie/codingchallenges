package main.DesignPatterns.observer.weatherobservable

import headfirst.designpatterns.observer.weatherobservable.WeatherData
import java.util.*

class ForecastDisplay(observable: Observable) : Observer, DisplayElement {
    private var currentPressure = 29.92f
    private var lastPressure = 0f
    override fun update(observable: Observable, arg: Any?) {
        if (observable is WeatherData) {
            lastPressure = currentPressure
            currentPressure = observable.pressure
            display()
        }
    }

    override fun display() {
        print("Forecast: ")
        if (currentPressure > lastPressure) {
            println("Improving weather on the way!")
        } else if (currentPressure == lastPressure) {
            println("More of the same")
        } else if (currentPressure < lastPressure) {
            println("Watch out for cooler, rainy weather")
        }
    }

    init {
        observable.addObserver(this)
    }
}
