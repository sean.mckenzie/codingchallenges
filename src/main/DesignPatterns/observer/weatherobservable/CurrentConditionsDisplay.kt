package main.DesignPatterns.observer.weatherobservable

import headfirst.designpatterns.observer.weatherobservable.WeatherData
import java.util.*


class CurrentConditionsDisplay(var observable: Observable) : Observer, DisplayElement {
    private var temperature = 0f
    private var humidity = 0f
    override fun update(obs: Observable, arg: Any?) {
        if (obs is WeatherData) {
            val weatherData = obs
            temperature = weatherData.temperature
            humidity = weatherData.humidity
            display()
        }
    }

    override fun display() {
        println(
            "Current conditions: " + temperature
                    + "F degrees and " + humidity + "% humidity"
        )
    }

    init {
        observable.addObserver(this)
    }
}