package main.DesignPatterns.observer.weather

interface DisplayElement {
    fun display()
}
