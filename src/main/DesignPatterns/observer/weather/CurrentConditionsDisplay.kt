package main.DesignPatterns.observer.weather

class CurrentConditionsDisplay(private val weatherData: Subject) : Observer, DisplayElement {
    private var temperature = 0f
    private var humidity = 0f
    override fun update(temperature: Float, humidity: Float, pressure: Float) {
        this.temperature = temperature
        this.humidity = humidity
        display()
    }

    override fun display() {
        println(
            "\nCurrent conditions: " + temperature
                    + "F degrees and " + humidity + "% humidity"
        )
    }

    init {
        weatherData.registerObserver(this)
    }
}
