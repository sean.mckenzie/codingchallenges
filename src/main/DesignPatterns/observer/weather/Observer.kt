package main.DesignPatterns.observer.weather

interface Observer {
    fun update(temp: Float, humidity: Float, pressure: Float)
}
