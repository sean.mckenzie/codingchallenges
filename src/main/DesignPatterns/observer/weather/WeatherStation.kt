package main.DesignPatterns.observer.weather

import headfirst.designpatterns.observer.weather.WeatherData

object WeatherStation {
    @JvmStatic
    fun main(args: Array<String>) {
        //create weather data object
        val weatherData = WeatherData()

        //create displays and pass them the object
        val currentDisplay = CurrentConditionsDisplay(weatherData)
        val statisticsDisplay = StatisticsDisplay(weatherData)
        val forecastDisplay = ForecastDisplay(weatherData)
        val heatIndexDisplay = HeatIndexDisplay(weatherData)

        //simulate new weather measurements:
        weatherData.setMeasurements(80f, 65f, 30.4f)
        weatherData.setMeasurements(82f, 70f, 29.2f)
        weatherData.setMeasurements(78f, 90f, 29.2f)
    }
}
