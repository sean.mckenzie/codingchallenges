package headfirst.designpatterns.observer.weather

import main.DesignPatterns.observer.weather.Observer
import main.DesignPatterns.observer.weather.Subject
import java.util.*

class WeatherData : Subject {
    val observers: MutableList<Observer>
    var temperature = 0f
    var humidity = 0f
    var pressure = 0f

    override fun registerObserver(o: Observer?) {
        if (o != null) {
            observers.add(o)
        }
    }

    override fun removeObserver(o: Observer?) {
        val i = observers.indexOf(o)
        if (i >= 0) {
            observers.removeAt(i)
        }
    }

    override fun notifyObservers() {
        for (observer in observers) {
            observer.update(temperature, humidity, pressure)
        }
    }

    fun measurementsChanged() {
        notifyObservers()
    }

    fun setMeasurements(temperature: Float, humidity: Float, pressure: Float) {
        this.temperature = temperature
        this.humidity = humidity
        this.pressure = pressure
        measurementsChanged()
    }

    init {
        observers = ArrayList()
    }
}