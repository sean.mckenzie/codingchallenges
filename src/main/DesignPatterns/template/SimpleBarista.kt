package main.DesignPatterns.template

object Barista {
    @JvmStatic
    fun main(args: Array<String>) {
        val tea = TeaOld()
        val coffee = CoffeeOld()
        println("Making tea...")
        tea.prepareRecipe()
        println("Making coffee...")
        coffee.prepareRecipe()
    }
}


class CoffeeOld {
    fun prepareRecipe() {
        boilWater()
        brewCoffeeGrinds()
        pourInCup()
        addSugarAndMilk()
    }

    fun boilWater() {
        println("Boiling water")
    }

    fun brewCoffeeGrinds() {
        println("Dripping Coffee through filter")
    }

    fun pourInCup() {
        println("Pouring into cup")
    }

    fun addSugarAndMilk() {
        println("Adding Sugar and Milk")
    }
}


class TeaOld {
    fun prepareRecipe() {
        boilWater()
        steepTeaBag()
        pourInCup()
        addLemon()
    }

    fun boilWater() {
        println("Boiling water")
    }

    fun steepTeaBag() {
        println("Steeping the tea")
    }

    fun addLemon() {
        println("Adding Lemon")
    }

    fun pourInCup() {
        println("Pouring into cup")
    }
}
