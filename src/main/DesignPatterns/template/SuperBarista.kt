package main.DesignPatterns.template

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

object BeverageTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val tea = Tea()
        val coffee = Coffee()
        println("\nMaking tea...")
        tea.prepareRecipe()
        println("\nMaking coffee...")
        coffee.prepareRecipe()
        val teaHook = TeaWithHook()
        val coffeeHook = CoffeeWithHook()
        println("\nMaking tea...")
        teaHook.prepareRecipe()
        println("\nMaking coffee...")
        coffeeHook.prepareRecipe()
    }
}


abstract class CaffeineBeverage {
    fun prepareRecipe() {
        boilWater()
        brew()
        pourInCup()
        addCondiments()
    }

    abstract fun brew()
    abstract fun addCondiments()
    fun boilWater() {
        println("Boiling water")
    }

    fun pourInCup() {
        println("Pouring into cup")
    }
}

class Tea : CaffeineBeverage() {
    override fun brew() {
        println("Steeping the tea")
    }

    override fun addCondiments() {
        println("Adding Lemon")
    }
}

class Coffee : CaffeineBeverage() {
    override fun brew() {
        println("Dripping Coffee through filter")
    }

    override fun addCondiments() {
        println("Adding Sugar and Milk")
    }
}

class CoffeeWithHook : CaffeineBeverageWithHook() {
    override fun brew() {
        println("Dripping Coffee through filter")
    }

    override fun addCondiments() {
        println("Adding Sugar and Milk")
    }

    override fun customerWantsCondiments(): Boolean {
        val answer = userInput
        return if (answer.toLowerCase().startsWith("y")) {
            true
        } else {
            false
        }
    }

    private val userInput: String
        private get() {
            var answer: String? = null
            print("Would you like milk and sugar with your coffee (y/n)? ")
            val `in` = BufferedReader(InputStreamReader(System.`in`))
            try {
                answer = `in`.readLine()
            } catch (ioe: IOException) {
                System.err.println("IO error trying to read your answer")
            }
            return answer ?: "no"
        }
}

class TeaWithHook : CaffeineBeverageWithHook() {
    override fun brew() {
        println("Steeping the tea")
    }

    override fun addCondiments() {
        println("Adding Lemon")
    }

    override fun customerWantsCondiments(): Boolean {
        val answer = userInput
        return if (answer.toLowerCase().startsWith("y")) {
            true
        } else {
            false
        }
    }

    // get the user's response
    private val userInput: String
        private get() {
            // get the user's response
            var answer: String? = null
            print("Would you like lemon with your tea (y/n)? ")
            val `in` = BufferedReader(InputStreamReader(System.`in`))
            try {
                answer = `in`.readLine()
            } catch (ioe: IOException) {
                System.err.println("IO error trying to read your answer")
            }
            return answer ?: "no"
        }
}

abstract class CaffeineBeverageWithHook {
    fun prepareRecipe() { //is our template method
        boilWater()
        brew()
        pourInCup()
        if (customerWantsCondiments()) {
            addCondiments()
        }
    }

    //abstract cannot be instantiated
    abstract fun brew() //methods need to be supplied by subclass are abstract
    abstract fun addCondiments()
    fun boilWater() {
        println("Boiling water")
    }

    fun pourInCup() {
        println("Pouring into cup")
    }

    open fun customerWantsCondiments(): Boolean {
        return true
    }
}
