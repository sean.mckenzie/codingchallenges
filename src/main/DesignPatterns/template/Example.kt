package main.DesignPatterns.template

//class Duck(var name: String, var weight: Int) : Comparable<Duck?> {
//    override fun toString(): String {
//        return "$name weighs $weight"
//    }
//
//    fun compareTo(`object`: Duck): Int {
//        return if (weight < `object`.weight) {
//            -1
//        } else if (weight == `object`.weight) {
//            0
//        } else { // this.weight > otherDuck.weight
//            1
//        }
//    }
//    
//}
//
//
//object DuckSortTestDrive {
//    @JvmStatic
//    fun main(args: Array<String>) {
//        val ducks = arrayOf(
//            Duck("Daffy", 8),
//            Duck("Dewey", 2),
//            Duck("Howard", 7),
//            Duck("Louie", 2),
//            Duck("Donald", 10),
//            Duck("Huey", 2)
//        )
//        println("Before sorting:")
//        display(ducks)
//        Arrays.sort(ducks)
//        println("\nAfter sorting:")
//        display(ducks)
//    }
//
//    fun display(ducks: Array<Duck>) {
//        for (d in ducks) {
//            System.out.println(d)
//        }
//    }
//}

internal abstract class Generalization {
    // 1. Standardize the skeleton of an algorithm in a "template" method
    fun findSolution() {
        stepOne()
        stepTwo()
        stepThr()
        stepFor()
    }

    // 2. Common implementations of individual steps are defined in base class
    private fun stepOne() {
        println("Generalization.stepOne")
    }

    // 3. Steps requiring peculiar implementations are "placeholders" in the base class
    abstract fun stepTwo()
    abstract fun stepThr()
    open fun stepFor() {
        println("Generalization.stepFor")
    }
}

internal abstract class Specialization : Generalization() {
    // 4. Derived classes can override placeholder methods
    // 1. Standardize the skeleton of an algorithm in a "template" method
    override fun stepThr() {
        step3_1()
        step3_2()
        step3_3()
    }

    // 2. Common implementations of individual steps are defined in base class
    private fun step3_1() {
        println("Specialization.step3_1")
    }

    // 3. Steps requiring peculiar implementations are "placeholders" in the base class
    protected abstract fun step3_2()
    private fun step3_3() {
        println("Specialization.step3_3")
    }
}

internal class Realization : Specialization() {
    // 4. Derived classes can override placeholder methods
    override fun stepTwo() {
        println("Realization.stepTwo")
    }

    override fun step3_2() {
        println("Realization.step3_2")
    }

    // 5. Derived classes can override implemented methods
    // 6. Derived classes can override and "call back to" base class methods
    override fun stepFor() {
        println("Realization.stepFor")
        super.stepFor()
    }
}

object TemplateMethodDemo {
    @JvmStatic
    fun main(args: Array<String>) {
        val algorithm: Generalization = Realization()
        algorithm.findSolution()
    }
}