package main.DesignPatterns.component.iterator

//class Menu(override var name: String, override var description: String) : MenuComponent() {
//    var iterator: Iterator<MenuComponent>? = null
//    var menuComponents: ArrayList<MenuComponent> = ArrayList<MenuComponent>()
//
//    override fun add(menuComponent: MenuComponent?) {
//        if (menuComponent != null) {
//            menuComponents.add(menuComponent)
//        }
//    }
//
//    override fun remove(menuComponent: MenuComponent?) {
//        menuComponents.remove(menuComponent)
//    }
//
//    override fun getChild(i: Int): MenuComponent {
//        return menuComponents[i]
//    }
//
//    override fun createIterator(): Iterator<MenuComponent?> {
//        if (iterator == null) {
//            iterator = CompositeIterator(menuComponents.iterator())
//        }
//        return iterator as Iterator<MenuComponent?>
//    }
//
//    override fun print() {
//        print(
//            """
//                
//                $name
//                """.trimIndent()
//        )
//        println(", $description")
//        println("---------------------")
//        val iterator: Iterator<MenuComponent> = menuComponents.iterator()
//        while (iterator.hasNext()) {
//            val menuComponent: MenuComponent = iterator.next()
//            menuComponent.print()
//        }
//    }
//}
//
//
//abstract class MenuComponent {
//    open fun add(menuComponent: MenuComponent?) {
//        throw UnsupportedOperationException()
//    }
//
//    open fun remove(menuComponent: MenuComponent?) {
//        throw UnsupportedOperationException()
//    }
//
//    open fun getChild(i: Int): MenuComponent {
//        throw UnsupportedOperationException()
//    }
//
//    open val name: String
//        get() {
//            throw UnsupportedOperationException()
//        }
//    open val description: String
//        get() {
//            throw UnsupportedOperationException()
//        }
//    open val price: Double
//        get() {
//            throw UnsupportedOperationException()
//        }
//    open val isVegetarian: Boolean
//        get() {
//            throw UnsupportedOperationException()
//        }
//
//    abstract fun createIterator(): Iterator<MenuComponent?>
//    open fun print() {
//        throw UnsupportedOperationException()
//    }
//}
//
//class MenuItem(
//    override var name: String,
//    override var description: String,
//    override var isVegetarian: Boolean,
//    override var price: Double
//) : MenuComponent() {
//
//    override fun createIterator(): NullIterator {
//        return NullIterator()
//    }
//
//    override fun print() {
//        print("  $name")
//        if (isVegetarian) {
//            print("(v)")
//        }
//        println(", $price")
//        println("     -- $description")
//    }
//}
//
//class CompositeIterator(iterator: Iterator<MenuComponent?>?) :
//    MutableIterator<MenuComponent> {
//    var stack = Stack<Iterator<MenuComponent>?>()
//    override fun next(): MenuComponent {
//        return if (hasNext()) {
//            val iterator = stack.peek()
//            val component = iterator!!.next()
//            stack.push(component.createIterator())
//            component
//        } 
//    }
//
//    override fun hasNext(): Boolean {
//        return if (stack.empty()) {
//            false
//        } else {
//            val iterator = stack.peek()
//            if (!iterator!!.hasNext()) {
//                stack.pop()
//                hasNext()
//            } else {
//                true
//            }
//        }
//    } /*
//	 * No longer needed as of Java 8
//	 * 
//	 * (non-Javadoc)
//	 * @see java.util.Iterator#remove()
//	 *
//	public void remove() {
//		throw new UnsupportedOperationException();
//	}
//	*/
//
//    init {
//        iterator?.let { stack.push(it) }
//    }
//
//    override fun remove() {
//        TODO("Not yet implemented")
//    }
//}
//
//private fun <E> Stack<E>.push(createIterator: Iterator<MenuComponent?>) {
//
//}
//
//class NullIterator : MutableIterator<MenuComponent?> {
//    override fun next(): MenuComponent? {
//        return null
//    }
//
//    override fun hasNext(): Boolean {
//        return false
//    } /*
//	 * No longer needed as of Java 8
//	 * 
//	 * (non-Javadoc)
//	 * @see java.util.Iterator#remove()
//	 * 
//	public void remove() {
//		throw new UnsupportedOperationException();
//	}
//	*/
//
//    override fun remove() {
//        TODO("Not yet implemented")
//    }
//}
//
//
//class Waitress(var allMenus: MenuComponent) {
//    fun printMenu() {
//        allMenus.print()
//    }
//
//    fun printVegetarianMenu() {
//        val iterator = allMenus.createIterator()
//        println("\nVEGETARIAN MENU\n----")
//        while (iterator!!.hasNext()) {
//            val menuComponent = iterator.next()
//            try {
//                if (menuComponent!!.isVegetarian) {
//                    menuComponent.print()
//                }
//            } catch (e: UnsupportedOperationException) {
//            }
//        }
//    }
//}
//
