package main.DesignPatterns.iterator

import java.util.*

class MenuItem(
    var name: String,
    var description: String,
    var isVegetarian: Boolean,
    var price: Double
) {

    override fun toString(): String {
        return "$name, $$price\n   $description"
    }
}

class DinerMenu : Menu {
    var numberOfItems = 0
    var menuItems: Array<MenuItem?>

    fun addItem(
        name: String?, description: String?,
        vegetarian: Boolean, price: Double
    ) {
        val menuItem = MenuItem(name!!, description!!, vegetarian, price)
        if (numberOfItems >= MAX_ITEMS) {
            System.err.println("Sorry, menu is full!  Can't add item to menu")
        } else {
            menuItems[numberOfItems] = menuItem
            numberOfItems += 1
        }
    }

    override fun createIterator(): Iterator {
        return DinerMenuIterator(menuItems)
        // To test Alternating menu items, comment out above line,
        // and uncomment the line below.
        //return new AlternatingDinerMenuIterator(menuItems);
    } // other menu methods here

    companion object {
        const val MAX_ITEMS = 6
    }

    init {
        menuItems = arrayOfNulls(MAX_ITEMS)
        addItem(
            "Vegetarian BLT",
            "(Fakin') Bacon with lettuce & tomato on whole wheat", true, 2.99
        )
        addItem(
            "BLT",
            "Bacon with lettuce & tomato on whole wheat", false, 2.99
        )
        addItem(
            "Soup of the day",
            "Soup of the day, with a side of potato salad", false, 3.29
        )
        addItem(
            "Hotdog",
            "A hot dog, with saurkraut, relish, onions, topped with cheese",
            false, 3.05
        )
        addItem(
            "Steamed Veggies and Brown Rice",
            "Steamed vegetables over brown rice", true, 3.99
        )
        addItem(
            "Pasta",
            "Spaghetti with Marinara Sauce, and a slice of sourdough bread",
            true, 3.89
        )
    }
}

class DinerMenuIterator(var items: Array<MenuItem?>) : Iterator {
    var position = 0
    override fun next(): MenuItem? {
        val menuItem = items[position]
        position += 1
        return menuItem
    }

    override fun hasNext(): Boolean {
        return !(position >= items.size || items[position] == null)
    }
}


interface Iterator {
    operator fun hasNext(): Boolean
    operator fun next(): MenuItem?
}


class PancakeHouseMenuIterator(var items: ArrayList<MenuItem>) : Iterator {
    var position = 0
    override fun next(): MenuItem {
        val item = items[position]
        position += 1
        return item
    }

    override fun hasNext(): Boolean {
        return position < items.size
    }
}


class PancakeHouseMenu : Menu {
    var menuItems: ArrayList<MenuItem> = ArrayList()

    fun addItem(
        name: String?, description: String?,
        vegetarian: Boolean, price: Double
    ) {
        val menuItem = MenuItem(name!!, description!!, vegetarian, price)
        menuItems.add(menuItem)
    }

    override fun createIterator(): Iterator {
        return PancakeHouseMenuIterator(menuItems)
    }

    override fun toString(): String {
        return "Objectville Pancake House Menu"
    } // other menu methods here

    init {
        addItem(
            "K&B's Pancake Breakfast",
            "Pancakes with scrambled eggs, and toast",
            true,
            2.99
        )
        addItem(
            "Regular Pancake Breakfast",
            "Pancakes with fried eggs, sausage",
            false,
            2.99
        )
        addItem(
            "Blueberry Pancakes",
            "Pancakes made with fresh blueberries",
            true,
            3.49
        )
        addItem(
            "Waffles",
            "Waffles, with your choice of blueberries or strawberries",
            true,
            3.59
        )
    }
}

interface Menu {
    fun createIterator(): Iterator?
}


object MenuTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val pancakeHouseMenu = PancakeHouseMenu()
        val dinerMenu = DinerMenu()
        val waitress = Waitress(pancakeHouseMenu, dinerMenu)

        // Without iterators
//        printMenu();

        // With iterators
        waitress.printMenu()
    }

    /*
	 * Without the Waitress, we need the code below...
	 */
    fun printMenu() {
        val pancakeHouseMenu = PancakeHouseMenu()
        val dinerMenu = DinerMenu()
        val breakfastItems: ArrayList<MenuItem> = pancakeHouseMenu.menuItems
        val lunchItems: Array<MenuItem?> = dinerMenu.menuItems

        // Hiding implementation
        println("USING FOR EACH")
        for (menuItem in breakfastItems) {
            System.out.print(menuItem.name)
            System.out.println("\t\t" + menuItem.price)
            System.out.println("\t" + menuItem.description)
        }
        for (menuItem in lunchItems) {
            System.out.print(menuItem!!.name)
            System.out.println("\t\t" + menuItem.price)
            System.out.println("\t" + menuItem.description)
        }

        // Exposing implementation
        println("USING FOR LOOPS")
        for (i in breakfastItems.indices) {
            val menuItem = breakfastItems[i]
            System.out.print(menuItem.name)
            System.out.println("\t\t" + menuItem.price)
            System.out.println("\t" + menuItem.description)
        }
        for (i in lunchItems.indices) {
            val menuItem = lunchItems[i]
            System.out.print(menuItem!!.name)
            System.out.println("\t\t" + menuItem.price)
            System.out.println("\t" + menuItem.description)
        }
    }
}


class Waitress(var pancakeHouseMenu: Menu, var dinerMenu: Menu) {
    fun printMenu() {
        val pancakeIterator = pancakeHouseMenu.createIterator()
        val dinerIterator = dinerMenu.createIterator()
        println("MENU\n----\nBREAKFAST")
        printMenu(pancakeIterator)
        println("\nLUNCH")
        printMenu(dinerIterator)
    }

    private fun printMenu(iterator: Iterator?) {
        while (iterator!!.hasNext()) {
            val menuItem = iterator.next()
            print(menuItem!!.name + ", ")
            print(menuItem.price.toString() + " -- ")
            System.out.println(menuItem.description)
        }
    }

    fun printVegetarianMenu() {
        printVegetarianMenu(pancakeHouseMenu.createIterator())
        printVegetarianMenu(dinerMenu.createIterator())
    }

    fun isItemVegetarian(name: String): Boolean {
        val breakfastIterator = pancakeHouseMenu.createIterator()
        if (isVegetarian(name, breakfastIterator)) {
            return true
        }
        val dinnerIterator = dinerMenu.createIterator()
        return isVegetarian(name, dinnerIterator)
    }

    private fun printVegetarianMenu(iterator: Iterator?) {
        while (iterator!!.hasNext()) {
            val menuItem = iterator.next()
            if (menuItem!!.isVegetarian) {
                System.out.print(menuItem.name)
                System.out.println("\t\t" + menuItem.price)
                System.out.println("\t" + menuItem.description)
            }
        }
    }

    private fun isVegetarian(name: String, iterator: Iterator?): Boolean {
        while (iterator!!.hasNext()) {
            val menuItem = iterator.next()
            if (menuItem!!.name == name) {
                if (menuItem.isVegetarian) {
                    return true
                }
            }
        }
        return false
    }
}

//
//class CafeMenu : Menu {
//    var menuItems = HashMap<String, MenuItem>()
//    fun addItem(
//        name: String?, description: String?,
//        vegetarian: Boolean, price: Double
//    ) {
//        val menuItem = MenuItem(name!!, description!!, vegetarian, price)
//        menuItems[menuItem.name] = menuItem
//    }
//
//    val items: Map<String, MenuItem>
//        get() = menuItems
//
//    override fun createIterator(): Iterator<MenuItem> {
//        return menuItems.values.iterator()
//    }
//
//    init {
//        addItem(
//            "Veggie Burger and Air Fries",
//            "Veggie burger on a whole wheat bun, lettuce, tomato, and fries",
//            true, 3.99
//        )
//        addItem(
//            "Soup of the day",
//            "A cup of the soup of the day, with a side salad",
//            false, 3.69
//        )
//        addItem(
//            "Burrito",
//            "A large burrito, with whole pinto beans, salsa, guacamole",
//            true, 4.29
//        )
//    }
//}
