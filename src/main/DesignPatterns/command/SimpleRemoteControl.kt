package main.DesignPatterns.command

//
// This is the invoker
//
class SimpleRemoteControl {
    var slot: Command? = null //must have one to hold command, which controls one device

    //method for setting the command slot will control 
    //could be called multiple times - i.e chg behaviour of button
    fun setCommand(command: Command?) { 
        slot = command
    }

    //method is called when button is pushed
    //all we do is take current command bound to slot & call execute() method
    fun buttonWasPressed() {
        slot!!.execute()
    }
}