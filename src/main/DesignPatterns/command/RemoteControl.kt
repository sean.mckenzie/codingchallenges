package main.DesignPatterns.command

//
// invoker
//
class RemoteControl {
    var onCommands: Array<Command?>
    var offCommands: Array<Command?>
    
    fun setCommand(slot: Int, onCommand: Command?, offCommand: Command?) {
        onCommands[slot] = onCommand
        offCommands[slot] = offCommand
    }

    fun onButtonWasPushed(slot: Int) {
        onCommands[slot]!!.execute()
    }

    fun offButtonWasPushed(slot: Int) {
        offCommands[slot]!!.execute()
    }

    override fun toString(): String {
        val stringBuff = StringBuffer()
        stringBuff.append("\n------ Remote Control -------\n")
        for (i in onCommands.indices) {
            val on = try { onCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            val off = try { offCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            stringBuff.append(
                """[slot $i] $on    $off
"""
            )
        }
        return stringBuff.toString()
    }

    init {
        onCommands = arrayOfNulls(7)
        offCommands = arrayOfNulls(7)
        val noCommand: NoCommand? = null
//        val noCommand: Command? = null
        for (i in 0..6) {
            onCommands[i] = noCommand
            offCommands[i] = noCommand
        }
    }
}

class RemoteControlWithUndo {
    var onCommands: Array<Command?>
    var offCommands: Array<Command?>
    var undoCommand: Command? = null

    fun setCommand(slot: Int, onCommand: Command?, offCommand: Command?) {
        onCommands[slot] = onCommand
        offCommands[slot] = offCommand
    }

    fun onButtonWasPushed(slot: Int) {
        onCommands[slot]!!.execute()
        undoCommand = onCommands[slot]
    }

    fun offButtonWasPushed(slot: Int) {
        offCommands[slot]!!.execute()
        undoCommand = offCommands[slot]
    }

    fun undoButtonWasPushed() {
        undoCommand!!.undo()
    }

    override fun toString(): String {
        val stringBuff = StringBuffer()
        stringBuff.append("\n------ Remote Control -------\n")
        for (i in onCommands.indices) {
            val on = try { onCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            val off = try { offCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            stringBuff.append(
                """[slot $i] $on    $off
"""
            )
        }
        val undo = try { undoCommand!!.name } catch(e: Exception) {"undo is empty"}

        stringBuff.append("""[undo] $undo""")
        return stringBuff.toString()
    }

    init {
        onCommands = arrayOfNulls(7)
        offCommands = arrayOfNulls(7)
        val noCommand: NoCommand? = null
//        val noCommand: Command? = null
        for (i in 0..6) {
            onCommands[i] = noCommand
            offCommands[i] = noCommand
        }
        undoCommand = noCommand
    }
}

class RemoteControlWithMacro {
    var onCommands: Array<MacroCommand?>
    var offCommands: Array<MacroCommand?>
    var undoCommand: MacroCommand? = null

    fun setCommand(slot: Int, onCommand: MacroCommand?, offCommand: MacroCommand?) {
        onCommands[slot] = onCommand
        offCommands[slot] = offCommand
    }

    fun onButtonWasPushed(slot: Int) {
        onCommands[slot]!!.execute()
        undoCommand = onCommands[slot]
    }

    fun offButtonWasPushed(slot: Int) {
        offCommands[slot]!!.execute()
        undoCommand = offCommands[slot]
    }

    fun undoButtonWasPushedHere() {
        undoCommand!!.undo()
    }

    override fun toString(): String {
        val stringBuff = StringBuffer()
        stringBuff.append("\n------ Remote Control -------\n")
        for (i in onCommands.indices) {
            val on = try { onCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            val off = try { offCommands[i]!!.name } catch(e: Exception) {"slot is empty"}
            stringBuff.append(
                """[slot $i] $on    $off
"""
            )
        }
        val undo = try { undoCommand!!.name } catch(e: Exception) {"undo is empty"}

        stringBuff.append("""[undo] $undo""")
        return stringBuff.toString()
    }

    init {
        onCommands = arrayOfNulls(7)
        offCommands = arrayOfNulls(7)
//        val noCommand: NoCommand? = null
        val noCommand: MacroCommand? = null
        for (i in 0..6) {
            onCommands[i] = noCommand
            offCommands[i] = noCommand
        }
        undoCommand = noCommand
    }
}

class NoCommand() : Command {
    override fun execute() { }
    override fun undo() { }
}
