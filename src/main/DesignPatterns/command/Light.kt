package main.DesignPatterns.command

class Light(location: String = "") {
    var location = ""
    fun name() {
        println("$location light")
    }
    
    fun on() {
        println("$location light is on")
    }

    fun off() {
        println("$location light is off")
    }

    init {
        this.location = location
    }
}
