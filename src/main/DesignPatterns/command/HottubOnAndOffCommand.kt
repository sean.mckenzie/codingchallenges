package main.DesignPatterns.command

class HottubOnCommand(var hottub: Hottub) : Command {
    override fun execute() {
        hottub.on()
        hottub.setTemperature(104)
        hottub.circulate()
    }

    override fun undo() {
        hottub.setTemperature(98)
        hottub.off()
        hottub.circulate()
    }
    override val name get() = hottub.toString() + " on"
}

class HottubOffCommand(var hottub: Hottub) : Command {
    override fun execute() {
        hottub.setTemperature(98)
        hottub.off()
    }

    override fun undo() {
        hottub.on()
        hottub.setTemperature(104)
        hottub.circulate()
    }
    override val name get() = hottub.toString() + " off"
}
