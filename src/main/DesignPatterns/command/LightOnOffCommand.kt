package main.DesignPatterns.command

class LightOnCommand(light: Light) : Command {
    var light: Light
    override fun execute() {
        light.on()
        //execute method calls on() method on receiving object
        //which is the light we are controlling
        //invoke the action(s) of the receiver needed to fulfil request 
        //UNDO()  tbc
    }

    override val name get() = light.location + " light on"

    override fun undo() {
        light.off()
    }
    
    init {
        this.light = light 
        //constructor is passed specific light the command is going to control
        //say living room light and stashes it in light variable
        //when execute called this is light that will be Receiver of request
        // 
        // This is the receiver 
        // 
    }
}

class LightOffCommand(light: Light) : Command {
    var light: Light
    override fun execute() {
        light.off()
    }

    override fun undo() {
        light.on()
    }
    
    override val name get() = light.location + " light off"

    init {
        this.light = light
    }
}

