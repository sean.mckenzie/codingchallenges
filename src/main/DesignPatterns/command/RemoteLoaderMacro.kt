package main.DesignPatterns.command

object RemoteLoaderMacro {
    @JvmStatic
    fun main(args: Array<String>) {
        val remoteControl = RemoteControlWithMacro()
        val light = Light("Living Room")
        val tv = TV("Living Room")
        val stereo = Stereo("Living Room")
        val hottub = Hottub()
        val lightOn = LightOnCommand(light)
        val stereoOn = StereoOnWithCDCommand(stereo)
        val tvOn = TVOnCommand(tv)
        val hottubOn = HottubOnCommand(hottub)
        val lightOff = LightOffCommand(light)
        val stereoOff = StereoOffCommand(stereo)
        val tvOff = TVOffCommand(tv)
        val hottubOff = HottubOffCommand(hottub)
        val partyOn = arrayOf<Command>(lightOn, stereoOn, tvOn, hottubOn)
        val partyOff = arrayOf(lightOff, stereoOff, tvOff, hottubOff)
        val partyOnMacro = MacroCommand(partyOn)
        val partyOffMacro = MacroCommand(partyOff)
        remoteControl.setCommand(0, partyOnMacro, partyOffMacro)
        println(remoteControl)
        println("--- Pushing Macro On---")
        remoteControl.onButtonWasPushed(0)
        println("--- Pushing Macro Off---")
//        remoteControl.offButtonWasPushed(0)
        remoteControl.undoButtonWasPushedHere()
    }
}


//object RemoteLoaderMacro {
//    @JvmStatic
//    fun main(args: Array<String>) {
//        val remoteControl = RemoteControlWithUndo()
//        val livingRoomLight = Light("Living Room")
//        val kitchenLight = Light("Kitchen")
//        val ceilingFan = CeilingFan("Living Room")
//        val garageDoor = GarageDoor("")
//        val stereo = Stereo("Living Room")
//        val livingRoomLightOn = LightOnCommand(livingRoomLight)
//        val livingRoomLightOff = LightOffCommand(livingRoomLight)
//        val kitchenLightOn = LightOnCommand(kitchenLight)
//        val kitchenLightOff = LightOffCommand(kitchenLight)
//        val ceilingFanOn = CeilingFanOnCommand(ceilingFan)
//        val ceilingFanMedium = CeilingFanMediumCommand(ceilingFan)
//        val ceilingFanHigh = CeilingFanHighCommand(ceilingFan)
////        val ceilingFanLowCommand = CeilingFanLowCommand(ceilingFan)
//        val ceilingFanOff = CeilingFanOffCommand(ceilingFan)
//        val garageDoorUp = GarageDoorOpenCommand(garageDoor)
//        val garageDoorDown = GarageDoorCloseCommand(garageDoor)
//        val stereoOnWithCD = StereoOnWithCDCommand(stereo)
//        val stereoOff = StereoOffCommand(stereo)
//        remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff)
//        remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff)
//        remoteControl.setCommand(2, ceilingFanOn, ceilingFanOff)
//        remoteControl.setCommand(3, stereoOnWithCD, stereoOff)
//        remoteControl.setCommand(4, ceilingFanMedium, ceilingFanOff)
//        remoteControl.setCommand(5, ceilingFanHigh, ceilingFanOff)
//        println("remoteControlWIthUndo")
//        println(remoteControl)
//        remoteControl.onButtonWasPushed(0)
//        remoteControl.offButtonWasPushed(0)
//        println(remoteControl)
//        println("on off & just before undo")
//        remoteControl.undoButtonWasPushed()
//        remoteControl.offButtonWasPushed(0)
//        remoteControl.onButtonWasPushed(0)
//        println(remoteControl)
//        println("undo off 0 on and just before undo")
//        remoteControl.undoButtonWasPushed()
//        println(remoteControl)
//        println("after undo 0")
//        remoteControl.undoButtonWasPushed()
//        println("after undo 0 AGAIN - only remembers ONE last command")
//        
//        remoteControl.onButtonWasPushed(1)
//        remoteControl.offButtonWasPushed(1)
//        remoteControl.undoButtonWasPushed()
//        println("after undo 1")
//
//
//        remoteControl.onButtonWasPushed(2)
//        remoteControl.undoButtonWasPushed()
//        println("after undo 2")
//        remoteControl.offButtonWasPushed(2)
//        println("after off 2 - post undo of on 2")
//        
//        remoteControl.onButtonWasPushed(3)
//        remoteControl.offButtonWasPushed(3)
//        remoteControl.undoButtonWasPushed()
//        println("after undo 3")
//
//        //STATE TEST
//        println("state test med off undo")
//        remoteControl.onButtonWasPushed(4)
//        remoteControl.offButtonWasPushed(4)
//        println(remoteControl)
//        remoteControl.undoButtonWasPushed()
//        println(remoteControl)
//        println("high undo")
//        remoteControl.onButtonWasPushed(5)
//        remoteControl.undoButtonWasPushed()
//        println(remoteControl)
//        println("after undo 2")
//
//
//
//
//    }
//}
