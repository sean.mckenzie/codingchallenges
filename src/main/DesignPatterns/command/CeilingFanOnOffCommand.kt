package main.DesignPatterns.command

class CeilingFanOnCommand(var ceilingFan: CeilingFan) : Command {
    override fun execute() {
        ceilingFan.low()
    }

    override fun undo() {
        ceilingFan.off()
    }

    override val name get() = ceilingFan.location + " ceiling fan on high"

}

class CeilingFanOffCommand(var ceilingFan: CeilingFan) : Command {
    var prevSpeed = 0
    override fun execute() {
        prevSpeed = ceilingFan.speed
        ceilingFan.off()
    }

    override fun undo() {
        when (prevSpeed) {
            CeilingFan.HIGH -> ceilingFan.high()
            CeilingFan.MEDIUM -> ceilingFan.medium()
            CeilingFan.LOW -> ceilingFan.low()
            else -> ceilingFan.off()
        }
    }
        override val name get() = ceilingFan.location+" ceiling fan off"
}

class CeilingFanHighCommand(var ceilingFan: CeilingFan) : Command {
    var prevSpeed = 0
    override fun execute() {
        prevSpeed = ceilingFan.speed
        ceilingFan.high()
    }

    override fun undo() {
        when (prevSpeed) {
            CeilingFan.HIGH -> ceilingFan.high()
            CeilingFan.MEDIUM -> ceilingFan.medium()
            CeilingFan.LOW -> ceilingFan.low()
            else -> ceilingFan.off()
        }
    }
}

class CeilingFanMediumCommand(var ceilingFan: CeilingFan) : Command {
    var prevSpeed = 0
    override fun execute() {
        prevSpeed = ceilingFan.speed
        ceilingFan.medium()
    }

    override fun undo() {
        when (prevSpeed) {
            CeilingFan.HIGH -> ceilingFan.high()
            CeilingFan.MEDIUM -> ceilingFan.medium()
            CeilingFan.LOW -> ceilingFan.low()
            else -> ceilingFan.off()
        }
    }
}

