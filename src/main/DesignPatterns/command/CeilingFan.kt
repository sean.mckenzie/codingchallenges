package main.DesignPatterns.command

class CeilingFan(location: String) {
    var location = ""
    var speed = 0    //local state for speed of fan :. to undo need to know state!
    
    fun name() {
        println("$location ceiling fan")
    }

    fun high() {
        // turns the ceiling fan on to high
        speed = HIGH
        println("$location ceiling fan is on high")
    }

    fun medium() {
        // turns the ceiling fan on to medium
        speed = MEDIUM
        println("$location ceiling fan is on medium")
    }

    fun low() {
        // turns the ceiling fan on to low
        speed = LOW
        println("$location ceiling fan is on low")
    }

    fun off() {
        // turns the ceiling fan off
        speed = 0
        println("$location ceiling fan is off")
    }

//    fun getSpeed() = speed

    companion object {
        const val HIGH = 3
        const val MEDIUM = 2
        const val LOW = 1
    }

    init {
        this.location = location
    }
}
