package main.DesignPatterns.command

//
// client
//
object RemoteControlTest {
    @JvmStatic
    fun main(args: Array<String>) {
        val remote = SimpleRemoteControl() //*invoker* - passed a command object can be used to make requests
        val light = Light()                //create Light object - *Receiver* of request

        val lightOn = LightOnCommand(light) //create a *command* pass to *receiver*
        val lightOff = LightOffCommand(light)

        remote.setCommand(lightOn) //pass *command* to *invoker*
        remote.buttonWasPressed()  //simulate button press
        
        remote.setCommand(lightOff) //repeat - other *command*
        remote.buttonWasPressed()
        
        //repeat other receiver, command 
        val garageDoor = GarageDoor()  // *receiver*
        val garageOpen = GarageDoorOpenCommand(garageDoor) //*command*
        val garageClose = GarageDoorCloseCommand(garageDoor) //*command*
        remote.setCommand(garageOpen) //*invoker*
        remote.buttonWasPressed()
         
        remote.setCommand(garageClose) //*invoker*
        remote.buttonWasPressed()
    }
}
