package main.DesignPatterns.command

class Hottub {
    var on = false
    var temp = 0
    fun on() {
        on = true
    }

    fun off() {
        on = false
    }

    fun circulate() {
        if (on) {
            println("Hottub is bubbling!")
        } else {
            println("Hottub is OFF")
        }
    }

    fun jetsOn() {
        if (on) {
            println("Hottub jets are on")
        }
    }

    fun jetsOff() {
        if (on) {
            println("Hottub jets are off")
        }
    }

    fun setTemperature(temperature: Int) {
        if (temperature > this.temp) {
            println("Hottub is heating to a steaming $temperature degrees")
        } else {
            println("Hottub is cooling to $temperature degrees")
        }
        this.temp = temperature
    }
}
