package main.DesignPatterns.command

interface Command {
    fun execute()
    val name: String get() = execute().toString()
    
    fun undo()
//            try { execute().toString() } catch(e: Exception) { "null" }
}
            

/*
*The Command Pattern* encapsulates a request as an
object, thereby letting you parameterize other objects
with different requests, queue or log requests, and support
undoable operations

* encapsulated request ...
* execute() {
*   receiver.action()
* }
* 
* receiver gets the action()
* 
* command object binds/packages  actions/receiver into an object
* that exposes just one method ... execute() 
* which causes action to be invoked on receiver
* this was parameterised (i.e. garageDoorClose OR lightOn)
* 
* Meta Command Patter .. allows creation of macros of commands
* to execute multiple commands at once
* 
*/

class MacroCommand(var commands: Array<Command>) : Command {
    override fun execute() {
        for (i in commands.indices) {
            commands[i].execute()
        }
    }

    /**
     * NOTE:  these commands have to be done backwards to ensure
     * proper undo functionality
     */
    override fun undo() {
        for (i in commands.indices.reversed()) {
            println("undo ${commands[i]!!.name}")
            commands[i].undo()
        }
    }

}

