package main.DesignPatterns.command

class StereoOnWithCDCommand(var stereo: Stereo) : Command {
    override fun execute() {
        stereo.on()
        stereo.setCD()
        stereo.setVolume(11)
    }

    override fun undo() {
        stereo.off()
    }

    override val name get() = stereo.location + " stereo on"

}

class StereoOffCommand(var stereo: Stereo) : Command {
    override fun execute() {
        stereo.off()
    }
    
    override fun undo() {
        stereo.on()
        stereo.setCD()
        stereo.setVolume(11)
    }

    override val name get() = stereo.location + " stereo off"

}
