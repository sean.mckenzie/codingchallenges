package main.DesignPatterns.command

class TVOnCommand(var tv: TV) : Command {
    override fun execute() {
        tv.on()
        tv.setInputChannel()
    }

    override fun undo() {
        tv.off()
    }

}

class TVOffCommand(var tv: TV) : Command {
    override fun execute() {
        tv.off()
    }

    override fun undo() {
        tv.on()
    }

}
