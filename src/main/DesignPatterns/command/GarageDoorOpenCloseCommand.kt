package main.DesignPatterns.command

class GarageDoorOpenCommand(garageDoor: GarageDoor) : Command {
    var garageDoor: GarageDoor
    override fun execute() {
        garageDoor.open()
        //execute method calls on() method on receiving object
        //which is the light we are controlling
    }
    override fun undo() {
        garageDoor.close()
    }



    init {
        this.garageDoor = garageDoor 
    }
}

class GarageDoorCloseCommand(garageDoor: GarageDoor) : Command {
    var garageDoor: GarageDoor
    override fun execute() {
        garageDoor.close()
    }
    
    override fun undo() {
        garageDoor.open()
    }

    init {
        this.garageDoor = garageDoor
    }
}

