package main.DesignPatterns.command

class GarageDoor(location: String = "") {
    var location = ""

    fun name() {
        println("$location garage door")
    }

    fun open() {
        println("$location garageDoor is open")
    }

    fun close() {
        println("$location garageDoor is closed")
    }

    init {
        this.location = location
    }
}
