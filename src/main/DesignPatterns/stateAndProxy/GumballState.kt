package main.DesignPatterns.state

import java.util.*

interface State {
    fun insertQuarter()
    fun ejectQuarter()
    fun turnCrank()
    fun dispense()
    fun refill()
}

class HasQuarterState(var gumballMachine: GumballMachine) : State {
    var randomWinner = Random(System.currentTimeMillis())
    override fun insertQuarter() {
        println("You can't insert another quarter")
    }

    override fun ejectQuarter() {
        println("Quarter returned")
        gumballMachine.setState(gumballMachine.getNoQuarterState())
    }

    override fun turnCrank() {
        println("You turned...")
        val winner = randomWinner.nextInt(10)
        if (winner == 0 && gumballMachine.count > 1) {
            gumballMachine.setState(gumballMachine.getWinnerState())
        } else {
            gumballMachine.setState(gumballMachine.getSoldState())
        }
    }

    override fun dispense() {
        println("No gumball dispensed")
    }

    override fun refill() {}
    override fun toString(): String {
        return "waiting for turn of crank"
    }
}


class NoQuarterState(var gumballMachine: GumballMachine) : State {
    override fun insertQuarter() {
        println("You inserted a quarter")
        gumballMachine.setState(gumballMachine.getHasQuarterState())
//        println("get state")
//        gumballMachine.getState().also(::println)
//        println(gumballMachine.state)
    }

    override fun ejectQuarter() {
        println("You haven't inserted a quarter")
    }

    override fun turnCrank() {
        println("You turned, but there's no quarter")
    }

    override fun dispense() {
        println("You need to pay first")
    }

    override fun refill() {}
    override fun toString(): String {
        return "waiting for quarter"
    }
}

class SoldOutState(var gumballMachine: GumballMachine) : State {
    override fun insertQuarter() {
        println("You can't insert a quarter, the machine is sold out")
    }

    override fun ejectQuarter() {
        println("You can't eject, you haven't inserted a quarter yet")
    }

    override fun turnCrank() {
        println("You turned, but there are no gumballs")
    }

    override fun dispense() {
        println("No gumball dispensed")
    }

    override fun refill() {
        gumballMachine.setState(gumballMachine.getNoQuarterState())
    }

    override fun toString(): String {
        return "sold out"
    }
}

class SoldState(var gumballMachine: GumballMachine) : State {
    override fun insertQuarter() {
        println("Please wait, we're already giving you a gumball")
    }

    override fun ejectQuarter() {
        println("Sorry, you already turned the crank")
    }

    override fun turnCrank() {
        println("Turning twice doesn't get you another gumball!")
    }

    override fun dispense() {
        gumballMachine.releaseBall()
        if (gumballMachine.count > 0) {
            gumballMachine.setState(gumballMachine.getNoQuarterState())
        } else {
            println("Oops, out of gumballs!")
            gumballMachine.setState(gumballMachine.getSoldOutState())
        }
    }

    override fun refill() {}
    override fun toString(): String {
        return "dispensing a gumball"
    }
}

class WinnerState(var gumballMachine: GumballMachine) : State {
    override fun insertQuarter() {
        println("Please wait, we're already giving you a Gumball")
    }

    override fun ejectQuarter() {
        println("Please wait, we're already giving you a Gumball")
    }

    override fun turnCrank() {
        println("Turning again doesn't get you another gumball!")
    }

    override fun dispense() {
        gumballMachine.releaseBall()
        if (gumballMachine.count === 0) {
            gumballMachine.setState(gumballMachine.getSoldOutState())
        } else {
            gumballMachine.releaseBall()
            println("YOU'RE A WINNER! You got two gumballs for your quarter")
            if (gumballMachine.count > 0) {
                gumballMachine.setState(gumballMachine.getNoQuarterState())
            } else {
                println("Oops, out of gumballs!")
                gumballMachine.setState(gumballMachine.getSoldOutState())
            }
        }
    }

    override fun refill() {}
    override fun toString(): String {
        return "despensing two gumballs for your quarter, because YOU'RE A WINNER!"
    }
}


class GumballMonitor(var machine: GumballMachine) {
    fun report() {
        println("Gumball Machine: " + machine.location)
        println("Current inventory: " + machine.count.toString() + " gumballs")
        println("Current state: " + machine.getState())
    }
}

class GumballMachine(location: String, numberGumballs: Int) {
    var soldOutState: State
    var noQuarterState: State
    var hasQuarterState: State
    var soldState: State
    var winnerState: State
    var state: State? = null
    var count = 0
    var location: String

    init {
        soldOutState = SoldOutState(this)
        noQuarterState = NoQuarterState(this)
        hasQuarterState = HasQuarterState(this)
        soldState = SoldState(this)
        winnerState = WinnerState(this)
        count = numberGumballs
        state = if (numberGumballs > 0) {
            noQuarterState
        } else {
            soldOutState
        }
        this.location = location
    }

    fun insertQuarter() {
        state!!.insertQuarter()
    }

    fun ejectQuarter() {
        state!!.ejectQuarter()
    }

    fun turnCrank() {
        state!!.turnCrank()
        state!!.dispense()
    }

    fun releaseBall() {
        println("A gumball comes rolling out the slot...")
        if (count != 0) {
            count = count - 1
        }
    }

    fun refill(count: Int) {
        this.count += count
        println("The gumball machine was just refilled; it's new count is: " + this.count)
        state!!.refill()
    }

    
    internal fun setState(state: State?) {
        this.state = state
    }

    internal fun getState(): State? {
        return state
    }

    internal fun getSoldOutState(): State {
        return soldOutState
    }

    internal fun getNoQuarterState(): State {
        return noQuarterState
    }

    internal fun getHasQuarterState(): State {
        return hasQuarterState
    }

    internal fun getSoldState(): State {
        return soldState
    }

    internal fun getWinnerState(): State {
        return winnerState
    }

    override fun toString(): String {
        val result = StringBuffer()
        result.append("\nMighty Gumball, Inc.")
        result.append("\nJava-enabled Standing Gumball Model #2004")
        result.append("\nInventory: $count gumball")
        if (count != 1) {
            result.append("s")
        }
        result.append("\n")
        result.append("Machine is $state\n")
        return result.toString()
    }

}

object GumballMachineTestDrive {
    @JvmStatic
    fun main(args: Array<String>) {
        val gumballMachine = GumballMachine("one",2)
        System.out.println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        System.out.println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.refill(5)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
    }
}


object GumballMachineMonitorTestDrive {
    @JvmStatic
    
    fun main(args: Array<String>) {
        var args= arrayOf("Seattle", "112")
        var count = 0
        if (args.size < 2) {
            println("GumballMachine <name> <inventory>")
            System.exit(1)
        }
        try {
            count = args[1].toInt()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            System.exit(1)
        }
        val gumballMachine = GumballMachine(args[0], count)
        val monitor = GumballMonitor(gumballMachine)
        println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        gumballMachine.insertQuarter()
        gumballMachine.turnCrank()
        println(gumballMachine)
        monitor.report()
    }
}
