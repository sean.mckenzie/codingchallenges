package main.DesignPatterns.stateAndProxy


//class GumballMachineOrig(count: Int) {
//    var stateOrig = SOLD_OUT
//    var countOrig = 0
//    fun insertQuarter() {
//        if (stateOrig == HAS_QUARTER) {
//            println("You can't insert another quarter")
//        } else if (stateOrig == NO_QUARTER) {
//            stateOrig = HAS_QUARTER
//            println("You inserted a quarter")
//        } else if (stateOrig == SOLD_OUT) {
//            println("You can't insert a quarter, the machine is sold out")
//        } else if (stateOrig == SOLD) {
//            println("Please wait, we're already giving you a gumball")
//        }
//    }
//
//    fun ejectQuarter() {
//        if (stateOrig == HAS_QUARTER) {
//            println("Quarter returned")
//            stateOrig = NO_QUARTER
//        } else if (stateOrig == NO_QUARTER) {
//            println("You haven't inserted a quarter")
//        } else if (stateOrig == SOLD) {
//            println("Sorry, you already turned the crank")
//        } else if (stateOrig == SOLD_OUT) {
//            println("You can't eject, you haven't inserted a quarter yet")
//        }
//    }
//
//    fun turnCrank() {
//        if (stateOrig == SOLD) {
//            println("Turning twice doesn't get you another gumball!")
//        } else if (stateOrig == NO_QUARTER) {
//            println("You turned but there's no quarter")
//        } else if (stateOrig == SOLD_OUT) {
//            println("You turned, but there are no gumballs")
//        } else if (stateOrig == HAS_QUARTER) {
//            println("You turned...")
//            stateOrig = SOLD
//            dispense()
//        }
//    }
//
//    private fun dispense() {
//        if (stateOrig == SOLD) {
//            println("A gumball comes rolling out the slot")
//            countOrig = countOrig - 1
//            stateOrig = if (countOrig == 0) {
//                println("Oops, out of gumballs!")
//                SOLD_OUT
//            } else {
//                NO_QUARTER
//            }
//        } else if (stateOrig == NO_QUARTER) {
//            println("You need to pay first")
//        } else if (stateOrig == SOLD_OUT) {
//            println("No gumball dispensed")
//        } else if (stateOrig == HAS_QUARTER) {
//            println("No gumball dispensed")
//        }
//    }
//
//    fun refill(numGumBalls: Int) {
//        countOrig = numGumBalls
//        stateOrig = NO_QUARTER
//    }
//
//    override fun toString(): String {
//        val result = StringBuffer()
//        result.append("\nMighty Gumball, Inc.")
//        result.append("\nJava-enabled Standing Gumball Model #2004\n")
//        result.append("Inventory: $countOrig gumball")
//        if (countOrig != 1) {
//            result.append("s")
//        }
//        result.append("\nMachine is ")
//        if (stateOrig == SOLD_OUT) {
//            result.append("sold out")
//        } else if (stateOrig == NO_QUARTER) {
//            result.append("waiting for quarter")
//        } else if (stateOrig == HAS_QUARTER) {
//            result.append("waiting for turn of crank")
//        } else if (stateOrig == SOLD) {
//            result.append("delivering a gumball")
//        }
//        result.append("\n")
//        return result.toString()
//    }
//
//    companion object {
//        const val SOLD_OUT = 0
//        const val NO_QUARTER = 1
//        const val HAS_QUARTER = 2
//        const val SOLD = 3
//    }
//
//    init {
//        this.countOrig = count
//        if (count > 0) {
//            stateOrig = NO_QUARTER
//        }
//    }
//}


//object GumballMachineTestDrive {
//    @JvmStatic
//    fun main(args: Array<String>) {
//        val gumballMachine = GumballMachineOrig(5)
//        System.out.println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        System.out.println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.ejectQuarter()
//        gumballMachine.turnCrank()
//        System.out.println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.ejectQuarter()
//        System.out.println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        System.out.println(gumballMachine)
//    }
//}


