package main.DesignPatterns.stateAndProxy

import java.lang.reflect.InvocationHandler
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.lang.reflect.Proxy

interface PersonBean {
    var name: String?
    var gender: String?
    var interests: String?
    var hotOrNotRating: Int
}

class PersonBeanImpl : PersonBean {
    override var name: String? = null
    override var gender: String? = null
    override var interests: String? = null
    var rating = 0
    var ratingCount = 0
    override var hotOrNotRating: Int
        get() = if (ratingCount == 0) 0 else rating / ratingCount
        set(rating) {
            this.rating += rating
            ratingCount++
        }
}


class NonOwnerInvocationHandler(var person: PersonBean) : InvocationHandler {
    @Throws(IllegalAccessException::class)
    override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any? {
        try {
            if (method.name.startsWith("get")) {
                return method.invoke(person, args)
            } else if (method.name == "setHotOrNotRating") {
                return method.invoke(person, args)
            } else if (method.name.startsWith("set")) {
                throw IllegalAccessException()
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        return null
    }
}

class OwnerInvocationHandler(var person: PersonBean) : InvocationHandler {
    @Throws(IllegalAccessException::class)
    override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any? {
        try {
            if (method.name.startsWith("get")) {
                return method.invoke(person, args)
            } else if (method.name == "setHotOrNotRating") {
                throw IllegalAccessException()
            } else if (method.name.startsWith("set")) {
                return method.invoke(person, args)
            }
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        return null
    }
}


class MatchMakingTestDrive {
    var datingDB = HashMap<String, PersonBean>()
    fun drive() {
        val joe = getPersonFromDatabase("Joe Javabean")
        val ownerProxy = getOwnerProxy(joe!!)
//        println("Name is " + ownerProxy!!.name.toString())                           //why?
        ownerProxy!!.interests = "bowling, Go"
        println("Interests set from owner proxy")
        try {
            ownerProxy.hotOrNotRating = 10
        } catch (e: Exception) {
            println("Can't set rating from owner proxy")
        }
        System.out.println("Rating is " + ownerProxy!!.hotOrNotRating.toString())
        val nonOwnerProxy = getNonOwnerProxy(joe!!)
        System.out.println("Name is " + nonOwnerProxy!!.name.toString())
        try {
            nonOwnerProxy.interests = "bowling, Go"
        } catch (e: Exception) {
            println("Can't set interests from non owner proxy")
        }
        nonOwnerProxy.hotOrNotRating = 3
        println("Rating set from non owner proxy")
        System.out.println("Rating is " + nonOwnerProxy.hotOrNotRating)
    }


    fun getOwnerProxy(person: PersonBean): PersonBean? {
        return Proxy.newProxyInstance(
            person.javaClass.classLoader,
            person.javaClass.interfaces,
            OwnerInvocationHandler(person)
        ) as PersonBean
    }

    fun getNonOwnerProxy(person: PersonBean): PersonBean? {
        return Proxy.newProxyInstance(
            person.javaClass.classLoader,
            person.javaClass.interfaces,
            NonOwnerInvocationHandler(person)
        ) as PersonBean
    }
    

    fun getPersonFromDatabase(name: String?): PersonBean? {
        return datingDB[name]
    }

    fun initializeDatabase() {
        val joe: PersonBean = PersonBeanImpl()
        joe.name = "Joe Javabean"
        joe.interests = "cars, computers, music"
        joe.hotOrNotRating = 7
        datingDB[joe!!.name.toString()] = joe
        val kelly: PersonBean = PersonBeanImpl()
        kelly.name = "Kelly Klosure"
        kelly.interests = "ebay, movies, music"
        kelly.hotOrNotRating = 6
        datingDB[kelly!!.name.toString()] = kelly
        
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val test = MatchMakingTestDrive()
            test.drive()
        }
    }

    init {
        initializeDatabase()
    }
}


