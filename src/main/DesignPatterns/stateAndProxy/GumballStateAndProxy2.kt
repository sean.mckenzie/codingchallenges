//package main.DesignPatterns.stateAndProxy
//
//import java.io.Serializable
//import java.util.*
//
//
//class GumballMachine(location: String, count: Int) {
//    private var soldOutState: State? = null
//    private var noQuarterState: State
//    private var hasQuarterState: State
//    private var soldState: State
//    private var winnerState: State
//    private var state: State? = soldOutState
//    var count = 0
//    var location: String
//
//    fun insertQuarter() {
//        state!!.insertQuarter()
//    }
//
//    fun ejectQuarter() {
//        state!!.ejectQuarter()
//    }
//
//    fun turnCrank() {
//        state!!.turnCrank()
//        state!!.dispense()
//    }
//
//    fun setState(state: State) {
//        this.state = state
//    }
//
//    fun releaseBall() {
//        println("A gumball comes rolling out the slot...")
//        if (count != 0) {
//            count = count - 1
//        }
//    }
//
//    fun refill(count: Int) {
//        this.count = count
//        state = noQuarterState
//    }
//
//    fun getState(): State? {
//        return state
//    }
//
//    fun getSoldOutState(): State? {
//        return soldOutState
//    }
//
//    fun getNoQuarterState(): State? {
//        return noQuarterState
//    }
//
//    fun getHasQuarterState(): State? {
//        return hasQuarterState
//    }
//
//    fun getSoldState(): State? {
//        return soldState
//    }
//
//    fun getWinnerState(): State? {
//        return winnerState
//    }
//
//    override fun toString(): String {
//        val result = StringBuffer()
//        result.append("\nMighty Gumball, Inc.")
//        result.append("\nJava-enabled Standing Gumball Model #2004")
//        result.append("\nInventory: $count gumball")
//        if (count != 1) {
//            result.append("s")
//        }
//        result.append("\n")
//        result.append("Machine is $state\n")
//        return result.toString()
//    }
//
//    init {
//        soldOutState = SoldOutState(this)
//        noQuarterState = NoQuarterState(this)
//        hasQuarterState = HasQuarterState(this)
//        soldState = SoldState(this)
//        winnerState = WinnerState(this)
//        this.count = count
//        if (count > 0) {
//            state = noQuarterState
//        }
//        this.location = location
//    }
//}
//
//class WinnerState(var gumballMachine: GumballMachine) : State {
//    override fun insertQuarter() {
//        println("Please wait, we're already giving you a Gumball")
//    }
//
//    override fun ejectQuarter() {
//        println("Please wait, we're already giving you a Gumball")
//    }
//
//    override fun turnCrank() {
//        println("Turning again doesn't get you another gumball!")
//    }
//
//    override fun dispense() {
//        println("YOU'RE A WINNER! You get two gumballs for your quarter")
//        try {
//            gumballMachine.releaseBall()
//            if (gumballMachine.count === 0) {
//                gumballMachine.getSoldOutState()?.let { gumballMachine.setState(it) }
//            } else {
//                gumballMachine.releaseBall()
//                if (gumballMachine.count > 0) {
//                    gumballMachine.getNoQuarterState()?.let { gumballMachine.setState(it) }
//                } else {
//                    println("Oops, out of gumballs!")
//                    gumballMachine.getSoldOutState()?.let { gumballMachine.setState(it) }
//                }
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun toString(): String {
//        return "despensing two gumballs for your quarter, because YOU'RE A WINNER!"
//    }
//
//    companion object {
//        private const val serialVersionUID = 2L
//    }
//}
//
//class HasQuarterState(var gumballMachine: GumballMachine) : State {
//    var randomWinner = Random(System.currentTimeMillis())
//    override fun insertQuarter() {
//        println("You can't insert another quarter")
//    }
//
//    override fun ejectQuarter() {
//        println("Quarter returned")
//        gumballMachine.getNoQuarterState()?.let { gumballMachine.setState(it) }
//    }
//
//    override fun turnCrank() {
//        println("You turned...")
//        val winner = randomWinner.nextInt(10)
//        if (winner == 0) {
//            gumballMachine.getWinnerState()?.let { gumballMachine.setState(it) }
//        } else {
//            gumballMachine.getSoldState()?.let { gumballMachine.setState(it) }
//        }
//    }
//
//    override fun dispense() {
//        println("No gumball dispensed")
//    }
//
//    override fun toString(): String {
//        return "waiting for turn of crank"
//    }
//
//    companion object {
//        private const val serialVersionUID = 2L
//    }
//}
//
//
//class NoQuarterState(var gumballMachine: GumballMachine) : State {
//    override fun insertQuarter() {
//        println("You inserted a quarter")
//        gumballMachine.getHasQuarterState()?.let { gumballMachine.setState(it) }
//    }
//
//    override fun ejectQuarter() {
//        println("You haven't inserted a quarter")
//    }
//
//    override fun turnCrank() {
//        println("You turned, but there's no quarter")
//    }
//
//    override fun dispense() {
//        println("You need to pay first")
//    }
//
//    override fun toString(): String {
//        return "waiting for quarter"
//    }
//
//    companion object {
//        private const val serialVersionUID = 2L
//    }
//}
//
//
//class SoldOutState(var gumballMachine: GumballMachine) : State {
//    override fun insertQuarter() {
//        println("You can't insert a quarter, the machine is sold out")
//    }
//
//    override fun ejectQuarter() {
//        println("You can't eject, you haven't inserted a quarter yet")
//    }
//
//    override fun turnCrank() {
//        println("You turned, but there are no gumballs")
//    }
//
//    override fun dispense() {
//        println("No gumball dispensed")
//    }
//
//    override fun toString(): String {
//        return "sold out"
//    }
//
//    companion object {
//        private const val serialVersionUID = 2L
//    }
//}
//
//
//class SoldState(var gumballMachine: GumballMachine) : State {
//    override fun insertQuarter() {
//        println("Please wait, we're already giving you a gumball")
//    }
//
//    override fun ejectQuarter() {
//        println("Sorry, you already turned the crank")
//    }
//
//    override fun turnCrank() {
//        println("Turning twice doesn't get you another gumball!")
//    }
//
//    override fun dispense() {
//        gumballMachine.releaseBall()
//        try {
//            if (gumballMachine.count > 0) {
//                gumballMachine.getNoQuarterState()?.let { gumballMachine.setState(it) }
//            } else {
//                println("Oops, out of gumballs!")
//                gumballMachine.getSoldOutState()?.let { gumballMachine.setState(it) }
//            }
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun toString(): String {
//        return "dispensing a gumball"
//    }
//
//    companion object {
//        private const val serialVersionUID = 2L
//    }
//}
//
//
//interface State : Serializable {
//    fun insertQuarter()
//    fun ejectQuarter()
//    fun turnCrank()
//    fun dispense()
//}
//
//
//class GumballMonitor(var machine: GumballMachine) {
//    fun report() {
//        println("Gumball Machine: " + machine.location)
//        println("Current inventory: " + machine.count.toString() + " gumballs")
//        println("Current state: " + machine.getState())
//    }
//}
//
//
//object GumballMachineTestDrive {
//    @JvmStatic
//    fun main(args: Array<String>) {
//        var count = 0
//        if (args.size < 2) {
//            println("GumballMachine <name> <inventory>")
//            System.exit(1)
//        }
//        try {
//            count = args[1].toInt()
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//            System.exit(1)
//        }
//        val gumballMachine = GumballMachine(args[0], count)
//        val monitor = GumballMonitor(gumballMachine)
//        println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        println(gumballMachine)
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        gumballMachine.insertQuarter()
//        gumballMachine.turnCrank()
//        println(gumballMachine)
//        monitor.report()
//    }
//}
//
