package main

class Challenge5 {

    fun getGridStatus(board: List<String>): String {

        val allStrings = getAllStrings(board.map { it.toLowerCase() })

        return when {
            confirmFourConnected(allStrings, "r") -> "Red Wins"
            confirmFourConnected(allStrings, "y") -> "Yellow Wins"
            boardIsFull(board) -> "Draw"
            redPlaysNext(board) -> "Red plays next"
            else -> "Yellow plays next"
        }
    }


    fun getAllStrings(board: List<String>): List<String> {
        val across = board[0].lastIndex
        val down = board.count() - 1

        val allAcrossStrings: List<String> = board
        val allDownStrings: List<String> = parseDownStrings(board, across, down)
        val allDiagonals: List<String> = parseDiagonalStrings(board, across, down)

        return allAcrossStrings + allDownStrings + allDiagonals
    }


    fun parseDownStrings(board: List<String>, across: Int, down: Int): List<String> {
        var listDown: List<String> = emptyList()

        for (column in 0..across) {
            var createString = ""
            for (row in 0..down) {
                createString += board[row].get(column)
            }
            listDown += createString
        }

        return listDown
    }


    fun parseDiagonalStrings(board: List<String>, across: Int, down: Int): List<String> {
        var diagonals: List<String> = emptyList()

        for (column in 0..across) {
            diagonals = buildDiagonalString(board, column, 0, 1, 1, diagonals)
        }

        for (row in 1..down) {
            diagonals = buildDiagonalString(board, 0, row, 1, 1, diagonals)
        }

        for (column in 0..across) {
            diagonals = buildDiagonalString(board.asReversed(), column, 0, 1, 1, diagonals)
        }

        for (row in 1..down) {
            diagonals = buildDiagonalString(board.asReversed(), 0, row, 1, 1, diagonals)
        }

        return diagonals.filter { it.length >= 4 }
    }


    private fun buildDiagonalString(
        board: List<String>, column: Int, row: Int, amendColumn: Int, amendRow: Int, diagonals: List<String>
    ): List<String> {
        val updatedDiagonals = diagonals.toMutableList()
        var createString = ""

        createString += getDiagonal(board, column, row, amendColumn, amendRow)
        updatedDiagonals += createString

        return updatedDiagonals
    }

    private fun getDiagonal(board: List<String>, column: Int, row: Int, amendColumn: Int, amendRow: Int): String? {
        var createDiagonalString = ""
        var getColumn = column
        var getRow = row

        while (getRow < board.count()) {
            createDiagonalString +=
                try { board[getRow].get(getColumn) }
                catch (e: Exception) { "" }

            getColumn += amendColumn
            getRow += amendRow

        }
        return createDiagonalString
    }

    fun boardIsFull(board: List<String>): Boolean {
        return (!board.toString().contains("."))
    }

    fun redPlaysNext(board: List<String>): Boolean {
        return (board.toString().contains("Y"))
    }


    fun confirmFourConnected(allStrings: List<String>, letter: String): Boolean {
        var found = false

        allStrings.forEach {
            if (findFourConnected(it, letter))
                found = true
        }

        return found
    }


    fun findFourConnected(input: String, letter: String): Boolean {
        return (fourLetters(input.toLowerCase(), letter) && connectedLetters(input.toLowerCase(), letter))
    }

    private fun fourLetters(inputLowerCase: String, letter: String) =
        inputLowerCase.count { letter.contains(it) } == 4

    private fun connectedLetters(inputLowerCase: String, letter: String) =
        inputLowerCase.lastIndexOf(letter) - inputLowerCase.indexOf(letter) == 3
}

