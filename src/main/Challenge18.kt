package main

class Node(val item: Any, var next: Node? = null) {

    fun first(): Node? = head
    fun last(): Node? {
        var node = head
        return node?.let {
            while (it.next != null) {
                node = it.next
            }
            node
        }
    }
}

var head: Node? = null

fun getDescription(inputNode: Node?): String =
    when (inputNode) {
        null -> "null"
        else -> "${inputNode.item} " + getDescription(inputNode.next)
    }


fun addToList(inputNode: Node?, stringToAdd: Any): Node? =
    when (inputNode?.item) {
        null -> Node(stringToAdd)
        else -> Node(inputNode.item, addToList(inputNode.next, stringToAdd))
    }

fun addNumbersToLinkedList(inputNode: Node?, count: Int): Node? {
    var outPutNode: Node? = inputNode
    var i = count
    while (i > 0) {
        outPutNode = addToList(outPutNode, i)
        i -= 1
    }
    return outPutNode
}

fun convertLinkedListStringToInt(inputStringNode: Node?): Node? =
    when (inputStringNode?.item) {
        null -> null
        else -> Node(
            convertStringToInt(inputStringNode.item.toString()),
            convertLinkedListStringToInt(inputStringNode.next)
        )
    }


fun convertStringToInt(inputString: String): Int = try {
    inputString.toInt()
} catch (e: NumberFormatException) {
    0
}


fun count(inputNode: Node?): Int {
    var node = inputNode
    return if (node != null) {
        var counter = 1
        while (node?.next != null) {
            node = node.next
            counter += 1
        }
        counter
    } else 0
}

fun reverseLinkedList(inputNode: Node?): Node? {
    var newNode: Node? = head

    for (i in count(inputNode) - 1 downTo 0) {
        newNode = addToList(newNode, nodeAtIndex(inputNode, i)!!.item)
    }
    return newNode
}

fun nodeAtIndex(inputNode: Node?, index: Int): Node? {
    if (index >= 0) {
        var node = inputNode
        var i = index
        while (node != null) {
            if (i == 0) return node
            i -= 1
            node = node.next
        }
    }
    return null
}

fun getDescriptionNonRecursive(inputNode: Node?): String {
    var s = ""
    var node: Node? = inputNode
    while (node != null) {
        s += "${node.item} "
        node = node.next
    }
    s += "null"
    return s
}
