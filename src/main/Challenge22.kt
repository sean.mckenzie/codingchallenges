package main

fun main(args: Array<String>) {
    println("Options Interface:" +
            "\n${BaseILaptop().getDescription()} ${BaseILaptop().getCost()}" +
            "\n${Case(Options()).getDescription()} ${Case(Options()).getCost()}" +
            "\n${Memory(Options()).getDescription()} ${Memory(Options()).getCost()}" +
            "\n${Disk(Options()).getDescription()} ${Disk(Options()).getCost()}" +
            "\n${Processor(Options()).getDescription()} ${Processor(Options()).getCost()}" +
            "\n${Battery(Options()).getDescription()} ${Battery(Options()).getCost()}" +
            "\n${Graphics(Options()).getDescription()} ${Graphics(Options()).getCost()}")

    val ultimateModel: ILaptop = Graphics(Battery(Processor(Disk(Memory(Case(BaseILaptop()))))))

    println("\nLaptop Builder Interface:\n${LaptopBuilder().laptopBuild1.getDescription()} = ${LaptopBuilder().laptopBuild1.getCost()} " +
            "\n${LaptopBuilder().laptopBuild2.getDescription()} = ${LaptopBuilder().laptopBuild2.getCost()}" +
            "\n${LaptopBuilder().laptopBuild3.getDescription()} = ${LaptopBuilder().laptopBuild3.getCost()}" +
            "\n${ultimateModel.getDescription()} = ${ultimateModel.getCost()}")

    otherMain()
}

interface ILaptop {
    fun getDescription(): String
    fun getCost(): Double
}

class LaptopBuilder {
    val laptopBuild1: ILaptop = Case(Memory(BaseILaptop()))
    val laptopBuild2: ILaptop = Disk(Processor(BaseILaptop()))
    val laptopBuild3: ILaptop = Battery(Graphics(BaseILaptop()))
}

class BaseILaptop: ILaptop {
    override fun getDescription(): String {
        return "Base Laptop Model"
    }
    override fun getCost(): Double {
        return 400.00
    }
}

class Options: ILaptop {
    override fun getDescription()= ""
    override fun getCost() = 00.00
}

abstract class LaptopDecorator(private var baseLaptop: ILaptop): ILaptop {
    override fun getDescription() = baseLaptop.getDescription()
    override fun getCost()= baseLaptop.getCost()
}

class Memory(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", 16GB Memory"
    override fun getCost() = super.getCost() + 49.99
}

class Case(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", Shiny Case"
    override fun getCost() = super.getCost() + 10.99
}

class Processor(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", Processor Upgrade"
    override fun getCost() = super.getCost() + 54.99
}

class Disk(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", 1TB Hard Disk"
    override fun getCost() = super.getCost() + 44.99
}

class Graphics(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", Gamer Graphics Card"
    override fun getCost() = super.getCost() + 67.99
}

class Battery(laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + ", Long Life Battery"
    override fun getCost() = super.getCost() + 67.99
}

//Version using functions & data

data class Laptop(val description: String, val cost: Double)

fun memory(laptop: Laptop): Laptop = Laptop(laptop.description + ", 16GB Memory",laptop.cost + 49.99)
fun case(laptop: Laptop): Laptop = Laptop(laptop.description + ", Shiny Case", laptop.cost + 10.99)
fun processor(laptop: Laptop): Laptop = Laptop(laptop.description + ", Processor Upgrade", laptop.cost + 54.99)
fun disk(laptop: Laptop): Laptop = Laptop(laptop.description + ", 1TB Hard Disk", laptop.cost + 44.99)
fun graphics(laptop: Laptop): Laptop = Laptop(laptop.description + ", Gamer Graphics Card", laptop.cost + 67.99)
fun battery(laptop: Laptop): Laptop = Laptop(laptop.description + ", Long Life Battery", cost = laptop.cost + 67.99)

data class AddOn(val description: String, val price: Double)

class AddOnDecorator(val addOn: AddOn, laptop: ILaptop): LaptopDecorator(laptop) {
    override fun getDescription() = super.getDescription() + addOn.description
    override fun getCost() = super.getCost() + addOn.price
}

fun addOn(addOn: AddOn, laptop: Laptop) = Laptop(laptop.description + addOn.description, laptop.cost + addOn.price)

fun otherMain() {
    val baseILaptop = BaseILaptop()
    val baseLaptop = Laptop("Base Laptop Model", 400.00)
    val zeroLaptop = Laptop("", 0.00)
    println("\nFunctions & Data - Options:\n$baseLaptop\n${case(zeroLaptop)}\n${memory(zeroLaptop)}\n${disk(zeroLaptop)}${processor(zeroLaptop)}${battery(zeroLaptop)}\n${graphics(zeroLaptop)}\n")

    val laptopBuild1 = case(memory(baseLaptop))
    val laptopBuild2 = disk(processor(baseLaptop))
    val laptopBuild3 = battery(graphics(baseLaptop))
    val ultimateModel = graphics(battery(processor(disk(memory(case(baseLaptop))))))
    val addOnDecorator = AddOnDecorator(AddOn(", disk drive", 44.99), baseILaptop)

    println("\nFunctions & Data Only - Laptop Builder:\n${laptopBuild1}\n${laptopBuild2}\n${laptopBuild3}\n${ultimateModel}")
    println("\nAddOn Function:\n${addOn(AddOn(", disk drive", 44.99), baseLaptop)}")
    println("\nAddOn Decorator Class\n${addOnDecorator.getDescription() + " @ cost =" + addOnDecorator.getCost()}")

}