package main

//Model
data class Player(
    val name: String,
    val cards: List<String>,
    val bet: Int
)

enum class BestHand(val points: Int) {
    StraightFlush(1),
    Flush(4),
    HighCard(9)
}

typealias Hand = List<String>

//Poker
fun findWinners(players: List<Player>, houseHand: Hand): Player? {

    val playersAndBestHands = mutableListOf<Pair<Player, BestHand>>()

    for (player in players) {
        when {
            isStraightFlush(player.cards + houseHand) -> playersAndBestHands.add(Pair(player, BestHand.StraightFlush))
            isFlush(player.cards + houseHand) -> playersAndBestHands.add(Pair(player, BestHand.Flush))
            else -> playersAndBestHands.add(Pair(player, BestHand.HighCard))
        }
    }

    val sortedPlayers = playersAndBestHands.sortedWith(compareBy { it.second.points })

    return if (sortedPlayers.first().second != sortedPlayers[1].second ) {
        sortedPlayers.first().first
    } else {
        println("at least two players have the same winning hand: ${sortedPlayers.first().second} - more logic required")
        null
    }
}

fun isStraightFlush(cards: Hand): Boolean {
    val flushHand = allCardsInSameSuit(cards)

    if (flushHand.isNotEmpty()) {
        return isConsecutiveNumbers(flushHand).isNotEmpty()
    }
    return false

}

fun isFlush(cards: Hand): Boolean {
    return allCardsInSameSuit(cards).isNotEmpty()
}

fun Hand.isBestFiveCards(): Hand {
    return this.sortedByDescending { card -> valueOfPokerCard(card) }.take(5)
}

fun isHighestFlush(cards: Hand): Hand {
    val flushCards = allCardsInSameSuit(cards)
    return if (isFlush(flushCards)) {
        return flushCards.isBestFiveCards()
    } else {
        emptyList()
    }
    return flushCards.isBestFiveCards()
}

fun allCardsInSameSuit(cards: Hand): Hand {
    val suits = listOf("H", "D", "S", "C")

    val handToReturn = mutableListOf<String>()

    suits.forEach { suit ->
        cards.forEach { card ->
            if (card.contains(suit)) {
                handToReturn.add(card)
            }
        }

        if (handToReturn.size >= 5) {
            return handToReturn
        } else {
            handToReturn.clear()
        }

    }
    return emptyList()
}

fun isConsecutiveNumbers(cards: Hand): Hand {

    val sortedCards = cards.isBestFiveCards()

    val firstCardValue = valueOfPokerCard(sortedCards.first())
    val lastCardValue = valueOfPokerCard(sortedCards.last())

    return if (firstCardValue - lastCardValue == 4) {
        sortedCards
    } else {
        emptyList()
    }
}

fun valueOfPokerCard(card: String): Int {

    return when (val cardValue = card.substring(0, 1)) {
        "T" -> 10
        "J" -> 11
        "Q" -> 12
        "K" -> 13
        "A" -> 14
        else -> cardNumberPokerValue(cardValue)
    }
}

private fun cardNumberPokerValue(cardValue: String): Int {
    return try {
        cardValue.toInt()
    } catch (e: Exception) {
        println("Not valid card number - $e")
        0
    }
}