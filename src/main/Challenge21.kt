package main

fun convertListToSudokuBoard(list: List<Int>) = list.chunked(9)

fun processSudokuBoard(list: List<Int>): List<Int> {
    var inputRows = list.toMutableList()
    var prePermutationAttempt = emptyList<Int>()
    var prePermRowColVals = emptyList<Int>()
    val historyPermutation: MutableList<List<Int>> = emptyList<List<Int>>().toMutableList()
    val historyPermRowColVals: MutableList<List<Int>> = emptyList<List<Int>>().toMutableList()
    var keepGoing = true

    while (keepGoing) {

        val (rowColVals,
            impossibleBoard,
            outputRows) = iterateBoard(inputRows)

        if (inputRows == outputRows) {

            if (!inputRows.contains(0)) {
                println("Complete Successfully and Stop $outputRows ")
                return outputRows.toList()

            } else if (impossibleBoard && prePermutationAttempt.isEmpty()) {
                println("Impossible Board - returning Empty List")
                return emptyList()

            } else if (impossibleBoard) {
                if (prePermRowColVals.count() > 3) {
                    println("This permutation failed, but there is another value to try")
                    val newRowColVals = prePermRowColVals.subList(0, prePermRowColVals.lastIndex)
                    prePermRowColVals = newRowColVals.also(::println)
                    inputRows = attemptPermutation(newRowColVals, prePermutationAttempt.toList()).toMutableList()

                } else if (historyPermRowColVals.count() > 1) {
                    println("Permutation failed completely remove and try another stored option")
                    historyPermutation.removeAt(historyPermutation.lastIndex)
                    historyPermRowColVals.removeAt(historyPermRowColVals.lastIndex)

                    prePermRowColVals = historyPermRowColVals.last().also(::println)
                    prePermutationAttempt = historyPermutation.last().also(::println)

                    val newRowColVals = prePermRowColVals.subList(0, prePermRowColVals.lastIndex)
                    prePermRowColVals = newRowColVals
                    inputRows = attemptPermutation(newRowColVals, prePermutationAttempt.toList()).toMutableList()
                }
            } else {
                if (outputRows.contains(0)) {
                    println("Still have spaces to find - attempt permutation")
                    prePermutationAttempt = outputRows
                    prePermRowColVals = rowColVals.first().also(::println)
                    historyPermutation += prePermutationAttempt
                    historyPermRowColVals += prePermRowColVals
                    inputRows = attemptPermutation(rowColVals.first(), outputRows.toList()).toMutableList()
                    rowColVals.removeAt(0)

                } else {
                    println("STOP as complete")
                    keepGoing = false
                }
            }
        } else inputRows = outputRows

    }
    println("Should not be able to exit without a known output - return empty list")
    return emptyList()
}


private fun iterateBoard(inputRows: MutableList<Int>): Triple<MutableList<List<Int>>, Boolean, MutableList<Int>> {
    val outputRows = emptyList<Int>().toMutableList()
    var impossibleBoard = false
    var fixedOne = false

    val rows = convertListToSudokuBoard(inputRows)
    val permsRowColVals = emptyList<List<Int>>().toMutableList()

    rows.forEach { row ->

        for (column in row.indices) {

            val valuesAvailableToUse = valuesAvailable(valuesAlreadyTaken(inputRows, rows.indexOf(row), column))

            outputRows.add(
                if (fixedOne || row[column] != 0) row[column]
                else if (valuesAvailableToUse.count() == 1) {
                    fixedOne = true
                    valuesAvailableToUse.last()
                } else if (valuesAvailableToUse.count() == 0) {
                    impossibleBoard = true
                    row[column]
                } else {
                    permsRowColVals += logPermutations(rows.indexOf(row), column, valuesAvailableToUse)
                    row[column]
                }
            )
        }
    }
    return Triple(permsRowColVals, impossibleBoard, outputRows)
}


fun attemptPermutation(rowColVals: List<Int>, outputRows: List<Int>): List<Int> {
    val newList = emptyList<Int>().toMutableList()
    var rowIndex = 0

    outputRows.forEach {
        newList += if (rowIndex == (rowColVals[0] * 9) + rowColVals[1]) {
            rowColVals.last()
        } else it
        rowIndex++
    }
return newList
}


fun logPermutations(rowCount: Int, column: Int, doesNotContain: List<Int>): List<Int> {
val newList = emptyList<Int>().toMutableList()
    newList.add(rowCount)
    newList.add(column)
    doesNotContain.forEach { newList.add(it) }

return newList.toList()
}

fun countNumberOfZeroes(list: List<Int>) = list.count { it == 0 }

fun returnIndexOfZero(list: List<Int>, count: Int): Int {
var found = 0
var index = 0
    list.forEach {
        if (it == 0) {
            found++
            if (found == count) return index else index++
        } else index ++
    }
return -1
}

fun returnColumn(list: List<Int>, index: Int): List<Int> {
    val rows = convertListToSudokuBoard(list)
    val cols = emptyList<Int>().toMutableList()

    rows.forEach {row ->
        cols.add(row[index])
    }
    return cols
}

fun gridStartIndex(index: Int): Int = (index/3)*3

fun returnNonet(list: List<Int>, actualRow: Int, actualCol: Int): List<Int> {
    val rows = convertListToSudokuBoard(list)
    val nonet = emptyList<Int>().toMutableList()

    var row = gridStartIndex(actualRow)
    val maxRow = row + 3
    var col = gridStartIndex(actualCol)
    val maxCol = col + 3


    while (row < maxRow) {
            while (col < maxCol) {
                if (maxRow == 4) throw UnsupportedOperationException("help")
                nonet.add(rows[row][col])
                col++
            }
        row++
        col = gridStartIndex(actualCol)
    }
    return nonet
}

fun valuesAvailable(alreadyUsed: List<Int>): List<Int> {
    return listOf(1,2,3,4,5,6,7,8,9).minus(alreadyUsed)
}

fun valuesAlreadyTaken(list: List<Int>, actualRow: Int, actualCol: Int)=
    (convertListToSudokuBoard(list)[actualRow] +
            returnColumn(list, actualCol) +
            returnNonet(list, actualRow, actualCol)).distinct()

