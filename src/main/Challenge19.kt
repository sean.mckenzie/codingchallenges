package main

import kotlin.random.Random

typealias Coords = Pair<Int, Int>
typealias Board = Array<String>

val north = 1
val east = 2
val south = 3
val west = 4

val k = 1000L
val kk = k*k
val kkk = k*k*k
val kkkk = k*k*k*k
val maxk = 10*kkkk-1

val shopLayout = "**************************\n" +
        "*                        *\n" +
        "* ********** *********** *\n" +
        "* ********** *********** *\n" +
        "*                        *\n" +
        "* ********** *********** *\n" +
        "* ********** *********** *\n" +
        "* ********** *********** *\n" +
        "*                        *\n" +
        "**************************\n"

fun shoppingTrolley(input: String = "", referenceID: Long = 0L): Pair<Array<String>, Long> {
    val board = shopStringToArray(shopLayout).also(::println)
    if (!isAStandardBoard(board)) throw UnsupportedOperationException("board is invalid: there are gaps in the edge")

    val (directionFacing: Int, currentCoords: Coords, trolleyId: Int) = decodeReferenceID(referenceID)

    when (input) {
        "" -> {
            return Pair(
                returnView(board, east, findCoordsFirstEmptySpace(shopLayout)),
                encodeReferenceID(east, findCoordsFirstEmptySpace(shopLayout), createTrolleyId())
            )
        }
        "M" -> {
            val (newCoords: Coords, newView: Array<String>) = moveTrolley(board, directionFacing, currentCoords)
            return Pair(
                newView,
                encodeReferenceID(directionFacing, newCoords, trolleyId)
            )
        }
        else -> {
            val turnedToFaceDirection = if (input == "L") turnLeft(directionFacing) else turnRight(directionFacing)
            val newDirectionView = returnView(board, turnedToFaceDirection, currentCoords)
            return Pair(
                newDirectionView,
                encodeReferenceID(turnedToFaceDirection, currentCoords, trolleyId)
            )
        }
    }
}


fun encodeReferenceID(direction: Int, coords: Coords, trolleyId: Int) =
    maxk - (direction.toLong() * kkkk) - (coords.first.toLong() * kkk) - (coords.second.toLong() * kk) - trolleyId.toLong()

fun decodeReferenceID(referenceID: Long): Triple<Int, Coords, Int> {
    val rawNumber = maxk - referenceID
    val direction = (rawNumber / kkkk)

    val xCoords = ((rawNumber - (direction * kkkk)) / kkk)
    val yCoords = ((rawNumber - (direction * kkkk) - (xCoords * kkk)) / 1000000L)
    val trolleyID = ((rawNumber - (direction * kkkk) - (xCoords * kkk) - (yCoords * kk)))

    return Triple(direction.toInt(), Pair(xCoords.toInt(), yCoords.toInt()), trolleyID.toInt())
}


fun Board.rotate90Left(): Board {
    val rowLength = this[0].length
    return (0 until rowLength).map { col ->
        this.map { it[col] }.joinToString(separator = "").reversed()
    }.toTypedArray()
}


fun turnRight(direction: Int): Int = if (direction == 4) 1 else direction + 1

fun turnLeft(direction: Int): Int = if (direction == 1) 4 else direction - 1

fun turnRightBoardRotate(board: Board) = board.rotate90Left().rotate90Left().rotate90Left()

fun turnLeftBoardRotate(board: Board) = board.rotate90Left()

fun returnRelativeCoordsAndBoardBasedOnRotation(board: Board, directionFacing: Int, startPosition: Coords): Pair<Coords, Board> {

    var addXToBoard = arrayOf<String>()
    for (i in 0..board.size - 1) {
        if (i != startPosition.second) {
            addXToBoard += board[i]
        } else {
            val addX = board[i].substring(0, startPosition.first) + "X" + board[i].substring(startPosition.first + 1)
            addXToBoard += addX
        }
    }

    val rotateXBoard = returnBoardRelativeToDirection(addXToBoard, directionFacing)

    var x = 0
    var y = 0
    for (i in rotateXBoard.indices) {
        if (rotateXBoard[i].indexOf("X") > -1) {
            x = rotateXBoard[i].indexOf("X")
            y = i
        }
    }
    return Pair(Pair(x, y), rotateXBoard)
}


fun moveTrolley(board: Board, directionFacing: Int, startPosition: Coords): Pair<Coords, Array<String>> {
    val currentView = returnView(board, directionFacing, startPosition)

    return if (currentView.contentEquals(emptyArray<String>())) {
        Pair(startPosition, currentView)
    } else {
        val newCoords = when (directionFacing) {
            north -> Pair(startPosition.first, startPosition.second - 1)
            east -> Pair(startPosition.first + 1, startPosition.second)
            south -> Pair(startPosition.first, startPosition.second + 1)
            west -> Pair(startPosition.first - 1, startPosition.second)
            else -> startPosition
        }
        val movedView = currentView.toMutableList().drop(1).toTypedArray()

        Pair(newCoords, movedView)
    }
}


fun returnView(board: Board, directionFacing: Int, startPosition: Coords): Array<String> {

    val relativeCoordsAndBoard =
        returnRelativeCoordsAndBoardBasedOnRotation(board, directionFacing, startPosition)
    val relativeCoords = relativeCoordsAndBoard.first
    val relativeBoard = relativeCoordsAndBoard.second
    relativeBoard.forEach { println("rotated $it") }

    val currentColumn = relativeCoords.first
    val currentRow = relativeCoords.second
    val rawRowLeft = relativeBoard[currentRow - 1]
    val rawRow = relativeBoard[currentRow]
    val rawRowRight = relativeBoard[currentRow + 1]

    var output = arrayOf<String>()
    val max = rawRow.toCharArray().size
    var i = currentColumn + 1

    while (i < max) {
        if (rawRow.toCharArray()[i].isWhitespace()) {
            var currentSpace = "O"
            if (rawRowLeft.toCharArray()[i].isWhitespace()) currentSpace += "L"
            if (rawRowRight.toCharArray()[i].isWhitespace()) currentSpace += "R"
            output += currentSpace
        } else {
            i = max
        }
        i += 1
    }
    return output
}


fun returnBoardRelativeToDirection(board: Board, directionFacing: Int) = when (directionFacing) {
    north -> board.rotate90Left()                               //NORTH
    east -> board                                               //EAST
    south -> board.rotate90Left().rotate90Left().rotate90Left() //SOUTH
    west -> board.rotate90Left().rotate90Left()                 //WEST
    else -> board                                           //EAST - return standard board if bad direction provided
}


fun findCoordsFirstEmptySpace(input: String): Coords {
    val shopList = shopStringToArray(input)
    val maxSize = shopSizeMaxIndexCoords(shopList)

    var yCoordinate = 0
    shopList.forEach {
        val xCoordinate = it.indexOf(" ", 0)
        if (xCoordinate != -1) return Pair(xCoordinate, yCoordinate)
        yCoordinate += 1
    }
    return maxSize
}

fun createTrolleyId() = Random.nextInt(100000, 999999)

fun shopStringToArray(input: String) = input.split("\n").filter { it.isNotEmpty() }.toTypedArray()

fun shopSizeMaxIndexCoords(input: Array<String>) = Pair(input[0].length - 1, input.size - 1)

fun isAStandardBoard(input: Array<String>): Boolean {
    var check = true
    if (input.first().contains(" ")) {
        check = false
    } else if (input.last().contains(" ")) {
        check = false
    } else {
        var i = 1
        while (i < input.size) {
            if (input[i].first().toString() != "*") {
                check = false
            } else if (input[i].last().toString() != "*") {
                check = false
            }
            i += 1
        }
    }
    return check
}

