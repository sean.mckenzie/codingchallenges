package main

import main.Sides.*

typealias Cube = Array<String>
typealias Rows = Array<String>
typealias Squares = List<String>
typealias Face = String

enum class Sides(val side: Int) {  
    Front(0),
    Back(1),
    Left(2),
    Right(3),
    Top(4),
    Bottom(5)
}

fun rotateCube(cube: Cube, side: Sides, rotation: String): Cube {
        return when (side) {
            Top -> cube.rotateTop(rotation)
            Bottom -> cube.rollForward().rollForward().rotateTop(rotation).rollForward().rollForward()
            Left -> cube.turnCube().rollForward().rotateTop(rotation).rollBackward().turnCubeCCW()
            Right -> cube.turnCube().rollBackward().rotateTop(rotation).rollForward().turnCubeCCW()
            Front -> cube.rollBackward().rotateTop(rotation).rollForward()
            Back -> cube.rollForward().rotateTop(rotation).rollBackward()
        }
}

fun Face.faceToSquares(): Squares = this.chunked(this.length / 9)

fun Face.faceToRows(): Rows = this.chunked(this.length / 3).toTypedArray()

fun Squares.squaresToFace(): Face = this.joinToString("")

fun Rows.rowsToFace(): Face = this.joinToString("")

fun Cube.getFace(direction: Sides): Face = this[direction.side]

fun Rows.rotateCCW() = this.rotate().rotate().rotate()
    
fun Rows.rotate(): Rows {
    val rowLength = this[0].length
    val squareLength = this[0].length / 3

        return (0 until rowLength step squareLength).map { col ->
            this.map { it.substring(col, col + squareLength)}.reversed().joinToString(separator = "")
        }.toTypedArray()
    }

fun Cube.rollBackward() = this.rollForward().rollForward().rollForward()

fun Cube.rollForward(): Cube {
    val front = this.getFace(Top)
    val back = this.getFace(Bottom)
    val left = this.getFace(Left).faceToRows().rotate().rowsToFace()
    val right = this.getFace(Right).faceToRows().rotateCCW().rowsToFace()
    val top = this.getFace(Back).faceToSquares().reversed().squaresToFace()
    val bottom = this.getFace(Front).faceToSquares().reversed().squaresToFace()

    return arrayOf(front, back, left, right, top, bottom)
}

fun Cube.turnCubeCCW() = this.turnCube().turnCube().turnCube()
 
fun Cube.turnCube(): Cube {
    val front = this.getFace(Right)
    val back = this.getFace(Left)
    val left = this.getFace(Front)
    val right = this.getFace(Back)
    val top = this.getFace(Top).faceToRows().rotate().rowsToFace()
    val bottom = this.getFace(Bottom).faceToRows().rotateCCW().rowsToFace()

    return arrayOf(front, back, left, right, top, bottom)
}

fun Cube.rotateTop(direction: String): Cube {
    return if (direction == "CW") this.rotateTopRow() else this.rotateTopRowCCW()
}

fun Cube.rotateTopRowCCW(): Cube= this.rotateTopRow().rotateTopRow().rotateTopRow()

fun Cube.rotateTopRow(): Cube {
    val front = rotatedSideFace(this, Right, Front)
    val back = rotatedSideFace(this, Left, Back)
    val left = rotatedSideFace(this, Front, Left)
    val right = rotatedSideFace(this, Back, Right)
    val top = this.getFace(Top).faceToRows().rotate().rowsToFace()
    val bottom = this.getFace(Bottom)
    
    return arrayOf(front, back, left, right, top, bottom)
}

private fun rotatedSideFace(cube: Cube, side1: Sides, side2: Sides): Face = 
    cube.getFace(side1).faceToRows()[0] +
            cube.getFace(side2).faceToRows()[1] + 
            cube.getFace(side2).faceToRows()[2] 


