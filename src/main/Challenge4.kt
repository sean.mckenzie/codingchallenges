package main

class Challenge4 {

    fun sortVouchers(input: String): String {

        var outputStringNew = ""
        val unsortedNewVouchers: List<String> =
            input.split(',').filter { it.contains("Available").or(it.contains("Activated")) }.sorted()

        val sortedNewVouchersTriple: List<Triple<String, String, String>> =
            input.split(',').filter { it.contains("Available").or(it.contains("Activated")) }
                .map {
                    Triple(
                        it.substringBefore(':'),
                        it.substringAfter(':').substringBeforeLast(':'),
                        it.substringAfterLast(':')
                    )
                }
                .sortedBy { it.first + it.second + it.third }

        sortedNewVouchersTriple.forEach { outputStringNew += "${it.first}:${it.second}:${it.third}," }

        fun finalStringNew(): String? {
            if (unsortedNewVouchers.isEmpty()) return ""
            else return unsortedNewVouchers.toString()
                .replace(" ", "")
                .replace("[", "")
                .replace("]", "") + ","
        }


        var outputStringOld = ""
        val sortedOldVouchersTriple: List<Triple<String, String, String>> =
            with(input) {
                split(',').filter { it.contains("Redeemed").or(it.contains("Expired")) }
                    .map {
                        Triple(
                            it.substringBefore(':'),
                            it.substringAfter(':').substringBeforeLast(':'),
                            it.substringAfterLast(':')
                        )
                    }
                    .sortedWith(compareByDescending<Triple<String, String, String>> { it.first + it.second })
            }


        sortedOldVouchersTriple.forEach { outputStringOld += "${it.first}:${it.second}:${it.third}," }

        fun finalStringOld(): String? {
            if (outputStringOld == null) return null
            else return outputStringOld.trim().substringBeforeLast(",")
        }



        return if (finalStringOld().isNullOrBlank())
            finalStringNew()!!.substringBeforeLast(",")
        else
            finalStringNew() + finalStringOld()

    }
}

