package main.AdventOfCode

fun processDigits(number: Int): String {
    val num6 = (number / 1) % 10
    val num5 = (number / 10) % 10
    val num4 = (number / 100) % 10
    val num3 = (number / 1000) % 10
    val num2 = (number / 10000) % 10
    val num1 = (number / 100000) % 10

    return ("$num1-$num2-$num3-$num4-$num5-$num6")
}


fun processDigitsSameOrBigger(number: Int): Boolean {
    var output = true
    var count = 0
    var divisor = 100000
    var numberA = 0
    while (count < 6) {
        val numberB = getDigit(number, divisor)
        if (numberB < numberA) {
            output = false
            count = 6
        } else {
            numberA = numberB
            divisor = divisor / 10
            count += 1
        }
         }
    return output
}


fun processDigitsSameOrBiggerX(number: Int): String {
    var output = "bigger"
    var count = 0
    var divisor = 100000
    var numberA = 0
    while (count < 6) {
        val numberB = getDigit(number, divisor)
        if (numberB < numberA) {
            output = "false"
            count = 6
        } else if (numberB == numberA) {
            numberA = numberB
            output = "same"
            divisor = divisor / 10
            count += 1
        } else {
            numberA = numberB
            divisor = divisor / 10
            count += 1
        }
         }
    return output
}

fun processDigitsSameOrBiggerExcludeTrebles(number: Int): String {
    //114444 so get a "same" - then a treble  1-bigger-1-same(doublecount=1)-4-(else...dc=0)-4(same&"same"&cd=0...leave&dc+1)
    // -4(same&dc>0)
    //122234 - should fail 1-big-2big-2same dc=1-2
    var output = "bigger"
    var count = 0
    var doubleCount = 0
    var divisor = 100000
    var numberA = 0
    while (count < 6) {
        val numberB = getDigit(number, divisor)
        if (numberB < numberA) {
            output = "false"
            count = 6
            println("if: count = $count, number = $numberB, output = $output, double-count = $doubleCount")
        } else if (numberB == numberA && output == "same" && doubleCount == 0) {
            numberA = numberB
//            output = "bigger" // here?   same???   first double
            divisor = divisor / 10
            count += 1
//            doubleCount += 1
            println("$number elseif1: count = $count, number = $numberB, output = $output, double-count = $doubleCount")
        } else if (numberB == numberA && doubleCount > 0) {
            numberA = numberB
            output = "bigger" // here?   same???   first double
            divisor = divisor / 10
            count += 1
//            doubleCount += 1
            println("$number elseif2: count = $count, number = $numberB, output = $output, double-count = $doubleCount")
        } else if (numberB == numberA) {
            numberA = numberB
            output = "same"  // actual same????
            divisor = divisor / 10
            count += 1
            doubleCount += 1
            println("$number elseif3: count = $count, number = $numberB, output = $output, double-count = $doubleCount")
        } else {
            numberA = numberB
            divisor = divisor / 10
            count += 1
            doubleCount = 0
            println("$number else: count = $count, number = $numberB, output = $output, double-count = $doubleCount")
        }
         }
    return output
}

private fun getDigit(number: Int, divisor: Int) = (number / divisor) % 10

fun createRange(start: Int, end: Int): List<Int> {
    var output: List<Int> = emptyList()

    var newNumber = start

    while (newNumber <= end) {
        if (processDigitsSameOrBigger(newNumber)) {
            output += newNumber
        }
        newNumber += 1
    }

    println(output)
    return output
}

fun createRangeMustIncludeDouble(start: Int, end: Int): List<Int> {
    var output: List<Int> = emptyList()
    var newNumber = start

    while (newNumber <= end) {
        if (processDigitsSameOrBiggerX(newNumber) == "same") {
            output += newNumber
        }
        newNumber += 1
    }

    println(output)
    return output
}


fun createRangeMustIncludeDoubleNotInLargerGroup(start: Int, end: Int): List<Int> {
    var output: List<Int> = emptyList()

    var newNumber = start

    while (newNumber <= end) {
        val check = processDigitsSameOrBiggerExcludeTrebles(newNumber)
        if (check == "same") {
            output += newNumber
        }
        newNumber += 1
    }

    println(output)
    return output
}

fun runMyList(input: List<Int>): String {
    var output = ""
    input.forEach { number ->
        output += processDigits(number)
    }
    return output
}


/*
--- Day 4: Secure Container ---
You arrive at the Venus fuel depot only to discover it's protected by a password.
The Elves had written the password on a sticky note, but someone threw it out.

However, they do remember a few key facts about the password:

It is a six-digit number.
The value is within the range given in your puzzle input.
Two adjacent digits are the same (like 22 in 122345).
Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
Other than the range rule, the following are true:

111111 meets these criteria (double 11, never decreases).
223450 does not meet these criteria (decreasing pair of digits 50).
123789 does not meet these criteria (no double).
How many different passwords within the range given in your puzzle input meet these criteria?

Your puzzle input is 108457-562041.

Answer: 2779  *********

###############

Your puzzle answer was 2779.

The first half of this puzzle is complete! It provides one gold star: *

--- Part Two ---
An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of
matching digits.

Given this additional criterion, but still ignoring the range rule, the following are now true:

112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
How many different passwords within the range given in your puzzle input meet all of the criteria?

Your puzzle input is still 108457-562041.

Answer:


You can also [Share] this puzzle.


*/