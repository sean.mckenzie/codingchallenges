package main.AdventOfCode

fun createLayers(image: String, width: Int, height: Int): List<List<String>> {

    val imageSplit = image.chunked(width)

    var output = emptyList<List<String>>()
    var list = emptyList<String>()
    var count = 1

    imageSplit.forEach {
        list += it
        if (count == height) {
            output += listOf(list)
            list = emptyList()
            count = 0
        }
        count += 1
    }
    return output
}

fun countString(list: List<String>, string1: String, string2: String, string3: String): Triple<Int,Int,Int> {
    var countString1 = 0
    var countString2 = 0
    var countString3 = 0
    list.forEach {
        countString1 += it.count{ string1.contains(it)}
        countString2 += it.count{ string2.contains(it)}
        countString3 += it.count{ string3.contains(it)}
    }
    return Triple(countString1, countString2, countString3)
}

fun countStringListValidation(listOfLists: List<List<String>>, string1: String, string2: String, string3: String): Int {

    var (one, two, three) = countString(listOfLists[0],string1,string2,string3)

    listOfLists.forEach {
        val (a,b,c) = countString(it, string1, string2, string3)
        if (a < one) {
            one = a
            two = b
            three = c
        }
    }

return two * three
}


fun decodeImage(imageList: List<List<String>>, width: Int, transparent:String): List<String> {
    var string = ""
    imageList[0].forEach { string += it}
//    println(string)
    var flatten = string.chunked(1).toMutableList()
    val flattenCount = string.length - 1
//    println("flattenCount = $flattenCount")

        while (flatten.contains(transparent)) {
            imageList.forEach { row ->
                var rowString = ""
                row.forEach { rowString += it}
//                println(rowString)
                var rowFlatten = rowString.chunked(1).toMutableList()

                for (i in 0..flattenCount) {
                    if (flatten[i] == transparent) flatten[i] = rowFlatten[i]
                }
            }
        }
    var answer = ""
    flatten.forEach { answer += it }
    val finalAnswer = answer.chunked(width)//.also(::println)

//    finalAnswer.forEach { println(it) }
//    var finalString = ""
//    finalAnswer.forEach {
//        finalString += it
//    }

//    println (finalString)

    return finalAnswer
}


/*
*/