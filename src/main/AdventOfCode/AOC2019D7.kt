package main.AdventOfCode

fun main(args: Array<String>) {

    println("Day07().partOne()")
    println(Day07().partOne())

//    println("Day07().partTwo()")
//    println(Day07().partTwo())

}

class Day07() {
    fun partTwo() {

//        Here are some example programs:
//        Max thruster signal 139629729 (from phase setting sequence 9,8,7,6,5):
        val input98765 = "98765"
        val inputListA = arrayListOf(3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5)

//        Max thruster signal 18216 (from phase setting sequence 9,7,8,5,6):
        val input97856 = "97856"
        val inputListB = arrayListOf(3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4, 53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10)

//        val inputString = input98765
        val inputString = input97856
//        val inputArrayList = inputListA
        val inputArrayList = inputListB

        var outputTest: MutableList<Int> = emptyList<Int>().toMutableList()

        val inputA = inputString.substring(0,1).toInt().also(::println)
        val inputB = inputString.substring(1,2).toInt().also(::println)
        val inputC = inputString.substring(2,3).toInt().also(::println)
        val inputD = inputString.substring(3,4).toInt().also(::println)
        val inputE = inputString.substring(4,5).toInt().also(::println)

        var outputA = emptyList<Int>().toMutableList()
        var outputB = emptyList<Int>().toMutableList()
        var outputC = emptyList<Int>().toMutableList()
        var outputD = emptyList<Int>().toMutableList()
        var outputE = listOf(0).toMutableList()

        var inputArrayA = inputArrayList
        var inputArrayB = inputArrayList
        var inputArrayC = inputArrayList
        var inputArrayD = inputArrayList
        var inputArrayE = inputArrayList


        var i = 0
//        run the whole set of instructions to see how may outputs / iterations there are
        var maxIndex = (intCodeUpgradePart2(inputArrayList, inputA, listOf(0)).second.third.size ).also(::println) //max = size
//        var maxIndex = (intCodeUpgradePart2(fullInput, inputA, listOf(0)).second.third.size - 1).also(::println)

        var outputEPrep = listOf(0).toMutableList()
        var x = 0
        while (x < maxIndex) {
            outputEPrep.add(0)
            x += 1
        }
        println("outputEPrep = $outputEPrep")
        println("input list = $inputArrayA")

        while (i < maxIndex  ){ // max - calulated above EXTRA ADDED HELP XXXXXXXXXXX
            println("before if max = $maxIndex   i = $i")
            if (i > 0) outputEPrep = outputE  //outputE != emptyList<Int>()
            println("max = $maxIndex   i = $i")

            val addA = intCodeUpgradePart2(inputArrayList, inputA, outputEPrep, i)
//            val addA = intCodeUpgradePart2(inputArrayA, inputA, outputEPrep, i)
            outputA = addA.second.third.toMutableList()
            inputArrayA = addA.first
         println("output A = input B = i=$i " + outputA + " " + inputArrayA ) //Q - updated array list?????   to feed into next amp???/

            val addB = intCodeUpgradePart2(inputArrayList, inputB, outputA, i)
//            val addB = intCodeUpgradePart2(inputArrayB, inputB, outputA, i)
            outputB = addB.second.third.toMutableList()
            inputArrayB = addB.first
            println("output B = input C = i=$i " + outputB + " " + inputArrayB)

            val addC = intCodeUpgradePart2(inputArrayList, inputC, outputB, i)
//            val addC = intCodeUpgradePart2(inputArrayC, inputC, outputB, i)
            outputC = addC.second.third.toMutableList()
            inputArrayC = addC.first
            println("output C = input D = i=$i " + outputC + " " + inputArrayC)

            val addD = intCodeUpgradePart2(inputArrayList, inputD, outputC, i)
//            val addD = intCodeUpgradePart2(inputArrayD, inputD, outputC, i)
            outputD = addD.second.third.toMutableList()
            inputArrayD = addD.first
         println("output D = input E = i=$i " + outputD + " " + inputArrayD)

            val addE = intCodeUpgradePart2(inputArrayList, inputE, outputD, i)
//            val addE = intCodeUpgradePart2(inputArrayE, inputE, outputD, i)
            outputE = addE.second.third.toMutableList()//+1 due to 0 always at start
            inputArrayE = addE.first
         println("output E = input A = i=$i " + outputE + " " + inputArrayE)
            i += 1
        }
        outputTest.add(outputE.last()) //assume last is the one you want???????

        val largest = outputTest.max()
        println("outputTest = largest: $largest of $outputTest")

////#######FULL TEST //HIDE UNTIL READY
//        val fullInput = arrayListOf(3,8,1001,8,10,8,105,1,0,0,21,30,47,64,81,98,179,260,341,422,99999,3,9,1001,9,5,9,4,9,99,3,9,1002,9,5,9,101,4,9,9,102,2,9,9,4,9,99,3,9,102,3,9,9,101,2,9,9,1002,9,3,9,4,9,99,3,9,1001,9,5,9,1002,9,3,9,1001,9,3,9,4,9,99,3,9,1002,9,3,9,101,2,9,9,102,5,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99)
//        val inputChars = "56789"
//        val inputList = listOf("5","6","7","8","9")
//
//        val getUniqueStrings =  get120UniqueStrings(inputList,inputChars).also(::println)
//        println (getUniqueStrings.count())
//
//        var output: MutableList<Int> = mutableListOf(0)
//
//        getUniqueStrings.forEach {
//
//            val inputA = it.substring(0,1).toInt().also(::println)
//            val inputB = it.substring(1,2).toInt().also(::println)
//            val inputC = it.substring(2,3).toInt().also(::println)
//            val inputD = it.substring(3,4).toInt().also(::println)
//            val inputE = it.substring(4,5).toInt().also(::println)
//
//
//            var outputA = emptyList<Int>().toMutableList()
//            var outputB = emptyList<Int>().toMutableList()
//            var outputC = emptyList<Int>().toMutableList()
//            var outputD = emptyList<Int>().toMutableList()
//            var outputE = listOf(0).toMutableList()
//
//            var i = 0
//            var maxIndex = (intCodeUpgradePart2(fullInput, inputA, listOf(0)).second.third.size ).also(::println)
//
//            var outputEPrep = listOf(0).toMutableList()
//            var x = 0
//            while (x < maxIndex) {
//                outputEPrep.add(0)
//                x += 1
//            }
//            println("outputEPrep = $outputEPrep")
//
//            while (i < maxIndex ){ // max - calulated above
//                println("before if max = $maxIndex   i = $i")
//                if (i > 0) outputEPrep = outputE  //outputE != emptyList<Int>()
//                println("max = $maxIndex   i = $i")
//
//                val addA = intCodeUpgradePart2(fullInput, inputA, outputEPrep, 0)
//                outputA = addA.second.third.toMutableList()
//                println("output A = input B = i=$i " + addA.second.third) //Q - updated array list?????   to feed into next amp???/
//
//                val addB = intCodeUpgradePart2(fullInput, inputB, outputA, 0)
//                outputB = addB.second.third.toMutableList()
//                println("output B = input C = i=$i " + addB.second.third)
//
//                val addC = intCodeUpgradePart2(fullInput, inputC, outputB, 0)
//                outputC = addC.second.third.toMutableList()
//                println("output C = input D = i=$i " + addC.second.third)
//
//                val addD = intCodeUpgradePart2(fullInput, inputD, outputC, 0)
//                outputD = addD.second.third.toMutableList()
//                println("output D = input E = i=$i " + addD.second.third)
//
//                val addE = intCodeUpgradePart2(fullInput, inputE, outputD, 0)
//                outputE = addE.second.third.toMutableList()//+1 due to 0 always at start
//                println("output E = input A = i=$i " + addE.second.third)
//                i += 1
//            }
//
//            output.add(outputE.last()) //assume last is the one you want???????
//
//            val largest = output.max()
//            println("outputTest = largest: $largest of $output")
//
//            if (getUniqueStrings.count() != 120) println("FAILED DO NOT USE")
//
//        }
////#######FULL TEST //HIDE UNTIL READY
    }

    fun partOne() {
        val fullInput = arrayListOf(3,8,1001,8,10,8,105,1,0,0,21,30,47,64,81,98,179,260,341,422,99999,3,9,1001,9,5,9,4,9,99,3,9,1002,9,5,9,101,4,9,9,102,2,9,9,4,9,99,3,9,102,3,9,9,101,2,9,9,1002,9,3,9,4,9,99,3,9,1001,9,5,9,1002,9,3,9,1001,9,3,9,4,9,99,3,9,1002,9,3,9,101,2,9,9,102,5,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99)

        val getUnique43210Strings =  get120UniqueStrings(listOf("4", "3", "2", "1", "0"),"43210").also(::println)
        println (getUnique43210Strings.count())

        var output: MutableList<Int> = mutableListOf(0)

        getUnique43210Strings.forEach {

            val inputA = it.substring(0,1).toInt().also(::println)
            val inputB = it.substring(1,2).toInt().also(::println)
            val inputC = it.substring(2,3).toInt().also(::println)
            val inputD = it.substring(3,4).toInt().also(::println)
            val inputE = it.substring(4,5).toInt().also(::println)

            val inputStart = listOf(0)

            val outputA = intCodeUpgradePart2(fullInput, inputA, inputStart).second.third.also(::println)
            val outputB = intCodeUpgradePart2(fullInput, inputB, outputA).second.third.also(::println)
            val outputC = intCodeUpgradePart2(fullInput, inputC, outputB).second.third.also(::println)
            val outputD = intCodeUpgradePart2(fullInput, inputD, outputC).second.third.also(::println)
            val outputE = intCodeUpgradePart2(fullInput, inputE, outputD).second.third.also(::println)

            output.add(outputE.last())
        }

        val inputListC = arrayListOf(3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0)
        val inputListB = arrayListOf(3,23,3,24,1002,24,10,24,1002,23,-1,23,
            101,5,23,23,1,24,23,23,4,23,99,0,0)
        val inputListA = arrayListOf(3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
            1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0)
        val inputStringC = "43210"
        val inputStringB = "01234"
        val inputStringA = "10432"

        val inputArrayList = inputListA
        val input = inputStringA

        val phaseSettingA = input.substring(0,1).toInt().also(::println)
        val phaseSettingB = input.substring(1,2).toInt().also(::println)
        val phaseSettingC = input.substring(2,3).toInt().also(::println)
        val phaseSettingD = input.substring(3,4).toInt().also(::println)
        val phaseSettingE = input.substring(4,5).toInt().also(::println)

        val inputStart = listOf(0)
        var outputE = inputStart

        val outputA = intCodeUpgradePart2(inputArrayList, phaseSettingA, outputE).second.third.also(::println)
        val outputB = intCodeUpgradePart2(inputArrayList, phaseSettingB, outputA).second.third.also(::println)
        val outputC = intCodeUpgradePart2(inputArrayList, phaseSettingC, outputB).second.third.also(::println)
        val outputD = intCodeUpgradePart2(inputArrayList, phaseSettingD, outputC).second.third.also(::println)
            outputE = intCodeUpgradePart2(inputArrayList, phaseSettingE, outputD).second.third.also(::println)

        println("output = $outputE")

        val largest = output.max()
        println("Final output = largest $largest of all runs = $output")

        if (getUnique43210Strings.count() != 120) println("FAILED DO NOT USE")
    }


    private fun get120UniqueStrings(input: List<String>, charsInput: String): List<String> {
        var chars = charsInput
        val charsList = chars.toMutableList()
        var list = input.toMutableList()
        var count = 0
        while (count < 1000) {
            charsList.shuffle()
            var chars = ""
            charsList.forEach { chars += it }
            list.add(chars)
            count += 1
        }
        return list.distinct().toList().filter { it.length == 5 }
    }

    fun intCodeUpgradePart2(inputList: ArrayList<Int>, phaseSetting: Int, getInputFrom: List<Int>, countIteration: Int = 0): Pair<ArrayList<Int>, Triple<Int?,String,List<Int>>> {
//count = i when calling function - iteration while (i < max)
        //countIteration - is passed as i iteration (when 0 - use phase input - else don't!!!)

        println("New call to intCodeUpgradePart2 with list: [x,x,x] phase: $phaseSetting getinput: $getInputFrom  countIteration: $countIteration arraylist = $inputList")
        var count = countIteration
        //iteration = say 9, then need to count up from 0-9 from getInput
        //AND feed input <= count as the first 'count' outputs - i.e repeat what done before
        var index = 0
        val maxIndex = inputList.count()

        var input = phaseSetting  //fisrt time in only
//        if (count != 0) input = getInputFrom.last() //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        var output = 0
        var finalOutput = 0
        var listOfOutputs = emptyList<Int>().toMutableList()
        var outputMessage = ""
        var workingList = inputList
        var inputIndex = 0
        while (index < maxIndex && workingList[index] != 99) {
            val result = parameterModeResult(workingList, index, input)
            output = result.second.first
            outputMessage = result.second.second
            workingList = result.first
        println("$index $result") //careful printing this - 1000s of lines XXXXXXXX //
            index = when (result.second.first) {
                null -> maxIndex
                else -> if (result.second.second == "stop" ) {
//           println("STOP!!!")//getInputFrom $getInputFrom  count = $count  inputIndex = $inputIndex") //HERE????????
                    maxIndex
                } else if (result.second.second == "jump-if-true") {
                    result.second.first //move to pointer else nothing
                } else if (result.second.second == "not-jump-if-true") {
                    index + 3
                } else if (result.second.second == "jump-if-false") {
                    result.second.first //move to pointer else nothing
                } else if (result.second.second == "not-jump-if-false") {
                    index + 3
                } else if (result.second.second == "input" ) {
//          println("getInputFrom $getInputFrom  count = $count  inputIndex = $inputIndex") //HERE????????
//                    if (inputIndex <= count) { // take prev numbers... then calculate remainders

                    input = getInputFrom.last()//[inputIndex]
                    finalOutput = result.second.third
//                    inputIndex += 1 //0 first time through (0,1,2 etc up to max)
                    index + 2
                } else if (result.second.second == "output") {
                    listOfOutputs.add(result.second.third)
                    finalOutput = result.second.third
                    index + 2  //try max index i.e. stop for this run  HERE!!!!!!! XXXX
                } else {
                    index + 4
                }
            }
        }
        return Pair(workingList,Triple(output,outputMessage,listOfOutputs))
    }


    fun processInputParameters(input: Int) = input.toString().padStart(5, '0').subSequence(0, 3).toString()

    fun processInputOpCode(input: Int): String {
        val instruction = input.toString().padStart(5, '0')
        val action = instruction.subSequence(3, 5).toString()
        return when (action) {
            "01" -> "add"
            "02" -> "multiply"
            "03" -> "input"
            "04" -> "output"
            "05" -> "jump-if-true"
            "06" -> "jump-if-false"
            "07" -> "less-than"
            "08" -> "equals"
            "99" -> "stop"
            else -> "unknown"
        }
    }




    fun parameterModeResult(inputList: ArrayList<Int>, index: Int, input: Int): Pair<ArrayList<Int>, Triple<Int,String,Int>> {
        val opcode = processInputOpCode(inputList[index])
        var finalOutput = 0
        when (opcode) {
            "add" -> return Pair(additionOpcode1(inputList, index, processInputParameters(inputList[index])),Triple(input,"add",finalOutput))
            "multiply" -> return Pair(multiplyOpcode2(inputList, index, processInputParameters(inputList[index])),Triple(input,"multiply",finalOutput))
            "equals" -> return Pair(equalsOpcode8(inputList, index, processInputParameters(inputList[index])),Triple(input,"equals",finalOutput))
            "less-than" -> return Pair(lessThanOpcode7(inputList, index, processInputParameters(inputList[index])),Triple(input,"less-than",finalOutput))
            "jump-if-false" -> {
                val jumpIfFalseOutput = jumpIfFalseOpcode6(inputList, index, processInputParameters(inputList[index]))
                if (jumpIfFalseOutput.first == null) {
                    return Pair(inputList, Triple(input, "not-jump-if-false",finalOutput))
                } else {
                    return Pair(inputList, Triple(jumpIfFalseOutput.second!!, "jump-if-false",finalOutput))
                }
            }
            "jump-if-true" -> {
                val jumpIfTrueOutput = jumpIfTrueOpcode5(inputList, index, processInputParameters(inputList[index]))
                if (jumpIfTrueOutput.first == null) {
//                    println("not-jump-if-true A")
                    return Pair(inputList, Triple(input, "not-jump-if-true",finalOutput))
                } else {
//                    println("jump-if-true B  - Pair(inputList, Pair(${jumpIfTrueOutput.second}, jump-if-true")
                    return Pair(inputList, Triple(jumpIfTrueOutput.second!!, "jump-if-true",finalOutput))
                }
            }
            "input" -> return Pair(inputOpcode3(inputList, index, input),Triple(input,"input",finalOutput))
            "output" -> {
                finalOutput = outputOpcode4(inputList, index, processInputParameters(inputList[index]))
                return Pair(inputList, Triple(finalOutput, "output",finalOutput))//outputOpcode4(inputList, index, processInputParameters(inputList[index])),"output"))
            }
            "stop" -> return Pair(inputList,Triple(finalOutput,"stop",finalOutput))
            else -> return Pair(inputList,Triple(finalOutput,"unknown",finalOutput))
        }
    }

    fun additionOpcode1(inputList: ArrayList<Int>, index: Int, params: String): ArrayList<Int> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()

        var total = 0
        val dest = inputList[index + 3]


        if (param1 == 0) {
            total += inputList[inputList[index + 1]]
        } else {
            total += inputList[index + 1]
        }

        if (param2 == 0) {
            total += inputList[inputList[index + 2]]
        } else {
            total += inputList[index + 2]
        }

        var count = 0
        val outputList = arrayListOf<Int>()
        while (count < inputList.count()) {
            if (count == dest) {
                outputList.add(total)
            } else {
                outputList.add(inputList[count])
            }
            count += 1
        }
        return outputList
    }

    fun multiplyOpcode2(inputList: ArrayList<Int>, index: Int, params: String): ArrayList<Int> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()

        var total1 = 0
        var total2 = 0
        val dest = inputList[index + 3]

        if (param1 == 0) {
            total1 = inputList[inputList[index + 1]]
        } else {
            total1 = inputList[index + 1]
        }

        if (param2 == 0) {
            total2 += inputList[inputList[index + 2]]
        } else {
            total2 += inputList[index + 2]
        }

        var count = 0
        val outputList = arrayListOf<Int>()
        while (count < inputList.count()) {
            if (count == dest) {
                outputList.add(total1*total2)
            } else {
                outputList.add(inputList[count])
            }
            count += 1
        }
        return outputList
    }

    fun equalsOpcode8(inputList: ArrayList<Int>, index: Int, params: String): ArrayList<Int> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()

        var total1 = 0
        var total2 = 0

        if (param1 == 0) {
            total1 = inputList[inputList[index + 1]]
        } else {
            total1 = inputList[index + 1]
        }

        if (param2 == 0) {
            total2 = inputList[inputList[index + 2]]
        } else {
            total2 = inputList[index + 2]
        }

        val outputNumber = if (total1 == total2) 1 else 0
        var count = 0
        val outputList = arrayListOf<Int>()
        while (count < inputList.count()) {
            if (count == inputList[index + 3]) {
//                println("equals than add $outputNumber to position $count")
                outputList.add(outputNumber)
            } else {
                outputList.add(inputList[count])
            }
            count += 1
        }
        return outputList
    }

    fun lessThanOpcode7(inputList: ArrayList<Int>, index: Int, params: String): ArrayList<Int> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()

        var total1 = 0
        var total2 = 0

        if (param1 == 0) {
            total1 = inputList[inputList[index + 1]]
        } else {
            total1 = inputList[index + 1]
        }

        if (param2 == 0) {
            total2 = inputList[inputList[index + 2]]
        } else {
            total2 = inputList[index + 2]
        }

        val outputNumber = if (total1 < total2) 1 else 0
        var count = 0
        val outputList = arrayListOf<Int>()
        while (count < inputList.count()) {
            if (count == inputList[index + 3]) {
//                println("less than add $outputNumber to position $count")
                outputList.add(outputNumber)
            } else {
                outputList.add(inputList[count])
            }
            count += 1
        }
        return outputList
    }

//Parameters that an instruction writes to will NEVER be in immediate mode.

    fun jumpIfTrueOpcode5(inputList: ArrayList<Int>, index: Int, params: String): Pair<Int?, Int?> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()
        if (param1 != 0) { //2205 or 0105, 5, 10
//            println("got to 1 ${inputList[index]}")
//            println("got to 1a Pair(0, ${inputList[index + 2]}) for index $index")
            if (inputList[index + 1] != 0) { //0105, 5, 10  {
                if (param2 != 0) {
                    return Pair(0, inputList[index + 2])
                } else return Pair(0, inputList[inputList[index + 2]])
            } else return Pair(null, null)
        } else if (inputList[inputList[index + 1]] != 0) {  //1005, 5..position is 0, 10 {
//            println("got to 2 ${inputList[index]} return Pair(0, ${inputList[inputList[index + 2]]})")
            if (param2 != 0) {
                return Pair(0, inputList[index + 2])
            } else return Pair(0, inputList[inputList[index + 2]])
        } else return Pair(null, null)
    }

    fun jumpIfFalseOpcode6(inputList: ArrayList<Int>, index: Int, params: String): Pair<Int?, Int?> {
        val param1 = params.substring(2, 3).toInt()
        val param2 = params.substring(1, 2).toInt()
        if (param1 != 0) { //2205 or 0105, 5, 10
//            println("got to 1 ${inputList[index]}")
//            println("got to 1a Pair(0, ${inputList[index + 2]}) for index $index")
            if (inputList[index + 1] == 0) { //0105, 5, 10  {
                if (param2 != 0) {
                    return Pair(0, inputList[index + 2])
                } else return Pair(0, inputList[inputList[index + 2]])
            } else return Pair(null, null)
        } else if (inputList[inputList[index + 1]] == 0) {  //1005, 5..position is 0, 10 {
//            println("got to 2 ${inputList[index]} return Pair(0, ${inputList[inputList[index + 2]]})")
            if (param2 != 0) {
                return Pair(0, inputList[index + 2])
            } else return Pair(0, inputList[inputList[index + 2]])
        } else return Pair(null, null)
    }

    fun inputOpcode3(inputList: ArrayList<Int>, index: Int, input: Int): ArrayList<Int> {
        val address = inputList[index + 1]

        var count = 0
        val outputList = arrayListOf<Int>()
        while (count < inputList.count()) {
            if (count == address) {
                outputList.add(input)
            } else {
                outputList.add(inputList[count])
            }
            count += 1
        }
        return outputList
    }

    fun outputOpcode4(inputList: ArrayList<Int>, index: Int, params: String): Int {
        val param1 = params.substring(2, 3).toInt()

        if (param1 == 0) {
            return inputList[inputList[index + 1]]
        } else {
            return inputList[index + 1]
        }
    }


}

//fun readNextInputFromFile(input2: File, indexOfInput: Int, getFromPrevPhaseIndex: Int): Int {
//
//    var throwaway = input2.bufferedReader()
//    var getFrom = 0  //input from prev amp - so for A first input = E,0
//    var matchNotFound = true
//
//    while (matchNotFound) {
//        var throwaway = input2.bufferedReader()
//        println("looking for index = $indexOfInput in input2: ${throwaway} starting with $getFromPrevPhaseIndex,")
////        println("looking for index = $indexOfInput in input2: ${input2} starting with $getFromPrevPhaseIndex,")
//
//        var tempList = emptyList<String>().toMutableList()
//
////        input2.forEachLine {
//        throwaway.forEachLine {
//            if (it.substringBefore(",") == getFromPrevPhaseIndex.toString()) tempList.add(it)
//        }
//        println("tempList = $tempList  & count of tempList = ${tempList.count()} for index of $indexOfInput")
//
//        var count = 0 //STUCK IN LOOP - WHY???
//
//        if (tempList.count() >= indexOfInput) {
//            println("got a templist >= the index number required - so use input of that index number $indexOfInput")
//
//            tempList.forEach {
//                println("it = $it  substring to use = ${it.substringAfter(",").toInt()}")
//                if (indexOfInput == 1) {   //count) {
//                    getInputFrom = it.substringAfter(",").toInt()
//                    matchNotFound = false
//                    println("matchFound")
//                }
//                matchNotFound = false
//            }
//        } else println("reading file again ${input2.readLines()}")
//
//    }
//    return getFrom
//}

//    return input2.readLines().//.indexOf(index).substringAfter(",").toInt()//get(index).substringAfter(",").toInt()
//    input2.readLines().last().substringAfter(",").toInt()
//}

/*
--- Day 7: Amplification Circuit ---
Based on the navigational maps, you're going to need to send more power to your ship's thrusters to reach Santa in time. To do this, you'll need to configure a series of amplifiers already installed on the ship.

There are five amplifiers connected in series; each one receives an input signal and produces an output signal. They are connected such that the first amplifier's output leads to the second amplifier's input, the second amplifier's output leads to the third amplifier's input, and so on. The first amplifier's input value is 0, and the last amplifier's output leads to your ship's thrusters.

    O-------O  O-------O  O-------O  O-------O  O-------O
0 ->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-> (to thrusters)
    O-------O  O-------O  O-------O  O-------O  O-------O
The Elves have sent you some Amplifier Controller Software (your puzzle input), a program that should run on your existing Intcode computer. Each amplifier will need to run a copy of the program.

When a copy of the program starts running on an amplifier, it will first use an input instruction to ask the amplifier for its current phase setting (an integer from 0 to 4). Each phase setting is used exactly once, but the Elves can't remember which amplifier needs which phase setting.

The program will then call another input instruction to get the amplifier's input signal, compute the correct output signal, and supply it back to the amplifier with an output instruction. (If the amplifier has not yet received an input signal, it waits until one arrives.)

Your job is to find the largest output signal that can be sent to the thrusters by trying every possible combination of phase settings on the amplifiers. Make sure that memory is not shared or reused between copies of the program.

For example, suppose you want to try the phase setting sequence 3,1,2,4,0, which would mean setting amplifier A to phase setting 3, amplifier B to setting 1, C to 2, D to 4, and E to 0. Then, you could determine the output signal that gets sent from amplifier E to the thrusters with the following steps:

Start the copy of the amplifier controller software that will run on amplifier A. At its first input instruction, provide it the amplifier's phase setting, 3. At its second input instruction, provide it the input signal, 0. After some calculations, it will use an output instruction to indicate the amplifier's output signal.
Start the software for amplifier B. Provide it the phase setting (1) and then whatever output signal was produced from amplifier A. It will then produce a new output signal destined for amplifier C.
Start the software for amplifier C, provide the phase setting (2) and the value from amplifier B, then collect its output signal.
Run amplifier D's software, provide the phase setting (4) and input value, and collect its output signal.
Run amplifier E's software, provide the phase setting (0) and input value, and collect its output signal.
The final output signal from amplifier E would be sent to the thrusters. However, this phase setting sequence may not have been the best one; another sequence might have sent a higher signal to the thrusters.

Here are some example programs:

Max thruster signal 43210 (from phase setting sequence 4,3,2,1,0):

3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0
Max thruster signal 54321 (from phase setting sequence 0,1,2,3,4):

3,23,3,24,1002,24,10,24,1002,23,-1,23,
101,5,23,23,1,24,23,23,4,23,99,0,0
Max thruster signal 65210 (from phase setting sequence 1,0,4,3,2):

3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,
1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0
Try every combination of phase settings on the amplifiers. What is the highest signal that can be sent to the thrusters?

To begin, get your puzzle input.

Answer: 45730

You can also [Share] this puzzle.
#################
Your puzzle answer was 45730.

The first half of this puzzle is complete! It provides one gold star: *

--- Part Two ---
It's no good - in this configuration, the amplifiers can't generate a large enough output signal to produce the thrust you'll need. The Elves quickly talk you through rewiring the amplifiers into a feedback loop:

      O-------O  O-------O  O-------O  O-------O  O-------O
0 -+->| Amp A |->| Amp B |->| Amp C |->| Amp D |->| Amp E |-.
   |  O-------O  O-------O  O-------O  O-------O  O-------O |
   |                                                        |
   '--------------------------------------------------------+
                                                            |
                                                            v
                                                     (to thrusters)
Most of the amplifiers are connected as they were before; amplifier A's output is connected to amplifier B's input, and so on. However, the output from amplifier E is now connected into amplifier A's input. This creates the feedback loop: the signal will be sent through the amplifiers many times.

In feedback loop mode, the amplifiers need totally different phase settings: integers from 5 to 9, again each used exactly once. These settings will cause the Amplifier Controller Software to repeatedly take input and produce output many times before halting. Provide each amplifier its phase setting at its first input instruction; all further input/output instructions are for signals.

Don't restart the Amplifier Controller Software on any amplifier during this process. Each one should continue receiving and sending signals until it halts.

All signals sent or received in this process will be between pairs of amplifiers except the very first signal and the very last signal. To start the process, a 0 signal is sent to amplifier A's input exactly once.

Eventually, the software on the amplifiers will halt after they have processed the final loop. When this happens, the last output signal from amplifier E is sent to the thrusters. Your job is to find the largest output signal that can be sent to the thrusters using the new phase settings and feedback loop arrangement.

Here are some example programs:

Max thruster signal 139629729 (from phase setting sequence 9,8,7,6,5):

3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,
27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5
Max thruster signal 18216 (from phase setting sequence 9,7,8,5,6):

3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,
-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,
53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10
Try every combination of the new phase settings on the amplifier feedback loop. What is the highest signal that can be sent to the thrusters?

Although it hasn't changed, you can still get your puzzle input.

Answer:


You can also [Share] this puzzle.



*/