package main.AdventOfCode

import kotlin.math.abs
import kotlin.math.absoluteValue

class Day3(lines: List<String>) {
    private val wires = lines.map { line ->
        line.splitToSequence(",")
            .mapNotNull {
                it[0] to (it.drop(1).toIntOrNull() ?: return@mapNotNull null)
            }
            .toList()
    }

    private inline fun Iterable<Pair<Char, Int>>.walk(crossinline at: (Int, Int) -> Unit) {
        var x = 0
        var y = 0
        for ((c, n) in this) {
            repeat(n) {
                at(x, y)
                when (c) {
                    'U' -> y++
                    'L' -> x--
                    'D' -> y--
                    'R' -> x++
                }
            }
        }
        at(x, y)
    }

    fun part1(): Int? {
        var result: Int? = null
        val field = mutableMapOf<Pair<Int, Int>, Int>()
        for ((i, wire) in wires.withIndex()) {
            wire.walk { x, y ->
                if ((x != 0 || y != 0) && field.getOrPut(x to y) { i } != i) {
                    val manhattan = x.absoluteValue + y.absoluteValue
                    if (result?.let { it > manhattan } != false) {
                        result = manhattan
                    }
                }
            }
        }
        return result
    }

    fun part2(): Int? {
        var result: Int? = null
        var field = mutableMapOf<Pair<Int, Int>, Int>()
        var distances = mutableMapOf<Pair<Int, Int>, Int>()
        for ((i, wire) in wires.withIndex()) {
            var distance = 0
            wire.walk { x, y ->
                when {
                    x == 0 && y == 0 -> {}
                    x to y !in field -> {
                        field[x to y] = i
                        distances[x to y] = distance
                    }
                    field[x to y] != i -> {
                        val other = distances[x to y]
                        if (other != null && result?.let { it > other + distance } != false) {
                            result = other + distance
                        }
                        if (other?.let { it > distance } != false) {
                            field[x to y] = i
                            distances[x to y] = distance
                        }
                    }
                }
                distance++
            }
        }
        return result
    }
}


fun distanceBetweenTwoCoordinates(inputA: Pair<Int, Int>, inputB: Pair<Int, Int>) =
    (abs(inputA.first - inputB.first) + abs(inputA.second - inputB.second))

fun decodeInstructionToVector(input: String): Pair<Int, Int> {
    val instructions = input.split(",").also(::println)
    var xAxis = 0
    var yAxis = 0

    instructions.forEach {
        when (it.subSequence(0, 1)) {
            "R" -> xAxis += it.substring(1).toInt()
            "L" -> xAxis -= it.substring(1).toInt()
            "U" -> yAxis += it.substring(1).toInt()
            "D" -> yAxis -= it.substring(1).toInt()
            else -> "println(invalid instruction $it)"
        }
    }
    return Pair(xAxis, yAxis)
}

//fun overlap(inputA: Pair<Int, Int>)


fun decodeInstructionToCoordinates(input: String, start: Pair<Int, Int>): List<Pair<Int, Int>> {
    val instructions = input.split(",").also(::println)
    var outputCoordinates = listOf(start)

    instructions.forEach {
        var amountToMove = it.substring(1).toInt()
        when (it.subSequence(0, 1)) {
            "R" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first + 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "L" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first - 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "U" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second + 1)
                    amountToMove -= 1
                }
            }
            "D" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second - 1)
                    amountToMove -= 1
                }
            }
            else -> "println(invalid instruction $it)"
        }
    }

    return outputCoordinates
}

fun decodeInstruction(input1: String, input2: String, start: Pair<Int, Int>): Int {
    val instructions1 = input1.split(",").also(::println)
    val instructions2 = input2.split(",").also(::println)
    var count = 0
    var position = 0
    var output = start
    var match = false
    while (match == false) {
        println(count)
        val coords1 = decodeSomeInstructionToCoordinates(instructions1.subList(0, count), start).also(::println)
        val coords2 = decodeSomeInstructionToCoordinates(instructions2.subList(0, count), start).also(::println)
        val duplicates = findDuplicatePairs(coords1, coords2)
        if (duplicates != listOf(start)) {
            match = true
            output = duplicates[1].also(::println)
            position += (coords1.indexOf(output)).also(::println)
            position += (coords2.indexOf(output)).also(::println)
        } else count += 1
    }

    return position
}


fun decodeSomeInstructionToCoordinates(instructions: List<String>, start: Pair<Int, Int>): List<Pair<Int, Int>> {

    var outputCoordinates = listOf(start)

    instructions.forEach {
        var amountToMove = it.substring(1).toInt()
        when (it.subSequence(0, 1)) {
            "R" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first + 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "L" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first - 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "U" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second + 1)
                    amountToMove -= 1
                }
            }
            "D" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second - 1)
                    amountToMove -= 1
                }
            }
            else -> "println(invalid instruction $it)"
        }
    }

    return outputCoordinates
}


fun decodeInstructionToCoordinatesAndProcessMatch(input: String, start: Pair<Int, Int>): List<Pair<Int, Int>> {
    val instructions = input.split(",").also(::println)
    var outputCoordinates = listOf(start)

    instructions.forEach {
        var amountToMove = it.substring(1).toInt()
        when (it.subSequence(0, 1)) {
            "R" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first + 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "L" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first - 1, outputCoordinates.last().second)
                    amountToMove -= 1
                }
            }
            "U" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second + 1)
                    amountToMove -= 1
                }
            }
            "D" -> {
                while (amountToMove > 0) {
                    outputCoordinates += Pair(outputCoordinates.last().first, outputCoordinates.last().second - 1)
                    amountToMove -= 1
                }
            }
            else -> "println(invalid instruction $it)"
        }
    }

    return outputCoordinates
}

fun findDuplicatePairs(list1: List<Pair<Int, Int>>, list2: List<Pair<Int, Int>>): List<Pair<Int, Int>> {
    var listOutput: List<Pair<Int, Int>> = emptyList()

    listOutput = list1.filter { list2.contains(it) }

//    list1.forEach { list1Pair ->
//        list2.forEach { list2Pair ->
//            if (list1Pair == list2Pair)
//                listOutput += list1Pair
//        }
//    }
    return listOutput
}

fun findDistanceToDuplicatePairs(
    list1: List<Pair<Int, Int>>,
    list2: List<Pair<Int, Int>>,
    startPoint: Pair<Int, Int>
): Int {
    var listOutput: List<Int> = emptyList()
    var output = 0
    list1.forEach { list1Pair ->
        list2.forEach { list2Pair ->
            if (list1Pair == startPoint) {
            } else if (list1Pair == list2Pair)
                listOutput += distanceBetweenTwoCoordinates(list1Pair, startPoint)
        }
    }
    if (listOutput.isNotEmpty()) {
        output = listOutput.sorted().first()
    }
    return output
}

fun findDistanceToFirstDuplicatePairs(
    list1: List<Pair<Int, Int>>,
    list2: List<Pair<Int, Int>>,
    startPoint: Pair<Int, Int>
): Int {
    var listOutput: List<Int> = emptyList()
    var output = 0
    var c = 0
    println(list1.count())
    while (c < list1.count()) {
        list1.forEach { list1Pair ->
            list2.forEach { list2Pair ->
                if (list1Pair == startPoint) {
                } else if (list1Pair == list2Pair) {
                    println("made it to $list1Pair")
                    listOutput += distanceBetweenTwoCoordinates(list1Pair, startPoint)
                    c = list1.count() + 1
                }
            }
        }
    }
    if (listOutput.isNotEmpty()) {

        output = listOutput.sorted().first()
    }
//    return x+y
    return output
}

fun findDistanceOfPair(list1: List<Pair<Int, Int>>, startPoint: Pair<Int, Int>): Int {
    var output = 0
    list1.forEach {
        var dist = distanceBetweenTwoCoordinates(it, startPoint)
        if (dist < output) {
            output = dist
        }
    }
    return output
}


/*
--- Day 3: Crossed Wires ---
The gravity assist was successful, and you're well on your way to the Venus refuelling station.
During the rush back on Earth, the fuel management system wasn't completely installed,
so that's next on the priority list.

Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a
central port and extend outward on a grid. You trace the path each wire takes as it leaves
the central port, one wire per line of text (your puzzle input).

The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you
need to find the intersection point closest to the central port. Because the wires are on a grid,
use the Manhattan distance for this measurement. While the wires do technically cross right at
the central port where they both start, this point does not count, nor does a wire count as crossing
with itself.

For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o),
it goes right 8, up 5, left 5, and finally down 3:

...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........
Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.

Here are a few more examples:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135
What is the Manhattan distance from the central port to the closest intersection?

To begin, get your puzzle input.

Answer:

 */