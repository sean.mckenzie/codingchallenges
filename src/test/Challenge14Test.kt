package test

import main.integerFold
import main.myFold
//import main.xmyFold
import org.junit.Test
import kotlin.test.assertEquals

class myFoldTest {

    //original example
    val myMultiplyFunction = fun(acc: Int, element: Int) = acc * element

    @Test
    fun `Multiplication Function Example`() {
        assertEquals(24, integerFold(listOf<Int>(4, 3, 2, 1), 1, myMultiplyFunction))
    }

    val convertIntegersToString = fun(acc: String, number: Int) = "$acc$number"

    @Test
    fun `should return a list of 1`() {
        assertEquals("The result is 1", myFold(listOf<Int>(1), "The result is ", convertIntegersToString))
    }

    @Test
    fun `should return a list of 2`() {
        assertEquals("The result is 12", myFold(listOf<Int>(1,2), "The result is ", convertIntegersToString))
    }

    @Test
    fun `should work with an empty list`() {
        assertEquals("The result is ", myFold(listOf<Int>(), "The result is ", convertIntegersToString))
    }

    @Test
    fun `should work with an empty string`() {
        assertEquals("12345", myFold(listOf<Int>(1,2,3,4,5), String(), convertIntegersToString))
    }

    @Test
    fun `should work with an empty string and empty list`() {
        assertEquals(String(), myFold(listOf<Int>(), "", convertIntegersToString))
    }

    @Test
    fun `should work function to reverse the numbers and separate string with colons`() {
        assertEquals("5:4:3:2:1:Is the answer", myFold(listOf<Int>(1,2,3,4,5), "Is the answer", fun(acc: String, number: Int) = "$number:$acc"))
    }

    //Adding test for different types
    @Test
    fun `should add Integers to Float and return Integer`() {
        val addIntegersToFloat = fun(acc: Int, number: Float) = (acc + number).toInt()
        val testResult = (5 + 1.05f + 2.25f).also(::println)

        assertEquals(testResult.toInt(), myFold(listOf(1.00f,2.00f), 5, addIntegersToFloat).also(::println))
    }

}
