package test

import main.addTwoRomanNumerals
import main.romanNumeralToInteger
import main.addTwoRomanNumeralsInteger
import main.integerToRomanNumeral
import org.junit.Test
import kotlin.test.assertEquals

class RomanNumeralTest {

    @Test
    fun `Should Handle I's as 1's`() {
        assertEquals(1, romanNumeralToInteger("I"))
        assertEquals(2, romanNumeralToInteger("II"))
        assertEquals(3, romanNumeralToInteger("III"))
        assertEquals(4, romanNumeralToInteger("IIII"))
    }

    @Test
    fun `should Handle V's as 5's`() {
        assertEquals(5, romanNumeralToInteger("V"))
        assertEquals(10, romanNumeralToInteger("VV"))
        assertEquals(15, romanNumeralToInteger("VVV"))
    }

    @Test
    fun `Should Handle V's when preceded or followed by I's`() {
        assertEquals(4, romanNumeralToInteger("IV"))
        assertEquals(5, romanNumeralToInteger("V"))
        assertEquals(6, romanNumeralToInteger("VI"))
        assertEquals(7, romanNumeralToInteger("VII"))
        assertEquals(8, romanNumeralToInteger("VIII"))
    }

    @Test
    fun `should Handle X's as 10's`() {
        assertEquals(10, romanNumeralToInteger("X"))
        assertEquals(20, romanNumeralToInteger("XX"))
        assertEquals(30, romanNumeralToInteger("XXX"))
        assertEquals(40, romanNumeralToInteger("XXXX"))
    }

    @Test
    fun `should Handle X's when preceded or followed by I's`() {
        assertEquals(9, romanNumeralToInteger("IX"))
        assertEquals(10, romanNumeralToInteger("X"))
        assertEquals(11, romanNumeralToInteger("XI"))
        assertEquals(12, romanNumeralToInteger("XII"))
        assertEquals(13, romanNumeralToInteger("XIII"))
    }

    @Test
    fun `should Handle L's, C's, D's and M's`() {
        assertEquals(50, romanNumeralToInteger("L"))
        assertEquals(100, romanNumeralToInteger("C"))
        assertEquals(500, romanNumeralToInteger("D"))
        assertEquals(1000, romanNumeralToInteger("M"))

        assertEquals(40, romanNumeralToInteger("XL"))
        assertEquals(90, romanNumeralToInteger("XC"))
        assertEquals(400, romanNumeralToInteger("CD"))
        assertEquals(900, romanNumeralToInteger("CM"))
    }


    @Test
    fun `should Handle Combinations of Numerals`() {
        assertEquals(14, romanNumeralToInteger("XIV"))
        assertEquals(79, romanNumeralToInteger("LXXIX"))
        assertEquals(225, romanNumeralToInteger("CCXXV"))
        assertEquals(845, romanNumeralToInteger("DCCCXLV"))
        assertEquals(2022, romanNumeralToInteger("MMXXII"))
        assertEquals(978, romanNumeralToInteger("CMLXXVIII"))
        assertEquals(999, romanNumeralToInteger("CMXCIX"))
        assertEquals(3888, romanNumeralToInteger("MMMDCCCLXXXVIII"))
    }

    @Test
    fun `should Retrun 0 if number is too large`() {
        assertEquals(0, romanNumeralToInteger("MMMMDCCCLXXXVIII"))
    }

    @Test
    fun `should accept two Strings and Return an Integer`() {
        val oneRomanNumeral = "CMLXXVIII" //978
        val twoRomanNumeral = "CMXCIX" //999
        val threeRomanNumeral = "DCCCXLV" //845
        val fourRomanNumeral = "MMXXII" //2022

        assertEquals(1977, addTwoRomanNumeralsInteger(oneRomanNumeral, twoRomanNumeral))
        assertEquals(2867, addTwoRomanNumeralsInteger(threeRomanNumeral, fourRomanNumeral))
    }


    @Test
    fun `should convert 1000's to M's`() {
        assertEquals("M", integerToRomanNumeral(1000))
        assertEquals("MM", integerToRomanNumeral(2000))
        assertEquals("MMM", integerToRomanNumeral(3000))
    }

    @Test
    fun `should convert 100's to C's`() {
        assertEquals("C", integerToRomanNumeral(100))
        assertEquals("CC", integerToRomanNumeral(200))
        assertEquals("CCC", integerToRomanNumeral(300))
    }

    @Test
    fun `should convert integers to roman numerals`() {
        assertEquals(integerToRomanNumeral(14),"XIV")
        assertEquals(integerToRomanNumeral(79),"LXXIX")
        assertEquals(integerToRomanNumeral(225),"CCXXV")
        assertEquals(integerToRomanNumeral(845),"DCCCXLV")
        assertEquals(integerToRomanNumeral(2022),"MMXXII")
        assertEquals(integerToRomanNumeral(978),"CMLXXVIII")
        assertEquals(integerToRomanNumeral(999),"CMXCIX")
        assertEquals(integerToRomanNumeral(3888),"MMMDCCCLXXXVIII")
    }

    @Test
    fun `should accept two Strings and Return a Roman Numeral String`() {
        val oneRomanNumeral = "CMLXXVIII" //978
        val twoRomanNumeral = "CMXCIX" //999
        val threeRomanNumeral = "DCCCXLV" //845
        val fourRomanNumeral = "MMXXII" //2022

        assertEquals("MCMLXXVII", addTwoRomanNumerals(oneRomanNumeral, twoRomanNumeral))
        assertEquals("MMDCCCLXVII", addTwoRomanNumerals(threeRomanNumeral, fourRomanNumeral))
    }
}
