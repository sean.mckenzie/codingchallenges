package test

import main.*
import org.junit.Test
import kotlin.test.assertEquals

class TrolleyTest {

    val shopSmallFull = "****\n" +
            "*  *\n" +
            "*  *\n" +
            "*  *\n" +
            "****\n"

    val shopSmallHoleTop = " ***\n" +
            "*  *\n" +
            "*  *\n" +
            "*  *\n" +
            "****\n"

    val shopSmallHoleBottom = "****\n" +
            "*  *\n" +
            "*  *\n" +
            "*  *\n" +
            "*** \n"

    val shopSmallHoleLeft = "****\n" +
            "*  *\n" +
            "   *\n" +
            "*  *\n" +
            "****\n"

    val shopSmallHoleRight = "****\n" +
            "*  *\n" +
            "*   \n" +
            "*  *\n" +
            "****\n"


    val shop1 = "**************************\n" +
            "*                        *\n" +
            "* ********** *********** *\n" +
            "* ********** *********** *\n" +
            "*                        *\n" +
            "* ********** *********** *\n" +
            "* ********** *********** *\n" +
            "* ********** *********** *\n" +
            "*                        *\n" +
            "**************************\n"

    val shopList1 = arrayOf(
        //12345678901234567890123456
        "**************************", //1
        "*                        *", //2
        "* ********** *********** *", //3
        "* ********** *********** *", //4
        "*                        *", //5
        "* ********** *********** *", //6
        "* ********** *********** *", //7
        "* ********** *********** *", //8
        "*                        *", //9
        "**************************"  //10
    )

    // 1 North 2 East 3 South 4 West
    val north = 1
    val east = 2
    val south = 3
    val west = 4

    //x marks s home position
    val eastFacing =
        "x**N*\n" +
                "W***E\n" +
                "***S*\n" //East Facing Relative To Start = 0,0
    val southFacing =
        "*E*\n" +
                "N*S\n" +
                "***\n" +
                "***\n" +
                "xW*\n" //South Facing Relative To Start = 0,4
    val westFacing =
        "*S***\n" +
                "E***W\n" +
                "*N**x\n" //West Facing Relative To Start = 4,2 (size of board)
    val northFacing =
        "*Wx\n" +
                "***\n" +
                "***\n" +
                "S*N\n" +
                "*E*\n" //North Facing Relative To Start = 2,0

    val boardSmallEastFacing: Board = arrayOf("*N**", "W  E", "*S**")       //1,1 = "Wx E"
    val boardSmallSouthFacing: Board = arrayOf("*E*", "* *", "N S", "*W*")  //1,1 = "NxS"
    val boardSmallWestFacing: Board = arrayOf("**S*", "E  W", "**N*")       //1,1 = "E xW"
    val boardSmallNorthFacing: Board = arrayOf("*W*", "S N", "* *", "*E*")  //1,1 = "SxN"


    @Test
    fun `shopArray returns List of Strings for input of String`() {
        val shopList: Board = shopStringToArray(shop1)
        assertEquals(true, shopList1.contentEquals(shopList))
    }

    @Test
    fun `shopSize returns size of shop as Pair of Coordinates`() {
        assertEquals(Pair(25, 9), shopSizeMaxIndexCoords(shopList1))
        assertEquals(Pair(3, 4), shopSizeMaxIndexCoords(shopStringToArray(shopSmallFull)))
    }

    @Test
    fun `shopStandardBoard returns true if all the edges are stars`() {
        assertEquals(true, isAStandardBoard(shopList1))
        assertEquals(true, isAStandardBoard(shopStringToArray(shopSmallFull)))
        assertEquals(false, isAStandardBoard(shopStringToArray(shopSmallHoleTop)))
        assertEquals(false, isAStandardBoard(shopStringToArray(shopSmallHoleLeft)))
        assertEquals(false, isAStandardBoard(shopStringToArray(shopSmallHoleRight)))
        assertEquals(false, isAStandardBoard(shopStringToArray(shopSmallHoleBottom)))
    }

    @Test
    fun `createTrolleyId returns a 6 random number`() {
        for (i in 0..1000) {
            val randomNumber = createTrolleyId()
            val stringRandomNumberLength = randomNumber.toString().length
            assertEquals(6, stringRandomNumberLength)
        }
    }

    @Test
    fun `findFirstEmptSpace returns coordinates`() {
        val shopSmall23 = "****\n" +
                "****\n" +
                "****\n" +
                "** *\n" +
                "****\n"

        val shopSmall31 = "*****\n" +
                "*** *\n" +
                "**  *\n" +
                "** **\n" +
                "*****\n"

        assertEquals(Pair(1, 1), findCoordsFirstEmptySpace(shopSmallFull))
        assertEquals(Pair(2, 3), findCoordsFirstEmptySpace(shopSmall23))
        assertEquals(Pair(3, 1), findCoordsFirstEmptySpace(shopSmall31))
    }

    @Test
    fun `rotate90Left rotates a board from North to East`() {
        val shopSmall45 =
            "C**D\n" +
                    "****\n" +
                    "****\n" +
                    "**1*\n" +
                    "A**B\n"

        val shopSmall45Rotated =
            "A***C\n" +
                    "*****\n" +
                    "*1***\n" +
                    "B***D\n"

        assertEquals(
            true,
            shopStringToArray(shopSmall45Rotated).contentEquals(shopStringToArray(shopSmall45).rotate90Left())
        )

    }

    @Test
    fun `turnRight and turnLeft return the correct direction`() {

        assertEquals(east, turnRight(north))
        assertEquals(south, turnRight(east))
        assertEquals(west, turnRight(south))
        assertEquals(north, turnRight(west))
        assertEquals(west, turnLeft(north))
        assertEquals(north, turnLeft(east))
        assertEquals(east, turnLeft(south))
        assertEquals(south, turnLeft(west))

    }

    @Test
    fun `turnRightBoardRotate and turnLeftBoardRotate return the board facing the correct direction`() {
        val expectedEast = shopStringToArray(eastFacing)
        val actualRightTurnFromNorth = turnRightBoardRotate(shopStringToArray(northFacing))

        assertEquals(true, expectedEast.contentEquals(actualRightTurnFromNorth)) //north turn right -> east
        assertEquals(
            true,
            shopStringToArray(southFacing).contentEquals(turnRightBoardRotate(shopStringToArray(eastFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(westFacing).contentEquals(turnRightBoardRotate(shopStringToArray(southFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(northFacing).contentEquals(turnRightBoardRotate(shopStringToArray(westFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(eastFacing).contentEquals(turnRightBoardRotate(shopStringToArray(northFacing)))
        )

        val expectedWest = shopStringToArray(westFacing)
        val actualLeftTurnFromNorth = turnLeftBoardRotate(shopStringToArray(northFacing))

        assertEquals(true, expectedWest.contentEquals(actualLeftTurnFromNorth)) //north turn right -> east
        assertEquals(
            true,
            shopStringToArray(southFacing).contentEquals(turnLeftBoardRotate(shopStringToArray(westFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(westFacing).contentEquals(turnLeftBoardRotate(shopStringToArray(northFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(northFacing).contentEquals(turnLeftBoardRotate(shopStringToArray(eastFacing)))
        )
        assertEquals(
            true,
            shopStringToArray(eastFacing).contentEquals(turnLeftBoardRotate(shopStringToArray(southFacing)))
        )
    }


    @Test
    fun `returnBoardRelativeToDirection returns correct view and relative coords`() {
        val turnEast = returnBoardRelativeToDirection(boardSmallEastFacing, east)
        val turnSouth = returnBoardRelativeToDirection(boardSmallEastFacing, south)
        val turnWest = returnBoardRelativeToDirection(boardSmallEastFacing, west)
        val turnNorth = returnBoardRelativeToDirection(boardSmallEastFacing, north)

        assertEquals(true, turnEast.contentEquals(boardSmallEastFacing))
        assertEquals(true, turnSouth.contentEquals(boardSmallSouthFacing))
        assertEquals(true, turnWest.contentEquals(boardSmallWestFacing))
        assertEquals(true, turnNorth.contentEquals(boardSmallNorthFacing))
    }

    @Test
    fun `returnRelativeCoordsAndBoardBasedOnRotation returns correct relative coords`() {
        val startPosition: Coords = Pair(1, 1)

        assertEquals(
            Pair(1, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardSmallEastFacing, east, startPosition).first
        )
        assertEquals(
            Pair(1, 2),
            returnRelativeCoordsAndBoardBasedOnRotation(boardSmallEastFacing, south, startPosition).first
        )
        assertEquals(
            Pair(2, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardSmallEastFacing, west, startPosition).first
        )
        assertEquals(
            Pair(1, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardSmallEastFacing, north, startPosition).first
        )

        val shopSmall45 =
            "C**D\n" +
                    "*1**\n" +
                    "****\n" +
                    "****\n" +
                    "A**B\n"
        val arrayShopSmall45 = shopStringToArray(shopSmall45)

        assertEquals(
            Pair(1, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(arrayShopSmall45, east, startPosition).first
        )
        assertEquals(
            Pair(1, 2),
            returnRelativeCoordsAndBoardBasedOnRotation(arrayShopSmall45, south, startPosition).first
        )
        assertEquals(
            Pair(2, 3),
            returnRelativeCoordsAndBoardBasedOnRotation(arrayShopSmall45, west, startPosition).first
        )
        assertEquals(
            Pair(3, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(arrayShopSmall45, north, startPosition).first
        )

        val boardMedEastFacing: Board = arrayOf(
            "*N******",
            "W   ***E",
            "*   ** *",
            "**     *",
            "*S******"
        )
        assertEquals(
            Pair(1, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, east, Pair(1, 1)).first
        )
        assertEquals(
            Pair(3, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, north, Pair(1, 1)).first
        )
        assertEquals(
            Pair(6, 3),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, west, Pair(1, 1)).first
        )
        assertEquals(
            Pair(1, 6),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, south, Pair(1, 1)).first
        )

        assertEquals(
            Pair(1, 2),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, east, Pair(1, 2)).first
        )
        assertEquals(
            Pair(2, 1),
            returnRelativeCoordsAndBoardBasedOnRotation(boardMedEastFacing, north, Pair(1, 2)).first
        )

    }


    @Test
    fun `returnView returns correct view`() {
        val startPosition: Coords = Pair(1, 1)
        val eastView = returnView(boardSmallEastFacing, east, startPosition)
        val southView = returnView(boardSmallEastFacing, south, startPosition)
        val westView = returnView(boardSmallEastFacing, west, startPosition)
        val northView = returnView(boardSmallEastFacing, north, startPosition)

        assertEquals(true, arrayOf("O").contentEquals(eastView))
        assertEquals(true, emptyArray<String>().contentEquals(westView))
        assertEquals(true, emptyArray<String>().contentEquals(northView))
        assertEquals(true, emptyArray<String>().contentEquals(southView))

        val boardMedEastFacing: Board = arrayOf(
            "*N******",
            "W   ***E",
            "*   ** *",
            "**     *",
            "*S******"
        )
        val eastMedView = returnView(boardMedEastFacing, east, startPosition)
        val northMedView = returnView(boardMedEastFacing, north, startPosition)
        val westMedView = returnView(boardMedEastFacing, west, startPosition)
        val southMedView = returnView(boardMedEastFacing, south, startPosition)

        assertEquals(true, arrayOf("OR", "OR").contentEquals(eastMedView))
        assertEquals(true, emptyArray<String>().contentEquals(northMedView))
        assertEquals(true, emptyArray<String>().contentEquals(westMedView))
        assertEquals(true, arrayOf("OL").contentEquals(southMedView))

        assertEquals(true, arrayOf("OLR", "OLR").contentEquals(returnView(boardMedEastFacing, east, Pair(1, 2))))
        assertEquals(true, arrayOf("OR").contentEquals(returnView(boardMedEastFacing, north, Pair(1, 2))))
        assertEquals(true, emptyArray<String>().contentEquals(returnView(boardMedEastFacing, west, Pair(1, 2))))
        assertEquals(true, emptyArray<String>().contentEquals(returnView(boardMedEastFacing, south, Pair(1, 2))))

        assertEquals(true, arrayOf("OLR").contentEquals(returnView(boardMedEastFacing, east, Pair(2, 2))))
        assertEquals(true, arrayOf("OLR").contentEquals(returnView(boardMedEastFacing, north, Pair(2, 2))))
        assertEquals(true, arrayOf("OR").contentEquals(returnView(boardMedEastFacing, west, Pair(2, 2))))
        assertEquals(true, arrayOf("OL").contentEquals(returnView(boardMedEastFacing, south, Pair(2, 2))))
    }

    @Test
    fun `moveTrolley returns new view and coordinates`() {
        val boardMedEastFacing: Board = arrayOf(
            "*N******",
            "W   ***E",
            "*   ** *",
            "**     *",
            "*S******"
        )

        val cannotMoveSouthView = returnView(boardMedEastFacing, south, Pair(1, 2))
        val newViewSouth = moveTrolley(boardMedEastFacing, south, Pair(1, 2))
        assertEquals(true, emptyArray<String>().contentEquals(cannotMoveSouthView))
        assertEquals(true, cannotMoveSouthView.contentEquals(newViewSouth.second))


        val cannotMoveWestView = returnView(boardMedEastFacing, west, Pair(1, 2))
        val newViewWest = moveTrolley(boardMedEastFacing, west, Pair(1, 2))
        assertEquals(true, emptyArray<String>().contentEquals(cannotMoveWestView))
        assertEquals(true, cannotMoveWestView.contentEquals(newViewWest.second))

        val canMove2EastView = returnView(boardMedEastFacing, east, Pair(1, 2))
        val newViewEast = moveTrolley(boardMedEastFacing, east, Pair(1, 2))
        assertEquals(true, arrayOf("OLR", "OLR").contentEquals(canMove2EastView))
        assertEquals(true, arrayOf("OLR").contentEquals(newViewEast.second))
        assertEquals(Pair(2, 2), newViewEast.first)

        val canMove2NorthView = returnView(boardMedEastFacing, north, Pair(3, 3))
        val newViewNorth = moveTrolley(boardMedEastFacing, north, Pair(3, 3))
        assertEquals(true, arrayOf("OL", "OL").contentEquals(canMove2NorthView))
        assertEquals(true, arrayOf("OL").contentEquals(newViewNorth.second))
        assertEquals(Pair(3, 2), newViewNorth.first)

        val lookN1 = returnView(shopList1, north, Pair(12, 8))
        val lookN2 = moveTrolley(shopList1, north, Pair(12, 8))
        val lookN3 = moveTrolley(shopList1, north, Pair(12, 7))
        assertEquals(true, arrayOf("O", "O", "O", "OLR", "O", "O", "OLR").contentEquals(lookN1))
        assertEquals(true, arrayOf("O", "O", "OLR", "O", "O", "OLR").contentEquals(lookN2.second))
        assertEquals(Pair(12, 7), lookN2.first)
        assertEquals(true, arrayOf("O", "OLR", "O", "O", "OLR").contentEquals(lookN3.second))
        assertEquals(Pair(12, 6), lookN3.first)
    }

    @Test
    fun `encodeReferenceID returns unique encoded reference number`() {
        assertEquals((9999999999999L - 1003004123456L), encodeReferenceID(1, Pair(3, 4), 123456))
        assertEquals((9999999999999L - 3123456678909L), encodeReferenceID(3, Pair(123, 456), 678909))
    }

    @Test
    fun `decodeReferenceID returns decoded reference number`() {
        assertEquals(Triple(1, Pair(3, 4), 123456), decodeReferenceID((9999999999999L - 1003004123456L)))
        assertEquals(Triple(3, Pair(123, 456), 678909), decodeReferenceID((9999999999999L - 3123456678909L)))
        assertEquals(Triple(0, Pair(0, 0), 0), decodeReferenceID((9999999999999L - 0L)))
        assertEquals(Triple(9, Pair(999, 999), 999999), decodeReferenceID((0L)))
    }


    @Test
    fun `shoppingTrolley returns unique referenceID on the first run`() {

        val array1 = arrayOf<String>().toMutableList()
        for (i in 1..100) {
            val (view, referenceID) = shoppingTrolley()
            array1.add(referenceID.toString())
            val trollyLength = referenceID.toString().length
            assertEquals(13, trollyLength)
        }
        val unique1 = array1.distinct()
        assertEquals(100, array1.size)
        assertEquals(100, unique1.size)
    }

    @Test
    fun `shoppingTrolley returns expected view for known referenceID`() {

        val refID = encodeReferenceID(direction = east, coords = Pair(12, 8), trolleyId = 123456)

        val (viewNorth, refID2) = shoppingTrolley("L", refID)
        assertEquals(true, arrayOf("O", "O", "O", "OLR", "O", "O", "OLR").contentEquals(viewNorth))

        val (viewMoveNorth, refID3) = shoppingTrolley("M", refID2)
        assertEquals(true, arrayOf("O", "O", "OLR", "O", "O", "OLR").contentEquals(viewMoveNorth))

        val (viewMoveNorth2, refID4) = shoppingTrolley("M", refID3)
        assertEquals(true, arrayOf("O", "OLR", "O", "O", "OLR").contentEquals(viewMoveNorth2))

        val (viewMoveNorth3, refID5) = shoppingTrolley("M", refID4)
        assertEquals(true, arrayOf("OLR", "O", "O", "OLR").contentEquals(viewMoveNorth3))

        val (viewMoveNorth4, refID6) = shoppingTrolley("M", refID5)
        assertEquals(true, arrayOf("O", "O", "OLR").contentEquals(viewMoveNorth4))

        val (viewTurnRight1, refID7) = shoppingTrolley("R", refID6)
        assertEquals(true, arrayOf("O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "OLR").contentEquals(viewTurnRight1))



    }
}





