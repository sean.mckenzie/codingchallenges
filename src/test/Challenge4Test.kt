package test

import main.Challenge4
import org.junit.Test
import kotlin.test.assertEquals

class Challenge4Test {

    val underTest = Challenge4()


    @Test
    fun `should convert vouchers as per the expected results`() {
        assertEquals(
            "190111:Activated:ffff,190111:Available:cccc,190111:Available:ccdd,190112:Activated:bbbb,190112:Available:aaaa,190110:Redeemed:dddd,190110:Expired:eeee",
            underTest.sortVouchers("190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190111:Available:ccdd,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff")
        )

        assertEquals(
            "190111:Activated:ffff,190111:Available:cccc,190112:Activated:bbbb,190112:Available:aaaa,190110:Redeemed:dddd,190110:Expired:eeee",
            underTest.sortVouchers("190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff")
        )

        assertEquals(
            "190111:Activated:ffff,190111:Available:cccc,190112:Activated:bbbb,190112:Available:aaaa,190110:Redeemed:dddd,190110:Expired:eeee",
            underTest.sortVouchers("190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff")
        )

        assertEquals(
            "190301:Redeemed:aaaa,190301:Redeemed:bbbb,190301:Expired:aaaa,190301:Expired:bbbb,190101:Redeemed:aaaa,190101:Redeemed:bbbb,190101:Expired:aaaa,190101:Expired:bbbb",
            underTest.sortVouchers("190101:Expired:aaaa,190101:Redeemed:aaaa,190101:Expired:bbbb,190101:Redeemed:bbbb,190301:Expired:aaaa,190301:Redeemed:aaaa,190301:Expired:bbbb,190301:Redeemed:bbbb")
            )

        assertEquals(
            "190211:Available:bbbc,190211:Available:bbbd",
            underTest.sortVouchers("190211:Available:bbbd,190211:Available:bbbc")
            )

        assertEquals(
            "190112:Redeemed:aaab,190112:Expired:aaaa",
            underTest.sortVouchers("190112:Expired:aaaa,190112:Redeemed:aaab")
            )

    }

}