package test

import main.*
import org.junit.Test
import kotlin.test.assertEquals

class NodeTest {

    @Test
    fun `getDescription of a Linked List should print the contents ending with a null`() {
        val firstNode = (Node("hello", Node("world")))
        assertEquals("hello world null", getDescription(firstNode))
    }

    @Test
    fun `getDescription of null should be null`() {
        val firstNode = null
        assertEquals("null", getDescription(firstNode))
        assertEquals("null", getDescription(null))
    }

    @Test
    fun `addToList adds a value on the end of a list`() {
        val firstNode = (Node("hello", Node("world")))
        val updatedNode = (Node("hello", Node("world", Node("!"))))
        assertEquals(updatedNode.first()?.item, addToList(firstNode, "!")?.first()?.item)
        assertEquals(updatedNode.last()?.item, addToList(firstNode, "!")?.last()?.item)
        assertEquals("hello world ! null", getDescription(addToList(firstNode, "!")))
    }

    @Test
    fun `addToList adds a value to a null list`() {
        assertEquals("! null", getDescription(addToList(null, "!")))
    }

    @Test
    fun `convertIntToString converts strings to integers`() {
        assertEquals(1, convertStringToInt("1"))
        assertEquals(999, convertStringToInt("999"))
        assertEquals(0, convertStringToInt("0"))
        assertEquals(0, convertStringToInt(""))
        assertEquals(0, convertStringToInt("garbage"))
        assertEquals(0, convertStringToInt("99 99"))
    }

    @Test
    fun `convertLinkedListStringToInt converts list of strings to list of integers`() {
        val linkedStrings = Node("1", Node("2"))
        val linkedStrings2 = Node("10", Node("5", Node("ouch")))
        val linkedStrings3 = Node("10", Node("999", Node("ouch", Node("12345"))))

        val linkedIntegers = Node(1, Node(2))
        val linkedIntegers2 = Node(10, Node(0))
        val linkedIntegers3 = Node(10, Node(12345))

        assertEquals(linkedIntegers.first(), convertLinkedListStringToInt(linkedStrings)?.first())
        assertEquals(2, count(convertLinkedListStringToInt(linkedStrings)))

        assertEquals(linkedIntegers2.last(), convertLinkedListStringToInt(linkedStrings2)?.last())
        assertEquals(3, count(convertLinkedListStringToInt(linkedStrings2)))

        assertEquals(linkedIntegers3.last(), convertLinkedListStringToInt(linkedStrings3)?.last())
        assertEquals(4, count(convertLinkedListStringToInt(linkedStrings3)))
    }

    @Test
    fun `nodeAtIndex will return Node at requested postion in List`() {
        val linkedListAF = Node("a", Node("b", Node("c", Node("d", Node("e", Node("f"))))))
        assertEquals("a b c d e f null", getDescription(linkedListAF))

        assertEquals("f null", getDescription(nodeAtIndex(linkedListAF, 5)))
        assertEquals("e f null", getDescription(nodeAtIndex(linkedListAF, 4)))
        assertEquals("a b c d e f null", getDescription(nodeAtIndex(linkedListAF, 0)))
    }

    @Test
    fun `reverseLinkedList will reverse the Linked List`() {
        val linkedListAB = Node("a", Node("b"))
        assertEquals("b a null", getDescription(reverseLinkedList(linkedListAB)))

        val linkedListAF = Node("a", Node("b", Node("c", Node("d", Node("e", Node("f"))))))
        assertEquals("f e d c b a null", getDescription(reverseLinkedList(linkedListAF)))

        val linkedList1to6 = Node(1, Node(2, Node(3, Node(4, Node(5, Node(6))))))
        assertEquals("6 5 4 3 2 1 null", getDescription(reverseLinkedList(linkedList1to6)))

        val linkedListRandom = Node(1, Node("2", Node(3.0, Node(true, Node("""RAW""", Node(arrayOf(1,2,3,4).joinToString(",","(",")")))))))
        assertEquals("(1,2,3,4) RAW true 3.0 2 1 null", getDescription(reverseLinkedList(linkedListRandom)))

    }

    @Test
    fun `testing for large Linked Lists`() {
        val numberOfLinks = 10000
        var largeLinkedList = addNumbersToLinkedList(null, numberOfLinks)
//        println(getDescription(newLikedList)) //fails c. 6k linked list with StackOverflowError
        println(getDescriptionNonRecursive(largeLinkedList)) //7000 .. 1

        assertEquals(numberOfLinks, count(largeLinkedList))
        assertEquals(numberOfLinks, largeLinkedList?.item)
        assertEquals(null, largeLinkedList?.last()?.item)

        val reversedLinkedList = reverseLinkedList(largeLinkedList)
        println(getDescriptionNonRecursive(reversedLinkedList)) //1 .. 7000

        assertEquals(numberOfLinks, count(reversedLinkedList))
        assertEquals(1, reversedLinkedList?.item)
        assertEquals(null, reversedLinkedList?.last()?.item)

    }

}
