package test

import main.*
import org.junit.Test
import kotlin.test.assertEquals

class PontoonTest {

    @Test
    fun `Should Process the Arrays for number cards`() {
        assertEquals(Pair(2, "score"), processCardsForScoreAndHandType(arrayOf("2C")))
        assertEquals(Pair(7, "score"), processCardsForScoreAndHandType(arrayOf("3C", "4H")))
        assertEquals(Pair(18, "score"), processCardsForScoreAndHandType(arrayOf("3C", "4H", "5S", "6D")))
        assertEquals(Pair(44, "bust"), processCardsForScoreAndHandType(arrayOf("2C", "3C", "4H", "5S", "6D", "7D", "8C", "9S")))
    }


    @Test
    fun `Should Process the Arrays for number and picture cards`() {
        assertEquals(Pair(13, "score"), processCardsForScoreAndHandType(arrayOf("3C", "KH")))
        assertEquals(Pair(10, "score"), processCardsForScoreAndHandType(arrayOf("KC")))
        assertEquals(Pair(20, "score"), processCardsForScoreAndHandType(arrayOf("QD", "KC")))
        assertEquals(Pair(30, "bust"), processCardsForScoreAndHandType(arrayOf("JS", "QD", "KC")))
        assertEquals(Pair(40, "bust"), processCardsForScoreAndHandType(arrayOf("TH", "JS", "QD", "KC")))
        assertEquals(Pair(49, "bust"), processCardsForScoreAndHandType(arrayOf("9C", "TH", "JS", "QD", "KC")))
    }


    @Test
    fun `Should Be Able To Process Ace Cards If Low Or High`() {
        assertEquals(Pair(15, "score"), processCardsForScoreAndHandType(arrayOf("JS", "4C", "AH")))
        assertEquals(Pair(21, "score"), processCardsForScoreAndHandType(arrayOf("JS", "KC", "AH")))
        assertEquals(Pair(14, "score"), processCardsForScoreAndHandType(arrayOf("3C", "AH")))
        assertEquals(Pair(12, "score"), processCardsForScoreAndHandType(arrayOf("AC", "AH")))
        assertEquals(Pair(14, "score"), processCardsForScoreAndHandType(arrayOf("AH", "AC", "AD", "AS")))
        assertEquals(Pair(14, "score"), processCardsForScoreAndHandType(arrayOf("AH", "AC", "AD", "AS", "5S", "5C")))
    }


    @Test
    fun `Should Be Able To Process Special Hands`() {
        assertEquals(Pair(21, "pontoon"), processCardsForScoreAndHandType(arrayOf("JS", "AC")))
        assertEquals(Pair(21, "pontoon"), processCardsForScoreAndHandType(arrayOf("AH", "KC")))
        assertEquals(Pair(21, "score"), processCardsForScoreAndHandType(arrayOf("TC", "AH")))
        assertEquals(Pair(19, "five card trick"), processCardsForScoreAndHandType(arrayOf("AH", "AC", "AD", "AS", "5S")))
        assertEquals(Pair(14, "score"), processCardsForScoreAndHandType(arrayOf("AH", "AC", "AD", "AS", "5S", "5C")))
        assertEquals(Pair(12, "score"), processCardsForScoreAndHandType(arrayOf("AC", "AH")))
        assertEquals(Pair(23, "bust"), processCardsForScoreAndHandType(arrayOf("AC", "AD", "AS", "TD", "QC")))
        //broken cards are ignored
        assertEquals(Pair(11, "invalid hand"), processCardsForScoreAndHandType(arrayOf("XS", "AC")))
        assertEquals(Pair(17, "invalid hand"), processCardsForScoreAndHandType(arrayOf("99H", "AC", "AD", "AS", "5S")))
    }


    @Test
    fun `Should parse two hands and determine the winner`() {
        //Testing Examples: player hand vs dealer hand
        //pontoon vs pontoon
        val dealerWins1 = checkPontoonWinner(arrayOf("JS", "AC"), arrayOf("JH", "AD")).also(::println)
        //invalid vs invalid
        val dealerWins2 = checkPontoonWinner(arrayOf("XS", "AC"), arrayOf("JHX", "AD")).also(::println)
        //bust vs any hand
        val dealerWins3 = checkPontoonWinner(arrayOf("QH","JS", "KC"), arrayOf("KS", "JH", "AD")).also(::println)
        //five card trick vs five card trick
        val dealerWins4 = checkPontoonWinner(arrayOf("AH", "AC", "AD", "AS", "5S"), arrayOf("JH", "AD", "2D", "3C", "2S")).also(::println)
        //score vs five card trick
        val dealerWins5 = checkPontoonWinner(arrayOf("TH", "AC"), arrayOf("JH", "AD", "2D", "3C", "2S")).also(::println)
        //score vs score
        val dealerWins6 = checkPontoonWinner(arrayOf("TH", "AC"), arrayOf("TC", "5S", "6D")).also(::println)
        //five card trick vs pontoon
        val dealerWins7 = checkPontoonWinner(arrayOf("JC", "AS", "2D", "3C", "2S"), arrayOf("JH", "AD")).also(::println)
        //score vs higher score
        val dealerWins8 = checkPontoonWinner(arrayOf("9H", "AC"), arrayOf("TC", "5S", "6D")).also(::println)

        //pontoon vs score
        val playerWins1 = checkPontoonWinner(arrayOf("JS", "AC"), arrayOf("TS", "AC")).also(::println)
        //score vs invalid
        val playerWins2 = checkPontoonWinner(arrayOf("TS", "AC"), arrayOf("JHX", "AD")).also(::println)
        //score vs bust
        val playerWins3 = checkPontoonWinner(arrayOf("JH", "KD"),arrayOf("QH","JS", "KC")).also(::println)
        //five card trick vs score
        val playerWins4 = checkPontoonWinner(arrayOf("AH", "AC", "AD", "AS", "5S"), arrayOf("KH", "JH", "AD")).also(::println)
        //pontoon vs bust
        val playerWins5 = checkPontoonWinner(arrayOf("JH", "AD"),arrayOf("QH","JS", "KC")).also(::println)
        //pontoon vs five card trick
        val playerWins6 = checkPontoonWinner(arrayOf("JH", "AD"),arrayOf("QH", "AS", "2D", "3C", "2S")).also(::println)
        //higher score vs score
        val playerWins7 = checkPontoonWinner(arrayOf("TC", "5S", "6D"), arrayOf("9H", "AC")).also(::println)


        assertEquals("The dealer wins", dealerWins1.substring(0,15))
        assertEquals("The dealer wins", dealerWins2.substring(0,15))
        assertEquals("The dealer wins", dealerWins3.substring(0,15))
        assertEquals("The dealer wins", dealerWins4.substring(0,15))
        assertEquals("The dealer wins", dealerWins5.substring(0,15))
        assertEquals("The dealer wins", dealerWins6.substring(0,15))
        assertEquals("The dealer wins", dealerWins7.substring(0,15))
        assertEquals("The dealer wins", dealerWins8.substring(0,15))

        assertEquals("The player wins", playerWins1.substring(0,15))
        assertEquals("The player wins", playerWins2.substring(0,15))
        assertEquals("The player wins", playerWins3.substring(0,15))
        assertEquals("The player wins", playerWins4.substring(0,15))
        assertEquals("The player wins", playerWins5.substring(0,15))
        assertEquals("The player wins", playerWins6.substring(0,15))
        assertEquals("The player wins", playerWins7.substring(0,15))
    }

}
