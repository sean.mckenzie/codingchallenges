package test

import main.Shopopoly.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class ShopopolyTest {

    @Test
    fun `diceReturnsTwoRandomNumbers`() {
        var count = 0
        while (count < 100) {
            val dice = Dice().also(::println)

            assertTrue(dice.diceOne > 0)
            assertTrue(dice.diceOne < 7)
            assertTrue(dice.diceTwo > 0)
            assertTrue(dice.diceTwo < 7)
            assertTrue(dice.totalScore > 1 && dice.totalScore < 13)
            count += 1
        }
    }

    @Test
    fun `toStringPrintsCorrectlyForAThrowOf2`() {
        var dice = Dice()
        while (dice.totalScore != 2) {
            dice = Dice()
        }
        assertTrue("$dice" == "You threw a 1 and a 1")
    }

    @Test
    fun `toStringPrintsCorrectlyForAThrowOf11`() {
        var dice = Dice()
        while (dice.totalScore != 11) {
            dice = Dice()
        }
        assertTrue("$dice" == "You threw a 5 and a 6" || "$dice" == "You threw a 6 and a 5")
    }

    @Test
    fun `shouldReturnAllValuesPossibleIn1000Throws`() {
        val correctList = listOf(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
        val diceRolls = mutableListOf<Int>()
        repeat(1000) {
            val dice = Dice()
            diceRolls.add(dice.diceOne + dice.diceTwo)
        }
        assertTrue(diceRolls.containsAll(correctList))
    }


    @Test
    fun `moneyIsReturnedZeroOrPositiveFromGBPTest`() {
        val ten = GBP(10)
        val zero = GBP(0)
        val minusOne = GBP(-1)
        assertEquals(ten.toString(), "£10")
        assertEquals(zero.toString(), "£0")
        assertEquals(minusOne.toString(), "£0")
    }

    @Test
    fun `freeParkingIsALocationTest`() {
        val freeParking = Location.FreeParking
        assertTrue(freeParking is Location.FreeParking)
    }

    @Test
    fun `goIsALocationTest`() {
        val go = Location.Go
        assertTrue(go is Location.Go)
    }

    @Test
    fun `go100RewardTest`() {
        val go = Location.Go
        assertEquals(GBP(100), go.reward)
    }

    @Test
    fun `factoryOrWarehouseTests`() {
        val warehouse = Location.FactoryOrWarehouse("Magna Park")
        assertTrue(warehouse is Location)
        assertTrue(warehouse is Location.FactoryOrWarehouse)
        assertTrue(warehouse is Rentable)
        assertTrue(warehouse is Purchaseable)
        assertEquals(GBP(100), warehouse.cost)
        assertEquals(GBP(20), warehouse.rent)
        assertEquals("Magna Park", warehouse.name)
    }

    @Test
    fun `retailSiteTests`() {
        val retailSite = Location.RetailSite(
            name = "Oxford Street",
            group = Group.Blue,
            cost = GBP(999),
            retailRentalValues = RetailRentalValues(
                rentUndeveloped = GBP(50),
                rentMinistore = GBP(100),
                rentSupermarket = GBP(150),
                rentMegastore = GBP(200)
            ),
            retailBuildingCosts = RetailBuildingCosts(
                costMinistore = GBP(300),
                costSupermarket = GBP(400),
                costMegastore = GBP(500)
            )
        )
        assertTrue(retailSite is Location)
        assertTrue(retailSite is Location.RetailSite)
        assertTrue(retailSite is Rentable)
        assertTrue(retailSite is Purchaseable)
        assertTrue(retailSite is Developable)
        assertEquals("Oxford Street", retailSite.name)
        assertEquals(Group.Blue, retailSite.group)
        assertEquals(GBP(999), retailSite.cost)
        assertEquals(GBP(50), retailSite.retailRentalValues.rentUndeveloped)
        assertEquals(GBP(100), retailSite.retailRentalValues.rentMinistore)
        assertEquals(GBP(150), retailSite.retailRentalValues.rentSupermarket)
        assertEquals(GBP(200), retailSite.retailRentalValues.rentMegastore)
        assertEquals(GBP(300), retailSite.retailBuildingCosts.costMinistore)
        assertEquals(GBP(400), retailSite.retailBuildingCosts.costSupermarket)
        assertEquals(GBP(500), retailSite.retailBuildingCosts.costMegastore)
    }
}

class BoardTest {
    val ledger = GameLedger
    val bank = object : Player {
        override val name = "Bank"
        override var boardLocation = 0
    }
    val playerA = object : Player {
        override val name = "PlayerA"
        override var boardLocation = 1
    }
    val playerB = object : Player {
        override val name = "PlayerA"
        override var boardLocation = 1
    }

    @Test
    fun `diceAddedToPlayerLocation`() {
        //Board size of 15, Go is location 1
        ledger.addNewPlayer(bank, playerA, GBP(500))
        assertEquals(1, playerA.boardLocation)

        val diceThrow1 = 5
        val passGo1 = playerA.updateLocation(15, diceThrow1)
        assertEquals(6, playerA.boardLocation)
        assertFalse(passGo1)

        val diceThrow2 = 9
        val passGo2 = playerA.updateLocation(15, diceThrow2)
        assertEquals(15, playerA.boardLocation)
        assertFalse(passGo2)

        val diceThrow3 = 2
        val passGo3 = playerA.updateLocation(15, diceThrow3)
        assertEquals(2, playerA.boardLocation)
        assertTrue(passGo3)

        //With a boardsize of 13 you will not land on same square with a dice roll
        var count = 0
        while (count < 100) {
            val dice = Dice().totalScore
            var currentLocation = playerB.boardLocation
            playerB.updateLocation(13, dice)
            assertTrue(playerB.boardLocation != currentLocation)
            count += 1
        }
    }

}
