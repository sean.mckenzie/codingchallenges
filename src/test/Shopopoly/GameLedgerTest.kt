package test

//import main.Shopopoly.GameLedger.playerBalance
import main.Shopopoly.GBP
import main.Shopopoly.GameLedger
import main.Shopopoly.GameLedger.playerCredit
import main.Shopopoly.Player
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals


class TransactionTest {
    @Before
    fun `deleteAllTransactions`() {
        GameLedger.transactions.clear()
    }


    val ledger = GameLedger
    val bank = object : Player {
        override val name = "Bank"
        override var boardLocation = 0
    }
    val playerA = object : Player {
        override val name = "PlayerA"
        override var boardLocation = 1
    }
    val playerB = object : Player {
        override val name = "PlayerB"
        override var boardLocation = 1
    }

    @Test
    fun `transactionAddedToGameLedgerForNewPlayer`() {
        val originalSize = ledger.transactions.size

        ledger.addNewPlayer(bank, playerA, GBP(500))
        ledger.addNewPlayer(bank, playerB, GBP(1000))
        assertEquals(ledger.transactions.size, originalSize + 2)
        assertEquals(ledger.transactions.first().amount, GBP(500))
        assertEquals(ledger.transactions.first().debitAccount, bank)
        assertEquals(ledger.transactions.first().creditAccount, playerA)
        assertEquals(ledger.transactions.last().amount, GBP(1000))
        assertEquals(ledger.transactions.last().debitAccount, bank)
        assertEquals(ledger.transactions.last().creditAccount, playerB)
    }

    @Test
    fun `transactionAddedToGameLedgerWhenPlayerPassesGo`() {
        ledger.feeForPlayerPassingGo(bank, playerA, GBP(100))
        assertEquals(ledger.transactions.last().debitAccount, bank)
        assertEquals(ledger.transactions.last().creditAccount, playerA)
        assertEquals(ledger.transactions.last().amount, GBP(100))

        ledger.feeForPlayerPassingGo(bank, playerB, GBP(100))
        assertEquals(ledger.transactions.last().debitAccount, bank)
        assertEquals(ledger.transactions.last().creditAccount, playerB)
        assertEquals(ledger.transactions.last().amount, GBP(100))
    }

    @Test
    fun `transactionAddedToGameLedgerWhenPlayerPurchasesLocation`() {
        ledger.feeForPlayerBuyingLocation(playerA, bank, GBP(666))
        assertEquals(ledger.transactions.last().debitAccount, playerA)
        assertEquals(ledger.transactions.last().amount, GBP(666))

        ledger.feeForPlayerBuyingLocation(playerB, bank, GBP(777))
        assertEquals(ledger.transactions.last().debitAccount, playerB)
        assertEquals(ledger.transactions.last().amount, GBP(777))

        ledger.feeForPlayerBuyingMinistore(playerA, bank, GBP(250))
        assertEquals(ledger.transactions.last().amount, GBP(250))

        ledger.feeForPlayerBuyingSupermarket(playerA, bank, GBP(350))
        assertEquals(ledger.transactions.last().amount, GBP(350))

        ledger.feeForPlayerBuyingMegastore(playerA, bank, GBP(450))
        assertEquals(ledger.transactions.last().amount, GBP(450))
    }

    @Test
    fun `transactionAddedToGameLedgerWhenPlayerPaysRent`() {
        ledger.feeForPlayerPayingRent(playerA, playerB, GBP(555))
        assertEquals(ledger.transactions.last().amount, GBP(555))

    }


    private fun printLedger() {
        ledger.transactions.forEach {
            println(
                " debit: " + it.debitAccount.name + " location: " + it.debitAccount.boardLocation + " "
                        + " credit: " + it.creditAccount.name + " location: " + it.creditAccount.boardLocation + " "
                        + it.amount + " "
                        + it.transactionType
            )
        }
    }

    @After
    fun `print ledger`() {
        printLedger()
    }

}

class BalanceTest {
    @Before
    fun `deleteAllTransactions`() {
        GameLedger.transactions.clear()
    }

    val ledger = GameLedger
    val bank = object : Player {
        override val name = "Bank"
        override var boardLocation = 0
    }
    val playerA = object : Player {
        override val name = "PlayerA"
        override var boardLocation = 1
    }
    val playerB = object : Player {
        override val name = "PlayerB"
        override var boardLocation = 1
    }


    @Test
    fun `balanceTest`() {

        ledger.addNewPlayer(bank, playerA, GBP(2000))
        ledger.addNewPlayer(bank, playerB, GBP(500))
        ledger.feeForPlayerBuyingLocation(playerA, bank, GBP(666))
        ledger.feeForPlayerBuyingLocation(playerB, bank, GBP(111))
        ledger.feeForPlayerBuyingMinistore(playerA, bank, GBP(250))
        ledger.feeForPlayerPassingGo(bank, playerA, GBP(200))
        ledger.feeForPlayerPassingGo(bank, playerB, GBP(100))
        ledger.feeForPlayerPayingRent(playerA, playerB, GBP(555))

        val balancePlayerA = playerCredit(playerA, ledger)
        assertEquals(GBP(729), balancePlayerA) //+2000-666-250+200-555

        val balancePlayerB = playerCredit(playerB, ledger)
        assertEquals(GBP(1044), balancePlayerB) //+500-111+100+555

        //payout more than all the money - leaves nothing, not negative
        ledger.feeForPlayerPayingRent(playerB, playerA, GBP(9999))
        val newBalancePlayerB = playerCredit(playerB, ledger)
        assertEquals(GBP(0), newBalancePlayerB) //1044-9999

    }

}
