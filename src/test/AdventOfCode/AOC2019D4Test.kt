package test.AdventOfCode

import main.AdventOfCode.*
import org.junit.Test
import kotlin.test.assertEquals

class Day4Test {

    @Test
    fun `processDigits should process a 6 digit number one character at a time`() {
        assertEquals("1-2-3-4-5-6", processDigits(123456))
    }


    @Test
    fun `processDigitsSameOrBigger should process a 6 digit number one character at a time`() {
        assertEquals(true, processDigitsSameOrBigger(123456))
        assertEquals(true, processDigitsSameOrBigger(123455))
        assertEquals(false, processDigitsSameOrBigger(123454))
    }


    @Test
    fun `processDigitsSameOrBiggerX should process a 6 digit number one character at a time`() {
        assertEquals("bigger", processDigitsSameOrBiggerX(123456))
        assertEquals("same", processDigitsSameOrBiggerX(123455))
        assertEquals("false", processDigitsSameOrBiggerX(123454))
    }

    @Test
    fun `processDigitsSameOrBiggerExcludeTrebles should process a 6 digit number one character at a time`() {
        println("processDigitsSameOrBiggerExcludeTrebles")
        assertEquals("bigger", processDigitsSameOrBiggerExcludeTrebles(123456))
//        assertEquals("same", processDigitsSameOrBiggerExcludeTrebles(123455))
//        assertEquals("false", processDigitsSameOrBiggerExcludeTrebles(123454))
    }

    @Test
    fun `createRange should produe a list of 6 digit numbers from and to`() {
        assertEquals(listOf(123456,123457), createRange(start = 123456,end = 123457))
//        assertEquals(expected = listOf(123456,123457,123458), actual = createRange(start = 000001,end = 000002))
        assertEquals(expected = listOf(123456,123457,123458), actual = createRange(123456,123458))
        assertEquals(expected = listOf(123456,123457,123458,123459), actual = createRange(123456,123459))
        assertEquals(expected = listOf(111111,111112), actual = createRange(108457,111112))
        assertEquals(expected = 2, actual = createRange(108457,111112).count())
    }
    //108457-562041  - guess 2863 = too high

    @Test
    fun `createRangeMustIncludeDouble should produe a list of 6 digit numbers from and to`() {
        assertEquals(listOf(123455), createRangeMustIncludeDouble(start = 123455,end = 123457))
        assertEquals(expected = 2, actual = createRangeMustIncludeDouble(108457,111112).count())
        assertEquals(expected = 2779, actual = createRangeMustIncludeDouble(108457,562041).count())
    }
   //108457-562041  - guess 2779 = CORRECT *******

    @Test
    fun `createRangeMustIncludeDoubleNotInLargerGroup should produe a list of 6 digit numbers from and to`() {
        assertEquals(listOf(111122), createRangeMustIncludeDoubleNotInLargerGroup(start = 111122,end = 111123))
        assertEquals(listOf(122233), createRangeMustIncludeDoubleNotInLargerGroup(start = 122232,end = 122234))
        assertEquals(listOf(112233,112234), createRangeMustIncludeDoubleNotInLargerGroup(start = 112230,end = 112234))
        assertEquals(listOf(114444,114445,114446), createRangeMustIncludeDoubleNotInLargerGroup(start = 114444,end = 114446))
        assertEquals(listOf(114444,114445,114446), createRangeMustIncludeDoubleNotInLargerGroup(start = 114444,end = 114446))
        assertEquals(expected = 2, actual = createRangeMustIncludeDoubleNotInLargerGroup(108457,111134).count())
        assertEquals(expected = 1972, actual = createRangeMustIncludeDoubleNotInLargerGroup(108457,562041).count())
    }
   //part 2:  108457-562041  - guess 1972 = CORRECT *******


    @Test
    fun `runMyList should print a list of 6 digit numbers`() {
        assertEquals("""
            1-2-3-4-5-61-2-3-4-5-7
        """.trimIndent(), runMyList(listOf(123456,123457)))
//        assertEquals(expected = listOf(123456,123457,123458), actual = createRange(start = 000001,end = 000002))
//        assertEquals(expected = listOf(123456,123457,123458), actual = createRange(123456,123458))
//        assertEquals(expected = listOf(123456,123457,123458,123459), actual = createRange(123456,123459))
//        assertEquals(expected = listOf(123456,123457,123458,123459), actual = createRange(108457,562041))
    }
    //108457-562041







}