package test.AdventOfCode

import main.AdventOfCode.intCodeUpgradePart2
import main.AdventOfCode.parameterModeResult
import main.AdventOfCode.processInputOpCode
import main.AdventOfCode.processInputParameters
import org.junit.Test
import kotlin.test.assertEquals

class Day5Test {

    @Test
    fun `processInputOpCode can recognise OpCodes from an integer`() {
        assertEquals("add", processInputOpCode(1001))
        assertEquals("multiply", processInputOpCode(1002))
        assertEquals("input", processInputOpCode(1003))
        assertEquals("output", processInputOpCode(1004))
        assertEquals("stop", processInputOpCode(1099))
        assertEquals("jump-if-true", processInputOpCode(1005))
        assertEquals("jump-if-false", processInputOpCode(1006))
        assertEquals("less-than", processInputOpCode(1007))
        assertEquals("equals", processInputOpCode(1008))
    }

    @Test
    fun `processInputParameters can recognise OpCode 1 from an integer`() {
        assertEquals("010", processInputParameters(1001))
        assertEquals("010", processInputParameters(1002))
        assertEquals("100", processInputParameters(10099))
        assertEquals("001", processInputParameters(103))
    }

    @Test
    fun `parameterModeResult can action OpCodes at a given index from a list of Integers`() {
        val inputAdd1 = arrayListOf(1001,4,3,4,33)
        val inputMultiply1 = arrayListOf(1002,4,3,4,33)


        assertEquals(Pair(arrayListOf(1001,4,3,4,36),Triple(0,"add",0)), parameterModeResult(inputAdd1, 0, 0,0))
        assertEquals(Pair(arrayListOf(1002,4,3,4,99),Triple(0,"multiply",0)), parameterModeResult(inputMultiply1, 0, 0,0))
        assertEquals(Pair(arrayListOf(1101,100,-1,4,99),Triple(0,"add",0)), parameterModeResult(arrayListOf(1101,100,-1,4,0), 0, 0,0))
        assertEquals(Pair(arrayListOf(1102,100,-20,4,-2000),Triple(0,"multiply",0)), parameterModeResult(arrayListOf(1102,100,-20,4,99), 0, 0,0))
        assertEquals(Pair(arrayListOf(102,100,4,4,9900),Triple(0,"multiply",0)), parameterModeResult(arrayListOf(102,100,4,4,99), 0, 0,0))
        assertEquals(Pair(arrayListOf(2,3,4,4,396),Triple(0,"multiply",0)), parameterModeResult(arrayListOf(2,3,4,4,99), 0, 0,0))
    //input & output
        assertEquals(Pair(arrayListOf(9999,0,99), Triple(9999,"input",0)), parameterModeResult(arrayListOf(3,0,99), 0, 9999,0))
        assertEquals(Pair(arrayListOf(9999,0,99), Triple(9999,"input",0)), parameterModeResult(arrayListOf(103,0,99), 0, 9999,0))
        assertEquals(Pair(arrayListOf(4,2,99), Triple(99,"output",99)), parameterModeResult(arrayListOf(4,2,99), 0, 9999,0))
        assertEquals(Pair(arrayListOf(104,2,99), Triple(2,"output",2)), parameterModeResult(arrayListOf(104,2,99), 0, 9999,0))
    //stop
    assertEquals(Pair(arrayListOf(99,777), Triple(0,"stop",0)), parameterModeResult(arrayListOf(99,777), 0, 666,0))
    assertEquals(Pair(arrayListOf(99,777), Triple(0,"stop",0)), parameterModeResult(arrayListOf(99,777), 0, 666,0))

}

    @Test
    fun `intCodePart2 can correctly process for list of Integers`() {

        assertEquals(Pair(arrayListOf(50,0,4,0,99),Triple(50,"output",50)), intCodeUpgradePart2(arrayListOf(3,0,4,0,99),50))
        assertEquals(Pair(arrayListOf(50,0,104,0,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,0,104,0,99),50))

        println("part one full result starts here")
        val fullInput = arrayListOf(3,225,1,225,6,6,1100,1,238,225,104,0,1101,11,91,225,1002,121,77,224,101,-6314,224,224,4,224,1002,223,8,223,1001,224,3,224,1,223,224,223,1102,74,62,225,1102,82,7,224,1001,224,-574,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,28,67,225,1102,42,15,225,2,196,96,224,101,-4446,224,224,4,224,102,8,223,223,101,6,224,224,1,223,224,223,1101,86,57,225,1,148,69,224,1001,224,-77,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1101,82,83,225,101,87,14,224,1001,224,-178,224,4,224,1002,223,8,223,101,7,224,224,1,223,224,223,1101,38,35,225,102,31,65,224,1001,224,-868,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,57,27,224,1001,224,-84,224,4,224,102,8,223,223,1001,224,7,224,1,223,224,223,1101,61,78,225,1001,40,27,224,101,-89,224,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1008,677,226,224,1002,223,2,223,1006,224,329,101,1,223,223,8,226,677,224,102,2,223,223,1005,224,344,101,1,223,223,1107,226,677,224,102,2,223,223,1006,224,359,101,1,223,223,1007,226,226,224,102,2,223,223,1006,224,374,101,1,223,223,7,677,677,224,102,2,223,223,1005,224,389,1001,223,1,223,108,677,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,226,224,102,2,223,223,1005,224,419,1001,223,1,223,1107,677,226,224,102,2,223,223,1005,224,434,1001,223,1,223,1108,677,677,224,102,2,223,223,1006,224,449,1001,223,1,223,7,226,677,224,102,2,223,223,1005,224,464,101,1,223,223,1008,677,677,224,102,2,223,223,1005,224,479,101,1,223,223,1007,226,677,224,1002,223,2,223,1006,224,494,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,509,101,1,223,223,1007,677,677,224,1002,223,2,223,1006,224,524,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,539,101,1,223,223,107,226,677,224,102,2,223,223,1005,224,554,1001,223,1,223,7,677,226,224,102,2,223,223,1006,224,569,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,584,101,1,223,223,1107,677,677,224,102,2,223,223,1005,224,599,101,1,223,223,1108,226,677,224,102,2,223,223,1006,224,614,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,629,101,1,223,223,108,226,677,224,102,2,223,223,1005,224,644,1001,223,1,223,108,226,226,224,102,2,223,223,1005,224,659,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226)
        val fullOutput = intCodeUpgradePart2(fullInput,1)
        assertEquals("output",fullOutput.second.second)

        println("part one full result ends here, answer = ${fullOutput.second.first}")

        assertEquals(7286649,fullOutput.second.first)

    }

    @Test
    fun `parameterModeResult can action new OpCodes`() {

        //equals
        assertEquals(Pair(arrayListOf(3,9,8,9,10,9,4,9,99,1,8),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,9,8,9,10,9,4,9,99,-1,8), 8))
        assertEquals(Pair(arrayListOf(3,9,8,9,10,9,4,9,99,0,8),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,9,8,9,10,9,4,9,99,-1,8), 88))
        assertEquals(Pair(arrayListOf(3,3,1108,1,8,3,4,3,99),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,3,1108,-1,8,3,4,3,99), 8))
        assertEquals(Pair(arrayListOf(3,3,1108,0,8,3,4,3,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,3,1108,-1,8,3,4,3,99), 88))

        //less than
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,1,8),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 7))
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,0,8),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 777))
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,0,8),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 8))
        assertEquals(Pair(arrayListOf(3,3,1107,1,8,3,4,3,99),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 7))
        assertEquals(Pair(arrayListOf(3,3,1107,0,8,3,4,3,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 77))
        assertEquals(Pair(arrayListOf(3,3,1107,0,8,3,4,3,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 8))

    }

    @Test
    fun `parameterModeResult works for provided input`() {

        var inputArray1 = arrayListOf(3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9)
        var inputArray2 = arrayListOf(3,3,1105,-1,9,1101,0,0,12,4,12,99,1)
        var inputArray3 = arrayListOf(3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99)

        //jump-to-if-false
        assertEquals(0, intCodeUpgradePart2(inputArray1,0).second.first)
        assertEquals(1, intCodeUpgradePart2(inputArray1,1).second.first)
        assertEquals(1, intCodeUpgradePart2(inputArray1,9999).second.first)

        //jump to if true
        assertEquals(0, intCodeUpgradePart2(inputArray2,0).second.first)
        assertEquals(1, intCodeUpgradePart2(inputArray2,1).second.first)
        assertEquals(1, intCodeUpgradePart2(inputArray2,9999).second.first)

        //longer example - The above example program uses an input instruction to ask for a single number.
        // The program will then
        // output 999 if the input value is below 8,
        // output 1000 if the input value is equal to 8, or
        // output 1001 if the input value is greater than 8.
        println("here longer example")
        assertEquals(999, intCodeUpgradePart2(inputArray3,7).second.third)
        assertEquals(1000, intCodeUpgradePart2(inputArray3,8).second.third)
        assertEquals(1001, intCodeUpgradePart2(inputArray3,9).second.third)
        assertEquals(1001, intCodeUpgradePart2(inputArray3,2222).second.third)

        //less than
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,1,8),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 7))
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,0,8),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 777))
        assertEquals(Pair(arrayListOf(3,9,7,9,10,9,4,9,99,0,8),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,9,7,9,10,9,4,9,99,-1,8), 8))
        assertEquals(Pair(arrayListOf(3,3,1107,1,8,3,4,3,99),Triple(1,"output",1)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 7))
        assertEquals(Pair(arrayListOf(3,3,1107,0,8,3,4,3,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 77))
        assertEquals(Pair(arrayListOf(3,3,1107,0,8,3,4,3,99),Triple(0,"output",0)), intCodeUpgradePart2(arrayListOf(3,3,1107,-1,8,3,4,3,99), 8))

        //final answer
        println("FINAL ANSWER")
        val fullInput = arrayListOf(3,225,1,225,6,6,1100,1,238,225,104,0,1101,11,91,225,1002,121,77,224,101,-6314,224,224,4,224,1002,223,8,223,1001,224,3,224,1,223,224,223,1102,74,62,225,1102,82,7,224,1001,224,-574,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,28,67,225,1102,42,15,225,2,196,96,224,101,-4446,224,224,4,224,102,8,223,223,101,6,224,224,1,223,224,223,1101,86,57,225,1,148,69,224,1001,224,-77,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1101,82,83,225,101,87,14,224,1001,224,-178,224,4,224,1002,223,8,223,101,7,224,224,1,223,224,223,1101,38,35,225,102,31,65,224,1001,224,-868,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,57,27,224,1001,224,-84,224,4,224,102,8,223,223,1001,224,7,224,1,223,224,223,1101,61,78,225,1001,40,27,224,101,-89,224,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1008,677,226,224,1002,223,2,223,1006,224,329,101,1,223,223,8,226,677,224,102,2,223,223,1005,224,344,101,1,223,223,1107,226,677,224,102,2,223,223,1006,224,359,101,1,223,223,1007,226,226,224,102,2,223,223,1006,224,374,101,1,223,223,7,677,677,224,102,2,223,223,1005,224,389,1001,223,1,223,108,677,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,226,224,102,2,223,223,1005,224,419,1001,223,1,223,1107,677,226,224,102,2,223,223,1005,224,434,1001,223,1,223,1108,677,677,224,102,2,223,223,1006,224,449,1001,223,1,223,7,226,677,224,102,2,223,223,1005,224,464,101,1,223,223,1008,677,677,224,102,2,223,223,1005,224,479,101,1,223,223,1007,226,677,224,1002,223,2,223,1006,224,494,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,509,101,1,223,223,1007,677,677,224,1002,223,2,223,1006,224,524,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,539,101,1,223,223,107,226,677,224,102,2,223,223,1005,224,554,1001,223,1,223,7,677,226,224,102,2,223,223,1006,224,569,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,584,101,1,223,223,1107,677,677,224,102,2,223,223,1005,224,599,101,1,223,223,1108,226,677,224,102,2,223,223,1006,224,614,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,629,101,1,223,223,108,226,677,224,102,2,223,223,1005,224,644,1001,223,1,223,108,226,226,224,102,2,223,223,1005,224,659,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226)
        assertEquals(15724522, intCodeUpgradePart2(fullInput,5).second.third)
    }



    /*
15724522
That's the right answer! You are one gold star closer to rescuing Santa.

You have completed Day 5! You can [Share] this victory or [Return to Your Advent Calendar].

    Opcode 5 is jump-if-true: if the first parameter is non-zero,
    it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
    (1005 or 5 means zero,  1105, 1205 etc means non zero ->
    so set pointer to value from second parameter ... index = parameter 2 ??? )


    Opcode 6 is jump-if-false: if the first parameter is zero,
    it sets the instruction pointer to the value from the second parameter. Otherwise, it does nothing.
    (1106, 1206 etc means non zero,     3006, 2006, 1006 or 6 means zero ->
    so set pointer to value from second parameter ... index = parameter 2 ??? )

    Opcode 7 is less than: if the first parameter is less than the second parameter,
    it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
    (52107, 43107, 33207 -> are less than - store 1 in position 3rd param)

    Opcode 8 is equals: if the first parameter is equal to the second parameter,
    it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
    (50008, 41108, 32208 -> are less than - store 1 in position 3rd param)

     */





}