package test.AdventOfCode

import main.AdventOfCode.calculateTotalFuel
import main.AdventOfCode.calculateTotalFuelInclusive
import main.AdventOfCode.moduleFuelCalc
import main.AdventOfCode.moduleFuelCalcTotal
import org.junit.Test
import kotlin.test.assertEquals

class Day1Test {

    val input = """
        129880
        115705
        118585
        124631
        81050
        138183
        61173
        95354
        130788
        89082
        75554
        110104
        140528
        71783
        125889
        126602
        73089
        76822
        51774
        85940
        81004
        149584
        145921
        105570
        142370
        80823
        147779
        115651
        70250
        67763
        128192
        51298
        134963
        73510
        90976
        141216
        65134
        140468
        143998
        101711
        88477
        53335
        138328
        141186
        149804
        64950
        53107
        54648
        97557
        85927
        125038
        80514
        64912
        140591
        114229
        57089
        123464
        127572
        137169
        146550
        51138
        115504
        128034
        147244
        108107
        101205
        51498
        136829
        140171
        59441
        144489
        139384
        145841
        96771
        116821
        88599
        126780
        65012
        67621
        129699
        149639
        97590
        147527
        117462
        146709
        60527
        107643
        92956
        72177
        92285
        62475
        63099
        66904
        77268
        62945
        134364
        106924
        117842
        130016
        123712
    """.trimIndent()

    @Test
    fun `moduleFuelCalc should calculate the fuel based on mass`() {

        assertEquals(2, moduleFuelCalc(12))
        assertEquals(2, moduleFuelCalc(14))
        assertEquals(654, moduleFuelCalc(1969))
        assertEquals(33583, moduleFuelCalc(100756))
    }

    @Test
    fun `calculateTotalFuel should sum the fuel for a string of inputs`() {

        val reducedInput = """
            12
            14
            1969
            100756
        """.trimIndent()
        val totalFuel = calculateTotalFuel(reducedInput)

        assertEquals((
                moduleFuelCalc(12) +
                moduleFuelCalc(14) +
                moduleFuelCalc(1969) +
                moduleFuelCalc(100756)
                ), totalFuel)
    }

    @Test
    fun `calculateTotalFuel should process full file`() {

        val totalFuelRaw = calculateTotalFuel(input)

        println("Total fuel raw = $totalFuelRaw")
        assertEquals(3478233, totalFuelRaw)

    }

    @Test
    fun `moduleFuelCalcTotal should calculate the fuel based on mass`() {

        assertEquals(2, moduleFuelCalcTotal(14))
        assertEquals(966, moduleFuelCalcTotal(1969))
        assertEquals(50346, moduleFuelCalcTotal(100756))
    }

    @Test
    fun `calculateTotalFuelInclusive should process full file`() {

        val totalFuelRaw = calculateTotalFuel(input)
        val totalFuelInclusive = calculateTotalFuelInclusive(input)

        println("Total fuel raw = $totalFuelRaw")
        println("Total fuel inclusive = $totalFuelInclusive")
        println("Increase of = ${((totalFuelInclusive.toDouble()/totalFuelRaw.toDouble())*100)}% ")
        assertEquals(5214475, totalFuelInclusive)
    }




}
