package test.AdventOfCode

import main.AdventOfCode.*
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertEquals

class Day3Test {

    @Test
    fun `distanceBetweenTwoCoordinates should calculate Manhatten Distance between two pairs of coordinates`() {

        assertEquals(0, distanceBetweenTwoCoordinates(Pair(0, 0), Pair(0, 0)))
        assertEquals(0, distanceBetweenTwoCoordinates(Pair(1, 0), Pair(1, 0)))
        assertEquals(0, distanceBetweenTwoCoordinates(Pair(85, 66), Pair(85, 66)))
        assertEquals(1, distanceBetweenTwoCoordinates(Pair(1, 0), Pair(0, 0)))
        assertEquals(2, distanceBetweenTwoCoordinates(Pair(1, 0), Pair(0, -1)))
        assertEquals(302, distanceBetweenTwoCoordinates(Pair(85, 66), Pair(-85, -66)))
        assertEquals(156, distanceBetweenTwoCoordinates(Pair(0, 0), Pair(145, 11)))
    }

    @Test
    fun `decodeInstructionToVector should convert to vector`() {

        assertEquals(Pair(1, 1), decodeInstructionToVector("R1,U1"))
        assertEquals(Pair(1, 1), decodeInstructionToVector("R1,U1,X1"))
        assertEquals(Pair(145, 11), decodeInstructionToVector("R75,D30,R83,U83,L12,D49,R71,U7,L72"))

    }

    @Test
    fun `decodeInstructionToCoordinates should convert to vector`() {

        assertEquals(
            listOf(Pair(0, 0), Pair(1, 0), Pair(2, 0), Pair(3, 0), Pair(4, 0)),
            decodeInstructionToCoordinates("R1,R1,R1,R1", Pair(0, 0))
        )
        assertEquals(
            listOf(Pair(0, 0), Pair(0, 1), Pair(0, 2), Pair(0, 3), Pair(0, 4)),
            decodeInstructionToCoordinates("U4", Pair(0, 0))
        )
        assertEquals(
            listOf(Pair(6, 0), Pair(5, 0), Pair(4, 0), Pair(3, 0), Pair(2, 0)),
            decodeInstructionToCoordinates("L4", Pair(6, 0))
        )
        assertEquals(
            listOf(Pair(6, 2), Pair(6, 1), Pair(6, 0), Pair(6, -1)),
            decodeInstructionToCoordinates("D3", Pair(6, 2))
        )
    }

    @Test
    fun `findDuplicatePairs should return list of duplicates`() {

        val coords1 = listOf(Pair(0, 0), Pair(0, 1), Pair(0, 2), Pair(0, 3), Pair(0, 4))
        val coords2 = listOf(Pair(2, 0), Pair(1, 1), Pair(0, 1), Pair(0, 0), Pair(0, -1))
        val overlap = listOf(Pair(0, 0), Pair(0, 1))
        assertEquals(overlap, findDuplicatePairs(coords1, coords2))

    }

    @Test
    fun `findDistanceOfNearestDuplicatePair should return distances from coordinate list`() {

        val coords1 = listOf(Pair(0, 0), Pair(0, 1), Pair(0, 2), Pair(0, 3), Pair(0, 4))
        val coords2 = listOf(Pair(2, 0), Pair(1, 1), Pair(0, 1), Pair(0, 0), Pair(0, -1))
        val input = Pair(0, 0)
        val overlap = listOf(Pair(0, 0), Pair(0, 1))
        assertEquals(0, findDistanceOfPair(overlap, input))

    }

    @Test
    fun `findDistanceToNearestDuplicatePair should return distances from coordinate list`() {

        val coords1 = listOf(Pair(0, 0), Pair(0, 1), Pair(0, 2), Pair(0, 3), Pair(0, 4))
        val coords2 = listOf(Pair(2, 0), Pair(1, 1), Pair(0, 1), Pair(0, 0), Pair(0, -1))
        val input = Pair(0, 0)
        val input2 = Pair(10, 10)

        assertEquals(1, findDistanceToDuplicatePairs(coords1, coords2, input))
        assertEquals(19, findDistanceToDuplicatePairs(coords1, coords2, input2))
    }

    @Test
    fun `findDistanceToNearestDuplicatePair with provided inputs`() {

        val input = Pair(0, 0)
        val instructions1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72"
        val instructions2 = "U62,R66,U55,R34,D71,R55,D58,R83"
        val answer1 = 159

        val instructions3 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
        val instructions4 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
        val answer2 = 135

        assertEquals(answer1,
            findDistanceToDuplicatePairs(
                decodeInstructionToCoordinates(instructions1,input),
                decodeInstructionToCoordinates(instructions2,input),
                input))

        assertEquals(answer2,
            findDistanceToDuplicatePairs(
                decodeInstructionToCoordinates(instructions3,input),
                decodeInstructionToCoordinates(instructions4,input),
                input))

    }

    @Test
    @Ignore
    fun `findDistanceToNearestDuplicatePair to find answer for Santa`() {

        val input = Pair(0, 0)
        val finalWireA =
            "R990,D362,L316,U101,R352,U798,L314,D564,R961,D634,L203,U601,R973,U227,R996,D639,L868,D135,L977,D201,R911,D486,R906,U719,L546,U324,R302,D200,L879,D206,L872,U681,R628,D272,R511,D827,L929,U915,L399,U696,R412,D640,R234,U487,R789,U354,L620,D914,L7,D228,L55,D591,L250,D228,R816,U935,R553,U98,L833,D418,R582,D793,R804,U283,R859,D206,L842,U663,L935,U495,L995,D181,R75,D33,R126,U489,L894,D675,R33,U239,L623,D931,L830,U63,R77,D576,L85,D415,R443,U603,R654,U495,L273,U583,R10,D648,L840,U904,R489,D655,R997,U559,L614,U917,R809,U540,L41,U519,R256,U111,R29,D603,L931,U518,R443,D51,L788,U483,L665,U890,L392,D701,R907,D125,L438,D107,L266,U766,R743,D343,R898,U293,L318,U417,L23,U44,L668,U614,R83,U31,R452,U823,R16,D418,R68,U823,L53,D638,L394,D714,R992,U196,R913,D526,L458,U428,L412,U901,R610,U348,L904,D815,R274,U439,R207,D81,L20,D507,L179,U249,L221,U603,L897,U490,R127,U99,L709,U925,L818,D777,R292,U935,R801,U331,R412,U759,L698,D53,L969,U492,L502,D137,R513,D999,L808,D618,L240,U378,L284,D726,L609,U530,R537,D36,L504,D26,R244,D692,L186,U767,L690,U182,R559,D926,R706,D132,L325,D846,R494,U238,L519,U655,R57,U658,L471,D717,L964,D346,L448,U286,L457,D504,R614,U652,R583,D780,R882,U417,R573,D297,L144,U347,L254,D589,L387,U309,L88,D510,R435,U636,L640,U801,R774,U678,R247,D846,L775,U527,L225,U798,R577,U897,R11,U153,L297,D748,L284,U806,R512,U906,L181,U39,R264,D47,L561,D441,L181,U210,L278,U998,R256,D278,R350,U466,L335,D310,L4,U298,L531,D423,R851,U285,L235,D139,R209,U882,R801,D36,L777,D153,L63"
        val finalWireB =
            "L995,D598,R577,U346,L797,D375,R621,D709,R781,U55,R965,U327,L479,U148,L334,U93,R644,U632,L557,D136,L690,D548,R982,D703,L971,U399,R600,D785,L504,U984,R18,U190,L755,D737,L787,D921,R303,U513,L544,U954,L814,U239,R550,D458,R518,D538,R362,D350,L103,U17,L362,D480,L80,U639,L361,D75,L356,D849,R635,U633,R934,U351,L314,U960,R657,U802,L687,U385,L558,D984,L996,U765,L147,D366,R908,U981,R44,U336,R396,U85,R819,D582,L21,D920,L627,D103,R922,U195,L412,D385,L159,U446,L152,U400,L303,U549,R734,D709,R661,U430,R177,U857,L53,U555,R35,D919,L163,D630,L162,U259,R46,D89,R965,D410,R37,U39,R621,D606,L816,D659,L668,D418,L775,D911,R296,U488,L129,U869,L455,U663,L942,U813,L274,D677,R161,D338,R455,D580,R976,D984,L336,U742,R334,U130,L210,U523,R958,U177,R126,U469,L513,D14,L772,D423,L369,D661,R167,D449,L685,U871,L930,U630,L54,D581,L921,U839,R782,D844,L581,D995,R110,U365,L594,D595,R391,D298,R297,U469,L148,D34,R5,D609,L654,U172,R940,D858,L682,D92,R395,D683,R947,U311,L850,U151,R452,U641,L599,D640,R86,U584,L518,D597,L724,D282,L691,D957,L119,U30,L8,D514,R237,U599,R775,U413,R802,D132,R925,U133,L980,D981,R272,U632,R995,U427,R770,D722,L817,D609,R590,D799,L699,U923,L881,U893,R79,U327,L405,D669,L702,D612,R895,D132,R420,U958,L955,U993,L817,D492,R453,D342,L575,D253,R97,U54,R456,U748,L912,U661,L987,D182,L816,U218,R933,D797,L207,D71,R546,U578,L894,D536,L253,D525,L164,D673,R784,U915,L774,U586,R733,D80,L510,U449,L403,D915,L612,D325,L470,U179,L460,U405,R297,D803,R637,U893,R565,U952,R550,U936,R378,D932,L669"
        val answer1 = 280

        val coordsA= decodeInstructionToCoordinates(finalWireA,input).also(::println)
        val coordsB= decodeInstructionToCoordinates(finalWireB,input).also(::println)

        assertEquals(answer1,
            findDistanceToDuplicatePairs(coordsA,coordsB,input))

    }

    @Test
//    @Ignore
    fun `findDistanceToFirstDuplicatePairs to find fastest answer for Santa`() {

        val input = Pair(0, 0)
        val finalWireA =
            "R990,D362,L316,U101,R352,U798,L314,D564,R961,D634,L203,U601,R973,U227,R996,D639,L868,D135,L977,D201,R911,D486,R906,U719,L546,U324,R302,D200,L879,D206,L872,U681,R628,D272,R511,D827,L929,U915,L399,U696,R412,D640,R234,U487,R789,U354,L620,D914,L7,D228,L55,D591,L250,D228,R816,U935,R553,U98,L833,D418,R582,D793,R804,U283,R859,D206,L842,U663,L935,U495,L995,D181,R75,D33,R126,U489,L894,D675,R33,U239,L623,D931,L830,U63,R77,D576,L85,D415,R443,U603,R654,U495,L273,U583,R10,D648,L840,U904,R489,D655,R997,U559,L614,U917,R809,U540,L41,U519,R256,U111,R29,D603,L931,U518,R443,D51,L788,U483,L665,U890,L392,D701,R907,D125,L438,D107,L266,U766,R743,D343,R898,U293,L318,U417,L23,U44,L668,U614,R83,U31,R452,U823,R16,D418,R68,U823,L53,D638,L394,D714,R992,U196,R913,D526,L458,U428,L412,U901,R610,U348,L904,D815,R274,U439,R207,D81,L20,D507,L179,U249,L221,U603,L897,U490,R127,U99,L709,U925,L818,D777,R292,U935,R801,U331,R412,U759,L698,D53,L969,U492,L502,D137,R513,D999,L808,D618,L240,U378,L284,D726,L609,U530,R537,D36,L504,D26,R244,D692,L186,U767,L690,U182,R559,D926,R706,D132,L325,D846,R494,U238,L519,U655,R57,U658,L471,D717,L964,D346,L448,U286,L457,D504,R614,U652,R583,D780,R882,U417,R573,D297,L144,U347,L254,D589,L387,U309,L88,D510,R435,U636,L640,U801,R774,U678,R247,D846,L775,U527,L225,U798,R577,U897,R11,U153,L297,D748,L284,U806,R512,U906,L181,U39,R264,D47,L561,D441,L181,U210,L278,U998,R256,D278,R350,U466,L335,D310,L4,U298,L531,D423,R851,U285,L235,D139,R209,U882,R801,D36,L777,D153,L63"
        val finalWireB =
            "L995,D598,R577,U346,L797,D375,R621,D709,R781,U55,R965,U327,L479,U148,L334,U93,R644,U632,L557,D136,L690,D548,R982,D703,L971,U399,R600,D785,L504,U984,R18,U190,L755,D737,L787,D921,R303,U513,L544,U954,L814,U239,R550,D458,R518,D538,R362,D350,L103,U17,L362,D480,L80,U639,L361,D75,L356,D849,R635,U633,R934,U351,L314,U960,R657,U802,L687,U385,L558,D984,L996,U765,L147,D366,R908,U981,R44,U336,R396,U85,R819,D582,L21,D920,L627,D103,R922,U195,L412,D385,L159,U446,L152,U400,L303,U549,R734,D709,R661,U430,R177,U857,L53,U555,R35,D919,L163,D630,L162,U259,R46,D89,R965,D410,R37,U39,R621,D606,L816,D659,L668,D418,L775,D911,R296,U488,L129,U869,L455,U663,L942,U813,L274,D677,R161,D338,R455,D580,R976,D984,L336,U742,R334,U130,L210,U523,R958,U177,R126,U469,L513,D14,L772,D423,L369,D661,R167,D449,L685,U871,L930,U630,L54,D581,L921,U839,R782,D844,L581,D995,R110,U365,L594,D595,R391,D298,R297,U469,L148,D34,R5,D609,L654,U172,R940,D858,L682,D92,R395,D683,R947,U311,L850,U151,R452,U641,L599,D640,R86,U584,L518,D597,L724,D282,L691,D957,L119,U30,L8,D514,R237,U599,R775,U413,R802,D132,R925,U133,L980,D981,R272,U632,R995,U427,R770,D722,L817,D609,R590,D799,L699,U923,L881,U893,R79,U327,L405,D669,L702,D612,R895,D132,R420,U958,L955,U993,L817,D492,R453,D342,L575,D253,R97,U54,R456,U748,L912,U661,L987,D182,L816,U218,R933,D797,L207,D71,R546,U578,L894,D536,L253,D525,L164,D673,R784,U915,L774,U586,R733,D80,L510,U449,L403,D915,L612,D325,L470,U179,L460,U405,R297,D803,R637,U893,R565,U952,R550,U936,R378,D932,L669"
        val answer1 = 280

        val finalWireC =
            "R990,D362,L316,U101,R352,U798,L314,D564,R961,D634,L203,U601,R973,U227"
        val finalWireD =
            "L995,D598,R577,U346,L797,D375,R621,D709,R781,U55,R965,U327,L479,U148"

        val finalWireE =
            "R75,D30,R83,U83,L12,D49,R71,U7,L72"
        val finalWireF =
            "U62,R66,U55,R34,D71,R55,D58,R83"
        val answerEF = 610

        val finalWireG =
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
        val finalWireH =
            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
        val answerGH = 410


        val coordsE= decodeInstructionToCoordinates(finalWireE,input).also(::println)
        val coordsF= decodeInstructionToCoordinates(finalWireF,input).also(::println)

        val XXX = decodeInstruction(finalWireE,finalWireF,input).also(::println)

        val Day3EF = Day3(listOf(finalWireE,finalWireF)).also(::println)
        val Day3GH = Day3(listOf(finalWireG,finalWireH)).also(::println)
        val Day3AB = Day3(listOf(finalWireA,finalWireB)).also(::println)
        println("EF")
        println(Day3EF.part1().toString())
        println(Day3EF.part2().toString())
        println("GH")
        println(Day3GH.part1().toString())
        println(Day3GH.part2().toString())

        println("AB")
        println(Day3AB.part1().toString())
        println(Day3AB.part2().toString())

//        val overlapCoords = overlap(coordsE,coordsF)

//        assertEquals(answerEF,
//            findDistanceToFirstDuplicatePairs(coordsE,coordsF,input)
//        )

//        val coordsC= decodeInstructionToCoordinates(finalWireC,input).also(::println)
//        val coordsD= decodeInstructionToCoordinates(finalWireD,input).also(::println)
//
//        val overlapCoordsCD = findDuplicatePairs(coordsC,coordsD).also(::println)

//        assertEquals(answerEF,
//            findDistanceToFirstDuplicatePairs(coordsE,coordsF,input)
//        )

//        val coordsA= decodeInstructionToCoordinates(finalWireA,input).also(::println)
//        val coordsB= decodeInstructionToCoordinates(finalWireB,input).also(::println)

        println("HERE")
//        val overlapCoordsAB = findDuplicatePairs(coordsA,coordsB).also(::println)

//        assertEquals(answerEF,
//            findDistanceToFirstDuplicatePairs(coordsE,coordsF,input)
//        )




    }




/*
...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........
 */



}
/*
--- Day 3: Crossed Wires ---
The gravity assist was successful, and you're well on your way to the Venus refuelling station.
During the rush back on Earth, the fuel management system wasn't completely installed,
so that's next on the priority list.

Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a
central port and extend outward on a grid. You trace the path each wire takes as it leaves
the central port, one wire per line of text (your puzzle input).

The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you
need to find the intersection point closest to the central port. Because the wires are on a grid,
use the Manhattan distance for this measurement. While the wires do technically cross right at
the central port where they both start, this point does not count, nor does a wire count as crossing
with itself.

For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o),
it goes right 8, up 5, left 5, and finally down 3:

...........
...........
...........
....+----+.
....|....|.
....|....|.
....|....|.
.........|.
.o-------+.
...........
Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.

Here are a few more examples:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135
What is the Manhattan distance from the central port to the closest intersection?

To begin, get your puzzle input.

Answer: 280

It turns out that this circuit is very timing-sensitive; you actually need to minimize the signal delay.

To do this, calculate the number of steps each wire takes to reach each intersection; choose the intersection where
the sum of both wires' steps is lowest. If a wire visits a position on the grid multiple times, use the steps value
from the first time it visits that position when calculating the total value of a specific intersection.

The number of steps a wire takes is the total number of grid squares the wire has entered to get to that location,
including the intersection being considered. Again consider the example from above:

...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
In the above example, the intersection closest to the central port is reached after 8+5+5+2 = 20 steps by the first wire and 7+6+4+3 = 20 steps by the second wire for a total of 20+20 = 40 steps.

However, the top-right intersection is better: the first wire takes only 8+5+2 = 15 and the second wire takes only 7+6+2 = 15, a total of 15+15 = 30 steps.

Here are the best steps for the extra examples from above:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps
What is the fewest combined steps the wires must take to reach an intersection?

Although it hasn't changed, you can still get your puzzle input.
Answer = 10554
 */