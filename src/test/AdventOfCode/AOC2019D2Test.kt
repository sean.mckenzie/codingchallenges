package test.AdventOfCode

import main.AdventOfCode.*
import org.junit.Test
import kotlin.test.assertEquals

class Day2Test {

    @Test
    fun `intCodeAction can recognise the OpCodes at an index in a list of Integers`() {
        assertEquals("add", intCodeAction(arrayListOf(1, 2, 3, 4), 0))
        assertEquals("multiply", intCodeAction(arrayListOf(1, 2, 3, 4, 2, 2, 3, 4), 4))
        assertEquals("end", intCodeAction(arrayListOf(1, 2, 3, 4, 2, 2, 3, 4, 99, 2, 3, 4), 8))
        assertEquals("fail", intCodeAction(arrayListOf(1, 2, 3, 4, 2, 2, 3, 4, 99, 2, 3, 4, 45, 2, 3, 4), 12))
    }

    @Test
    fun `addResult can action OpCode 1 at a given index from a list of Integers`() {
        val input1 = arrayListOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)

        assertEquals(70, addResult(input1, 0))
        assertEquals(43, addResult(input1, 1))
        assertEquals(13, addResult(input1, 2))
    }

    @Test
    fun `multiplyResult can action OpCode 1 at a given index from a list of Integers`() {
        val input1 = arrayListOf(1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50)

        assertEquals(1200, multiplyResult(input1, 0))
        assertEquals(120, multiplyResult(input1, 1))
        assertEquals(30, multiplyResult(input1, 2))
        assertEquals(30, multiplyResult(input1, 3))
        assertEquals(150, multiplyResult(input1, 4))
    }

    @Test
    fun `processIntCode can action OpCode 1 at a given index from a list of Integers`() {

        val input1 = arrayListOf(1,9,10,3,1,3,11,0,99,30,40,50)
        val output1 = arrayListOf(1,9,10,70,1,3,11,0,99,30,40,50)
        val output2 = arrayListOf(120,9,10,70,1,3,11,0,99,30,40,50)

        assertEquals(Pair(output1,70), processIntCode(input1,0))
        assertEquals(Pair(output2,120), processIntCode(output1,4))
        assertEquals(Pair(arrayListOf(2,0,0,0,99),2), processIntCode(arrayListOf(1,0,0,0,99),0))
    }

    @Test
    fun `processIntCode can action OpCode 2 at a given index from a list of Integers`() {

        val input1 = arrayListOf(2,9,10,3,2,3,11,0,99,30,40,50)
        val output1 = arrayListOf(2,9,10,1200,2,3,11,0,99,30,40,50)
        val output2 = arrayListOf(60000,9,10,1200,2,3,11,0,99,30,40,50)

        assertEquals(Pair(output1,1200), processIntCode(input1,0))
        assertEquals(Pair(output2,60000), processIntCode(output1,4))
        assertEquals(Pair(arrayListOf(2,3,0,6,99),6), processIntCode(arrayListOf(2,3,0,3,99),0))
        assertEquals(Pair(arrayListOf(2,4,4,5,99,9801),9801), processIntCode(arrayListOf(2,4,4,5,99,0),0))
    }

    @Test
    fun `processIntCode can action OpCode 3 at a given index from a list of Integers`() {
        val input1 = arrayListOf(2,9,10,3,2,3,11,0,99,30,40,50)

        assertEquals(Pair(input1, 99), processIntCode(input1, 8))
    }

    @Test
    fun `processIntCode can action OpCode 4 and return same input with failure code 0`() {
        val input1 = arrayListOf(2,9,10,3,2,3,11,0,99,30,40,50)

        assertEquals(Pair(input1, null), processIntCode(input1, 2))
    }

    @Test
    fun `intCode can correctly process for list of Integers`() {
        val input1 = arrayListOf(1,9,10,3,2,3,11,0,99,30,40,50)
        val output1 = arrayListOf(3500,9,10,70,2,3,11,0,99,30,40,50)

        assertEquals(output1, intCode(input1))
        assertEquals(arrayListOf(30,1,1,4,2,5,6,0,99), intCode(arrayListOf(1,1,1,4,99,5,6,0,99)))
    }

    @Test
    fun `intCode can correctly process full file`() {

        val fullInput = arrayListOf(1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,2,6,19,23,1,23,5,27,1,27,13,31,2,6,31,35,1,5,35,39,1,39,10,43,2,6,43,47,1,47,5,51,1,51,9,55,2,55,6,59,1,59,10,63,2,63,9,67,1,67,5,71,1,71,5,75,2,75,6,79,1,5,79,83,1,10,83,87,2,13,87,91,1,10,91,95,2,13,95,99,1,99,9,103,1,5,103,107,1,107,10,111,1,111,5,115,1,115,6,119,1,119,10,123,1,123,10,127,2,127,13,131,1,13,131,135,1,135,10,139,2,139,6,143,1,143,9,147,2,147,6,151,1,5,151,155,1,9,155,159,2,159,6,163,1,163,2,167,1,10,167,0,99,2,14,0,0)
        val fullOutputSuccess = intCode(fullInput)

        println("Full Output Success = $fullOutputSuccess")

        val fullInputBroken = arrayListOf(1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,2,6,19,23,1,23,5,27,1,27,13,31,2,6,31,35,1,5,35,39,1,39,10,43,2,6,43,47,1,47,5,51,1,51,9,55,2,55,6,59,1,59,10,63,2,63,9,67,1,67,5,71,1,71,5,75,2,75,6,79,1,5,79,83,1,10,83,87,2,13,87,91,1,10,91,95,2,13,95,99,1,99,9,103,1,5,103,107,1,107,10,111,1,111,5,115,1,115,6,119,1,119,10,123,1,123,10,127,2,127,13,131,1,13,131,135,1,135,10,139,2,139,6,143,1,143,9,147,2,147,6,151,1,5,151,155,1,9,155,159,2,159,6,163,1,163,2,167,1,10,167,0,99,2,14,0,0)
        val fullOutputBroken = intCode(fullInputBroken)

        println("Full Output Broken  = $fullOutputBroken")
    }

    @Test
    fun `updateInput correctly adds noun & verb to input`() {
        val input1 = arrayListOf(1,0,0,3,1,1,2,3,1,3,4,3,1)
        val noun1 = 5
        val verb1 = 9
        val output1 = arrayListOf(1,5,9,3,1,1,2,3,1,3,4,3,1)

        val input2 = arrayListOf(3500,9,10,70,2,3,11,0,99,30,40,50)
        val noun2 = 99
        val verb2 = 99
        val output2 = arrayListOf(3500,99,99,70,2,3,11,0,99,30,40,50)

        assertEquals(output1, updateInput(input1, noun1, verb1))
        assertEquals(output2, updateInput(input2, noun2, verb2))
    }

    @Test
    fun `findCorrectNounAndVerb correctly finds result`() {
        val input1 = arrayListOf(33,0,0,3,1,1,2,3,1,3,4,3,1)
        assertEquals(33, input1.first())

        val fullInput = arrayListOf(1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,2,6,19,23,1,23,5,27,1,27,13,31,2,6,31,35,1,5,35,39,1,39,10,43,2,6,43,47,1,47,5,51,1,51,9,55,2,55,6,59,1,59,10,63,2,63,9,67,1,67,5,71,1,71,5,75,2,75,6,79,1,5,79,83,1,10,83,87,2,13,87,91,1,10,91,95,2,13,95,99,1,99,9,103,1,5,103,107,1,107,10,111,1,111,5,115,1,115,6,119,1,119,10,123,1,123,10,127,2,127,13,131,1,13,131,135,1,135,10,139,2,139,6,143,1,143,9,147,2,147,6,151,1,5,151,155,1,9,155,159,2,159,6,163,1,163,2,167,1,10,167,0,99,2,14,0,0)
        val correctValue = 19690720
        val fullOutput = findCorrectNounAndVerb(fullInput, correctValue)
        println(fullOutput)

        assertEquals(correctValue, fullOutput.first())
    }
}
