package test

import main.*
import org.junit.Test
import kotlin.test.assertEquals

internal class PokerKtTest {

    @Test
    fun `findWinners returns winner with straight flush`() {
        val player1 = Player("Sean", listOf("7H", "8H"), 10)
        val player2 = Player("Amy", listOf("QH", "AH"), 10)
        val houseHand = listOf("4H", "5H", "6H", "AS", "QD")

        val winner = findWinners(listOf(player1, player2), houseHand)
        assertEquals(winner, player1)
    }

    @Test
    fun `findWinners returns winner with flush`() {
        val player1 = Player("Sean", listOf("2H", "TD"), 10)
        val player2 = Player("Amy", listOf("KH", "AH"), 10)
        val houseHand = listOf("4H", "5H", "6H", "AS", "QD")

        val winner = findWinners(listOf(player1, player2), houseHand)
        assertEquals(winner, player2)
    }

    @Test
    fun `findWinners returns player with straight flush over player with flush`() {
        val player1 = Player("Amy", listOf("KH", "AH"), 10)
        val player2 = Player("Sean", listOf("7H", "8H"), 10)
        val houseHand = listOf("4H", "5H", "6H", "AS", "QD")

        val winner = findWinners(listOf(player1, player2), houseHand)
        assertEquals(winner, player2)
    }

    @Test
    fun `findWinners needs to handle two or more with same hand`() {
        val houseHand = listOf("4H", "5H", "6H", "AS", "QD")
        val player1 = Player("StraightFlushHigh", listOf("7H", "8H"), 10)
        val player2 = Player("StraightFlushLow", listOf("2H", "3H"), 10)

        val winnerStraight = findWinners(listOf(player1, player2), houseHand)
        assertEquals(winnerStraight, null)

        val player3 = Player("FlushHigh", listOf("KH", "AH"), 10)
        val player4 = Player("FlushLow", listOf("7H", "TH"), 10)

        val winnerFlush = findWinners(listOf(player3, player4), houseHand)
        assertEquals(winnerFlush, null)
    }

    @Test
    fun `isStraightFlush returns true when there is a straight flush`() {
        val cards = listOf("4H", "5H", "6H", "7H", "8H", "AS", "QD")
        assertEquals(isStraightFlush(cards), true)
        assertEquals(isStraightFlush(cards.shuffled()), true)

        val pictureCards = listOf("4H", "5H", "TD", "JD", "QD", "KD", "AD")
        assertEquals(isStraightFlush(pictureCards), true)
        assertEquals(isStraightFlush(pictureCards.shuffled()), true)
    }

    @Test
    fun `isStraightFlush returns false where there is not a straight flush`() {
        val cards = listOf("2H", "5H", "6H", "7H", "8H", "AS", "QD")
        assertEquals(isStraightFlush(cards), false)

        val allHearts = listOf("2H", "4H", "6H", "8H", "TH", "QH", "AH")
        assertEquals(isStraightFlush(allHearts), false)
    }

    @Test
    fun `allCardsInSameSuit returns all the cards in the same suit when there are five cards in the same suit`() {
        val cardsHearts = listOf("4H", "5H", "6H", "7H", "8H", "AS", "QD")
        assertEquals(allCardsInSameSuit(cardsHearts), listOf("4H", "5H", "6H", "7H", "8H"))

        val cardsDiamonds = listOf("4D", "5D", "6D", "AS", "QD", "7D", "8D")
        assertEquals(allCardsInSameSuit(cardsDiamonds), listOf("4D", "5D", "6D", "QD", "7D", "8D"))
    }

    @Test
    fun `allCardsInSameSuit returns empty hand if there are not five cards in same suit`() {
        val cardsHearts = listOf("4H", "5H", "6H", "7H", "8D", "AS", "QD")
        assertEquals(allCardsInSameSuit(cardsHearts), emptyList())

        val cardsDiamonds = listOf("4D", "5D", "6D", "7D", "8S", "AS", "QS")
        assertEquals(allCardsInSameSuit(cardsDiamonds), emptyList())
    }

    @Test
    fun `valueOfPokerCard returns numeric value of the card`() {
        assertEquals(valueOfPokerCard("2H"), 2)
        assertEquals(valueOfPokerCard("7H"), 7)
        assertEquals(valueOfPokerCard("TD"), 10)
        assertEquals(valueOfPokerCard("JS"), 11)
        assertEquals(valueOfPokerCard("QC"), 12)
        assertEquals(valueOfPokerCard("KX"), 13)
        assertEquals(valueOfPokerCard("AS"), 14)
    }

    @Test
    fun `isConsecutiveNumbers returns highest hand of consecutive numbers`() {
        val cardsHearts = listOf("4H", "5H", "6H", "7H", "8H")
        assertEquals(isConsecutiveNumbers(cardsHearts), listOf("8H", "7H", "6H", "5H", "4H"))

        val cardsDiamonds = listOf("4D", "5D", "6D", "7D", "8D", "9D")
        assertEquals(isConsecutiveNumbers(cardsDiamonds), listOf("9D", "8D", "7D", "6D", "5D"))

        val pictureCards = listOf("TD", "JD", "QD", "KD", "AD")
        assertEquals(isConsecutiveNumbers(pictureCards), listOf("AD", "KD", "QD", "JD", "TD"))
    }

    @Test
    fun `isFlush returns true if five or more cards are same suit`() {
        val cardsHearts = listOf("4H", "5H", "6H", "7H", "8H", "AS", "QD")
        assertEquals(isFlush(cardsHearts), true)

        val cardsDiamonds = listOf("4D", "5D", "6D", "AS", "QD", "7D", "8D")
        assertEquals(isFlush(cardsDiamonds), true)
    }

    @Test
    fun `ifFlush returns false if there is not five or more cards in same suit`() {
        val cardsHearts = listOf("4H", "5H", "6H", "7H", "8D", "AS", "QD")
        assertEquals(isFlush(cardsHearts), false)

        val cardsDiamonds = listOf("4D", "5D", "6D", "7D", "8S", "AS", "QS")
        assertEquals(isFlush(cardsDiamonds), false)
    }

    @Test
    fun `isHighestFlush returns highest hand if five or more cards are same suit`() {
        val pictureCards = listOf("AD", "9D", "TD", "JD", "4D", "KD", "6D")
        assertEquals(isHighestFlush(pictureCards.shuffled()), listOf("AD", "KD", "JD", "TD", "9D"))

        val noFlush = listOf("AD", "9H", "TS", "JC", "4D", "KC", "6D")
        assertEquals(isHighestFlush(noFlush.shuffled()), emptyList())

    }

}