package test

import main.*
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class Sudoku {

    val easySudokuStart = listOf(
        0, 2, 1, 0, 0, 9, 0, 7, 0,
        8, 6, 0, 0, 7, 0, 4, 3, 0,
        0, 3, 0, 0, 8, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 3, 0, 5,
        7, 1, 6, 0, 0, 0, 0, 0, 9,
        0, 0, 3, 0, 2, 1, 6, 0, 0,
        0, 0, 0, 3, 5, 8, 0, 9, 4,
        1, 9, 0, 0, 6, 7, 0, 8, 0,
        3, 0, 8, 0, 0, 2, 7, 0, 6
    )

    val easySudokuEnd = listOf(
        4, 2, 1, 6, 3, 9, 5, 7, 8,
        8, 6, 9, 1, 7, 5, 4, 3, 2,
        5, 3, 7, 2, 8, 4, 9, 6, 1,
        2, 8, 4, 7, 9, 6, 3, 1, 5,
        7, 1, 6, 5, 4, 3, 8, 2, 9,
        9, 5, 3, 8, 2, 1, 6, 4, 7,
        6, 7, 2, 3, 5, 8, 1, 9, 4,
        1, 9, 5, 4, 6, 7, 2, 8, 3,
        3, 4, 8, 9, 1, 2, 7, 5, 6
    )

    val simpleSudokuStart = listOf(
        7, 0, 9, 0, 0, 2, 6, 8, 0,
        0, 0, 2, 0, 5, 0, 7, 0, 4,
        0, 0, 0, 0, 0, 0, 2, 0, 0,
        1, 9, 0, 0, 0, 7, 0, 6, 0,
        8, 6, 7, 1, 9, 5, 0, 4, 0,
        5, 0, 4, 0, 0, 0, 0, 9, 0,
        4, 3, 5, 7, 8, 0, 0, 2, 0,
        0, 0, 6, 4, 0, 0, 0, 0, 1,
        9, 8, 0, 5, 0, 6, 0, 0, 3
    )

    val unsolveableSudoku = listOf(
        7, 8, 1, 5, 4, 3, 9, 2, 6,
        0, 0, 6, 1, 7, 9, 5, 0, 0,
        9, 5, 4, 6, 2, 8, 7, 3, 1,
        6, 9, 5, 8, 3, 7, 2, 1, 4,
        1, 4, 8, 2, 6, 5, 3, 7, 9,
        3, 2, 7, 9, 1, 4, 8, 0, 0,
        4, 1, 3, 7, 5, 2, 6, 9, 8,
        0, 0, 2, 0, 0, 0, 4, 0, 0,
        5, 7, 9, 4, 8, 6, 1, 0, 3
    )

    val hardSudokuStart = listOf(
        0, 0, 0, 0, 7, 4, 3, 1, 6,
        0, 0, 0, 6, 0, 3, 8, 4, 0,
        0, 0, 0, 0, 0, 8, 5, 0, 0,
        7, 2, 5, 8, 0, 0, 0, 3, 4,
        0, 0, 0, 0, 3, 0, 0, 5, 0,
        0, 0, 0, 0, 0, 2, 7, 9, 8,
        0, 0, 8, 9, 4, 0, 0, 0, 0,
        0, 4, 0, 0, 8, 5, 9, 0, 0,
        9, 7, 1, 3, 2, 6, 4, 8, 5
    )

    val hardSudokuEnd = listOf(
        5, 8, 9, 2, 7, 4, 3, 1, 6,
        2, 1, 7, 6, 5, 3, 8, 4, 9,
        4, 6, 3, 1, 9, 8, 5, 2, 7,
        7, 2, 5, 8, 1, 9, 6, 3, 4,
        8, 9, 6, 4, 3, 7, 1, 5, 2,
        1, 3, 4, 5, 6, 2, 7, 9, 8,
        6, 5, 8, 9, 4, 1, 2, 7, 3,
        3, 4, 2, 7, 8, 5, 9, 6, 1,
        9, 7, 1, 3, 2, 6, 4, 8, 5
    )

    val multipleSolutionSoduku = listOf(
        0, 8, 0, 0, 0, 9, 7, 4, 3,
        0, 5, 0, 0, 0, 8, 0, 1, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0,
        8, 0, 0, 0, 0, 5, 0, 0, 0,
        0, 0, 0, 8, 0, 4, 0, 0, 0,
        0, 0, 0, 3, 0, 0, 0, 0, 6,
        0, 0, 0, 0, 0, 0, 0, 7, 0,
        0, 3, 0, 5, 0, 0, 0, 8, 0,
        9, 7, 2, 4, 0, 0, 0, 5, 0
    )

    @Test
    fun `convertListToSudokuBoard returns Rows in chunks of 9`() {
        assertEquals(listOf(0, 2, 1, 0, 0, 9, 0, 7, 0), convertListToSudokuBoard(easySudokuStart)[0])
        assertEquals(listOf(3, 0, 8, 0, 0, 2, 7, 0, 6), convertListToSudokuBoard(easySudokuStart)[8])
    }

    @Test
    fun `countNumberOfZeroes returns count for 0s in a row`() {
        assertEquals(0, countNumberOfZeroes(listOf()))
        assertEquals(0, countNumberOfZeroes(listOf(9)))
        assertEquals(0, countNumberOfZeroes(listOf(9, 9, 9, 9, 9, 9, 9, 9, 9)))
        assertEquals(1, countNumberOfZeroes(listOf(0)))
        assertEquals(1, countNumberOfZeroes(listOf(0, 9, 9, 9, 9, 9, 9, 9, 9, 9)))
        assertEquals(2, countNumberOfZeroes(listOf(0, 9, 0, 9, 9, 9, 9, 9, 9, 9)))
        assertEquals(9, countNumberOfZeroes(listOf(0, 0, 0, 0, 0, 0, 0, 0, 0)))
    }


    @Test
    fun `processSudokuBoard should return an empty list for an unsolveable board`() {
        assertEquals(listOf(), processSudokuBoard(unsolveableSudoku))
    }


    @Test
    fun `returnIndexOfZero returns Index for specific count of 0s`() {
        assertEquals(0, returnIndexOfZero(listOf(0, 8, 0, 0, 7, 0, 0, 1, 3), 1))
        assertEquals(2, returnIndexOfZero(listOf(9, 8, 0, 0, 7, 0, 0, 1, 3), 1))
        assertEquals(3, returnIndexOfZero(listOf(9, 8, 0, 0, 7, 0, 0, 1, 3), 2))
        assertEquals(5, returnIndexOfZero(listOf(9, 8, 0, 0, 7, 0, 0, 1, 3), 3))
        assertEquals(6, returnIndexOfZero(listOf(9, 8, 0, 0, 7, 0, 0, 1, 3), 4))
        assertEquals(-1, returnIndexOfZero(listOf(9, 8, 0, 0, 7, 0, 0, 1, 3), 5))
        assertEquals(-1, returnIndexOfZero(listOf(9, 9, 9, 9, 9, 9, 9, 9, 9), 1))
    }

    @Test
    fun `returnColumn returns Column for an Index`() {
        assertEquals(listOf(0, 8, 0, 0, 7, 0, 0, 1, 3), returnColumn(easySudokuStart, 0))
        assertEquals(listOf(0, 0, 1, 5, 9, 0, 4, 0, 6), returnColumn(easySudokuStart, 8))
    }

    @Test
    fun `gridStartIndex returns starting index for a row or column`() {
        assertEquals(0, gridStartIndex(0))
        assertEquals(0, gridStartIndex(1))
        assertEquals(0, gridStartIndex(2))
        assertEquals(3, gridStartIndex(3))
        assertEquals(3, gridStartIndex(4))
        assertEquals(3, gridStartIndex(5))
        assertEquals(6, gridStartIndex(6))
        assertEquals(6, gridStartIndex(7))
        assertEquals(6, gridStartIndex(8))
    }

    @Test
    fun `returnNonet returns Grid for a Given Row and Column`() {
        assertEquals(listOf(0, 2, 1, 8, 6, 0, 0, 3, 0), returnNonet(easySudokuStart, 0, 0))
        assertEquals(listOf(0, 2, 1, 8, 6, 0, 0, 3, 0), returnNonet(easySudokuStart, 2, 2))

        assertEquals(listOf(0, 0, 9, 0, 7, 0, 0, 8, 0), returnNonet(easySudokuStart, 0, 3))
        assertEquals(listOf(0, 0, 9, 0, 7, 0, 0, 8, 0), returnNonet(easySudokuStart, 2, 5))

        assertEquals(listOf(0, 7, 0, 4, 3, 0, 0, 0, 1), returnNonet(easySudokuStart, 0, 6))
        assertEquals(listOf(0, 7, 0, 4, 3, 0, 0, 0, 1), returnNonet(easySudokuStart, 2, 8))

        assertEquals(listOf(0, 0, 0, 7, 1, 6, 0, 0, 3), returnNonet(easySudokuStart, 3, 0))
        assertEquals(listOf(0, 0, 0, 7, 1, 6, 0, 0, 3), returnNonet(easySudokuStart, 5, 2))

        assertEquals(listOf(3, 5, 8, 0, 6, 7, 0, 0, 2), returnNonet(easySudokuStart, 6, 3))
        assertEquals(listOf(0, 9, 4, 0, 8, 0, 7, 0, 6), returnNonet(easySudokuStart, 6, 6))
    }

    @Test
    fun `notInListOneToNine returns list of core numbers not present in the list`() {
        assertEquals(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9), valuesAvailable(listOf()))
        assertEquals(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9), valuesAvailable(listOf(0)))
        assertEquals(listOf(2, 3, 4, 5, 6, 7, 8, 9), valuesAvailable(listOf(0, 1)))
        assertEquals(listOf(), valuesAvailable(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)))
        assertEquals(listOf(8, 9), valuesAvailable(listOf(0, 1, 2, 3, 4, 5, 6, 7)))
        assertEquals(listOf(9), valuesAvailable(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8)))
    }

    @Test
    fun `knownValuesForPosition for a row, column and grid`() {
        assertTrue { (listOf(0, 1, 2, 3, 6, 7, 8, 9).containsAll(valuesAlreadyTaken(easySudokuStart, 0, 0))) }
        assertTrue { (listOf(4, 5).containsAll(valuesAvailable(valuesAlreadyTaken(easySudokuStart, 0, 0)))) }

        assertTrue { (listOf(0, 3, 4, 6, 7, 8, 9).containsAll(valuesAlreadyTaken(easySudokuStart, 1, 3))) }
        assertTrue { (listOf(1, 2, 5).containsAll(valuesAvailable(valuesAlreadyTaken(easySudokuStart, 1, 3)))) }
    }


    @Test
    fun `processSudokuBoard should process for each zero in a file for an easy board`() {
        assertEquals(easySudokuEnd, processSudokuBoard(easySudokuStart))
    }

    @Test
    fun `processSudokuBoard should process for each zero in a file for an hard board`() {
        assertEquals(hardSudokuEnd, processSudokuBoard(hardSudokuStart))
    }


}

/*
In this challenge you wll need to solve Sudoku puzzles.
A Sudoku grid consists of 9 columns and 9 rows.
The grid is broken into nine regions, each with three columns and three rows.
Each position on a complete Sudoku grid must contain a number between 1 and 9
such that every column and every row on the grid contains every number between
1 and 9 and every region contains every number between 1 and 9.

A Sudokuk puzzle is a grid with numbers removed and to solve the puzzle you
need to determine the value of every missing number until the grid is complete.

In this challenge you are requried to create a function that adds missing numbers
to a Sudoku grid until the grid is complete or there are only grid positions left
that could contain more than one value. The function will take an array
(or an ordered list) of 81 numbers as input and write out a list of 81 numbers
with a value between 0 and 9. Each sublist of 9 numbers represents a row on the
grid. Value 0 represents an empty grid position.

As a bonus improve the function so that it returns all complete solutions.
This will return an array of arrays of 81 numbers, returning an empty array
if there is no solution.
 */


