package test

import main.*
import main.Sides.*
import org.junit.Test
import kotlin.test.assertEquals

class Rubix {

    private val startCube1: Cube = arrayOf("GGGGGGGGG", "RRRRRRRRR", "BBBBBBBBB", "YYYYYYYYY", "WWWWWWWWW", "OOOOOOOOO")
    private val startCube2: Cube = arrayOf("G1G2G3G4G5G6G7G8G9", "R1R2R3R4R5R6R7R8R9", "B1B2B3B4B5B6B7B8B9", "Y1Y2Y3Y4Y5Y6Y7Y8Y9", "W1W2W3W4W5W6W7W8W9", "O1O2O3O4O5O6O7O8O9")

    private val rotateTop = arrayOf("Y1Y2Y3G4G5G6G7G8G9", "B1B2B3R4R5R6R7R8R9", "G1G2G3B4B5B6B7B8B9", "R1R2R3Y4Y5Y6Y7Y8Y9", "W7W4W1W8W5W2W9W6W3", "O1O2O3O4O5O6O7O8O9")
    private val rotateBottom = arrayOf("G1G2G3G4G5G6B7B8B9", "R1R2R3R4R5R6Y7Y8Y9", "B1B2B3B4B5B6R7R8R9", "Y1Y2Y3Y4Y5Y6G7G8G9", "W1W2W3W4W5W6W7W8W9", "O7O4O1O8O5O2O9O6O3")
    private val rotateLeft = arrayOf("W1G2G3W4G5G6W7G8G9", "R1R2O3R4R5O6R7R8O9", "B7B4B1B8B5B2B9B6B3", "Y1Y2Y3Y4Y5Y6Y7Y8Y9", "R9W2W3R6W5W6R3W8W9", "O1O2G7O4O5G4O7O8G1")
    private val rotateRight = arrayOf("G1G2O7G4G5O4G7G8O1", "W9R2R3W6R5R6W3R8R9", "B1B2B3B4B5B6B7B8B9", "Y7Y4Y1Y8Y5Y2Y9Y6Y3", "W1W2G3W4W5G6W7W8G9", "R1O2O3R4O5O6R7O8O9")
    private val rotateFront = arrayOf("G7G4G1G8G5G2G9G6G3", "R1R2R3R4R5R6R7R8R9", "B1B2O9B4B5O8B7B8O7", "W7Y2Y3W8Y5Y6W9Y8Y9", "W1W2W3W4W5W6B9B6B3", "O1O2O3O4O5O6Y1Y4Y7")
    private val rotateBack = arrayOf("G1G2G3G4G5G6G7G8G9", "R7R4R1R8R5R2R9R6R3", "W3B2B3W2B5B6W1B8B9", "Y1Y2O1Y4Y5O2Y7Y8O3", "Y3Y6Y9W4W5W6W7W8W9", "B7B4B1O4O5O6O7O8O9")

    @Test
    fun `faceToSquares returns list the side values`() {
        val startFace1 = "GGGGGGGGG"
        val squares1: Squares = listOf("G", "G", "G", "G", "G", "G", "G", "G", "G")

        val startFace2 = "G1G2G3G4G5G6G7G8G9"
        val squares2: Squares = listOf("G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9")

        val chunkedFace1 = startFace1.faceToSquares()
        assertEquals(chunkedFace1, squares1)

        val chunkedFace2 = startFace2.faceToSquares()
        assertEquals(chunkedFace2, squares2)
    }

    @Test
    fun `faceToRows returns array of three lines`() {
        val startFace1 = "GGGGGGGGG"
        val lines1: Rows = arrayOf("GGG", "GGG", "GGG")

        val startFace2 = "G1G2G3G4G5G6G7G8G9"
        val lines2: Rows = arrayOf("G1G2G3", "G4G5G6", "G7G8G9")

        val chunkedFace1 = startFace1.faceToRows()
        assertEquals(true, chunkedFace1.contentEquals(lines1))

        val chunkedFace2 = startFace2.faceToRows()
        assertEquals(true, chunkedFace2.contentEquals(lines2))
    }

    @Test
    fun `squaresToFace returns string of side values`() {
        val startFace1 = "GGGGGGGGG"
        val squares1: Squares = listOf("G", "G", "G", "G", "G", "G", "G", "G", "G")

        val startFace2 = "G1G2G3G4G5G6G7G8G9"
        val squares2: Squares = listOf("G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9")

        val wholeFace1 = squares1.squaresToFace()
        assertEquals(wholeFace1, startFace1)

        val wholeFace2 = squares2.squaresToFace()
        assertEquals(wholeFace2, startFace2)
    }

    @Test
    fun `rowsToFace returns string of side values`() {
        val startFace1 = "GGGGGGGGG"
        val rows1: Rows = arrayOf("GGG", "GGG", "GGG")

        val startFace2 = "G1G2G3G4G5G6G7G8G9"
        val rows2: Rows = arrayOf("G1G2G3", "G4G5G6", "G7G8G9")

        val wholeFace1 = rows1.rowsToFace()
        assertEquals(wholeFace1, startFace1)

        val wholeFace2 = rows2.rowsToFace()
        assertEquals(wholeFace2, startFace2)
    }

    @Test
    fun `rotate should rotate the rows`() {
        val rowsBefore1: Rows = arrayOf("ABC", "DEF", "GHI")
        val rowsAfter1: Rows = arrayOf("GDA", "HEB", "IFC")

        val rowsBefore2: Rows = arrayOf("G1G2G3", "G4G5G6", "G7G8G9")
        val rowsAfter2: Rows = arrayOf("G7G4G1", "G8G5G2", "G9G6G3")

        val rowsBefore3: Rows = arrayOf("GR1GR2GR3", "GR4GR5GR6", "GR7GR8GR9")
        val rowsAfter3: Rows = arrayOf("GR7GR4GR1", "GR8GR5GR2", "GR9GR6GR3")
        
        assertEquals(true, rowsAfter1.contentEquals(rowsBefore1.rotate()))
        assertEquals(true, rowsAfter2.contentEquals(rowsBefore2.rotate()))
        assertEquals(true, rowsAfter3.contentEquals(rowsBefore3.rotate()))
    }

    @Test
    fun `getFace returns appropriate face string`() {

        assertEquals(startCube1.getFace(Front), "GGGGGGGGG")
        assertEquals(startCube1.getFace(Back), "RRRRRRRRR")
        assertEquals(startCube1.getFace(Left), "BBBBBBBBB")
        assertEquals(startCube1.getFace(Right), "YYYYYYYYY")
        assertEquals(startCube1.getFace(Top), "WWWWWWWWW")
        assertEquals(startCube1.getFace(Bottom), "OOOOOOOOO")

        assertEquals(startCube2.getFace(Top), "W1W2W3W4W5W6W7W8W9")
        assertEquals(startCube2.getFace(Bottom), "O1O2O3O4O5O6O7O8O9")
    }

    @Test
    fun `rotate returns Face rotated clockwise`() {

        val face = "W1W2W3W4W5W6W7W8W9"
        val rotatedFace = "W7W4W1W8W5W2W9W6W3"
        assertEquals(true, rotatedFace.contentEquals(face.faceToRows().rotate().rowsToFace()))

        val startTop = startCube2.getFace(Top)
        val endTop = rotateTop.getFace(Top)
        assertEquals(true, endTop.contentEquals(startTop.faceToRows().rotate().rowsToFace()))

        val startBack = startCube2.getFace(Back)
        val endBack = rotateBack.getFace(Back)
        assertEquals(true, endBack.contentEquals(startBack.faceToRows().rotate().rowsToFace()))

        val startBottom = startCube2.getFace(Bottom)
        val endBottom = rotateBottom.getFace(Bottom)
        assertEquals(true, endBottom.contentEquals(startBottom.faceToRows().rotate().rowsToFace()))
    }

    @Test
    fun `rollForward returns Cube rolled forward and rollBackward does the opposite`() {
        val startCube2Rolled = arrayOf("W1W2W3W4W5W6W7W8W9", "O1O2O3O4O5O6O7O8O9", "B7B4B1B8B5B2B9B6B3", "Y3Y6Y9Y2Y5Y8Y1Y4Y7", "R9R8R7R6R5R4R3R2R1", "G9G8G7G6G5G4G3G2G1")
        
        assertEquals(true, startCube2Rolled.contentEquals(startCube2.rollForward()))
        assertEquals(true, startCube2.contentEquals(startCube2.rollForward().rollForward().rollForward().rollForward()))
        assertEquals(true, startCube2.contentEquals(startCube2.rollForward().rollBackward()))
    }
    
    @Test
    fun `turnCube returns whole Cube turned clockwise and turnCubeCCW does the opposite`() {
        val startCube2Turned = arrayOf("Y1Y2Y3Y4Y5Y6Y7Y8Y9", "B1B2B3B4B5B6B7B8B9", "G1G2G3G4G5G6G7G8G9", "R1R2R3R4R5R6R7R8R9", "W7W4W1W8W5W2W9W6W3", "O3O6O9O2O5O8O1O4O7")
        
        assertEquals(true, startCube2Turned.contentEquals(startCube2.turnCube()))
        assertEquals(true, startCube2.contentEquals(startCube2.turnCube().turnCube().turnCube().turnCube()))
        assertEquals(true, startCube2.contentEquals(startCube2.turnCube().turnCubeCCW()))
    }
    
    @Test
    fun `rotateTopRow returns Cube with top row rotated turned clockwise and rotateTopRowCCW does the opposite`() {
        
        assertEquals(true, rotateTop.contentEquals(startCube2.rotateTopRow()))
        assertEquals(true, startCube2.contentEquals(startCube2.rotateTopRow().rotateTopRowCCW()))
        assertEquals(true, startCube2.contentEquals(rotateTop.rotateTopRowCCW()))
    }
    
    @Test
    fun `rotateCube returns Cube rotates the correct face either CW or CCW`() {
        
        assertEquals(true, rotateTop.contentEquals(rotateCube(startCube2, Top, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateTop, Top, "CCW")))

        assertEquals(true, rotateBottom.contentEquals(rotateCube(startCube2, Bottom, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateBottom, Bottom, "CCW")))

        assertEquals(true, rotateLeft.contentEquals(rotateCube(startCube2, Left, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateLeft, Left, "CCW")))

        assertEquals(true, rotateRight.contentEquals(rotateCube(startCube2, Right, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateRight, Right, "CCW")))

        assertEquals(true, rotateFront.contentEquals(rotateCube(startCube2, Front, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateFront, Front, "CCW")))

        assertEquals(true, rotateBack.contentEquals(rotateCube(startCube2, Back, "CW")))
        assertEquals(true, startCube2.contentEquals(rotateCube(rotateBack, Back, "CCW")))
        
    }

    @Test
    fun `rotateCube returns Cube rotates the correct face either CW or CCW for original examples`() {
        val start: Cube = arrayOf("GGGGGGGGG", "RRRRRRRRR", "BBBBBBBBB", "YYYYYYYYY", "WWWWWWWWW", "OOOOOOOOO")

        val top = arrayOf("YYYGGGGGG","BBBRRRRRR","GGGBBBBBB","RRRYYYYYY","WWWWWWWWW","OOOOOOOOO")
        val bottom = arrayOf("GGGGGGBBB","RRRRRRYYY","BBBBBBRRR","YYYYYYGGG","WWWWWWWWW","OOOOOOOOO")
        val left = arrayOf("WGGWGGWGG","RRORRORRO","BBBBBBBBB","YYYYYYYYY","RWWRWWRWW","OOGOOGOOG")
        val right = arrayOf("GGOGGOGGO","WRRWRRWRR","BBBBBBBBB","YYYYYYYYY","WWGWWGWWG","ROOROOROO")
        val front = arrayOf("GGGGGGGGG","RRRRRRRRR","BBOBBOBBO","WYYWYYWYY","WWWWWWBBB","OOOOOOYYY")
        val back = arrayOf("GGGGGGGGG","RRRRRRRRR","WBBWBBWBB","YYOYYOYYO","YYYWWWWWW","BBBOOOOOO")
        
        assertEquals(true, top.contentEquals(rotateCube(start, Top, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(top, Top, "CCW")))

        assertEquals(true, bottom.contentEquals(rotateCube(start, Bottom, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(bottom, Bottom, "CCW")))

        assertEquals(true, left.contentEquals(rotateCube(start, Left, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(left, Left, "CCW")))

        assertEquals(true, right.contentEquals(rotateCube(start, Right, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(right, Right, "CCW")))

        assertEquals(true, front.contentEquals(rotateCube(start, Front, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(front, Front, "CCW")))

        assertEquals(true, back.contentEquals(rotateCube(start, Back, "CW")))
        assertEquals(true, start.contentEquals(rotateCube(back, Back, "CCW")))
    }
    
}
