package test

import main.Challenge5
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class Challenge5Test {

    val underTest = Challenge5()

    @Test
    fun `should confirm if there are four in a row`() {
        assertTrue { underTest.findFourConnected("..rrrr.", "r") }
        assertTrue { underTest.findFourConnected("..rRrr.", "r") }
        assertTrue { underTest.findFourConnected("RRrr...", "r") }
        assertTrue { underTest.findFourConnected("....RRRR....", "r") }
        assertTrue { underTest.findFourConnected("..rrrR.", "r") }
        assertTrue { underTest.findFourConnected("..yYyy.", "y") }
        assertTrue { underTest.findFourConnected("YYyy...", "y") }
        assertTrue { underTest.findFourConnected(".YYYY.....", "y") }
        assertTrue { underTest.findFourConnected("......yyyY.", "y") }
    }

    @Test
    fun `should not confirm if there are not four in a row`() {
        assertFalse { underTest.findFourConnected(".r.rrr.", "r") }
        assertFalse { underTest.findFourConnected("Rr.rrr.Rr", "r") }
        assertFalse { underTest.findFourConnected(".Rrrr.", "y") }
        assertFalse { underTest.findFourConnected(".Rrrr.", "y") }
    }

    @Test
    fun `should confirm if board is full`() {
        assertTrue { underTest.boardIsFull(listOf("ryYrrr", "ryyrrr", "rrryrr")) }
        assertFalse { underTest.boardIsFull(listOf("ryYrrr", "ryyrrr", "rr.yrr")) }
    }

    @Test
    fun `should confirm red plays next`() {
        assertTrue { underTest.redPlaysNext(listOf("...ry..", "...ryy.", "..yrrrY")) }
        assertFalse { underTest.redPlaysNext(listOf("...Ry..", "..ryy.", "..yrrry")) }
    }

    @Test
    fun `should return all downstrings`() {
        assertEquals(
            listOf("nqade", "orbef", "pscfg", "qtdgh", "ruehi", "svfij", "twgjk"),
            underTest.parseDownStrings(
                listOf(
                    "nopqrst",
                    "qrstuvw",
                    "abcdefg",
                    "defghij",
                    "efghijk"),6,4))
    }

    @Test
    fun `should return diagonals`() {
        assertEquals(
            listOf("nrcgi", "osdhj", "pteik", "qufj", "qbfh", "eectr", "ffdus", "ggevt", "hhfw", "dbsq"),
            underTest.parseDiagonalStrings(
                listOf(
                    "nopqrst",
                    "qrstuvw",
                    "abcdefg",
                    "defghij",
                    "efghijk"),6,4))
    }

    @Test
    fun `should return all strings`() {
        assertEquals(
            listOf(
                "nopqrst", "qrstuvw", "abcdefg", "defghij", "efghijk", "nqade", "orbef", "pscfg", "qtdgh", "ruehi", "svfij", "twgjk", "nrcgi", "osdhj", "pteik", "qufj", "qbfh", "eectr", "ffdus", "ggevt", "hhfw", "dbsq"),
            underTest.getAllStrings(
                listOf(
                    "nopqrst",
                    "qrstuvw",
                    "abcdefg",
                    "defghij",
                    "efghijk")))
    }

    @Test
    fun `should confirm the correct message for each grid`() {
        assertEquals(
        "Red Wins",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                ".R.....",
                ".r.....",
                ".ry....",
                ".ryyy..")))


        assertEquals(
        "Red Wins",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                ".......",
                ".yy....",
                ".rrRr..",
                ".ryyy..")))

        assertEquals(
        "Red Wins",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                "....r..",
                "...ry..",
                "..Ryr..",
                ".ryyyr.")))

        assertEquals(
        "Yellow Wins",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                "...y...",
                "...ry..",
                "...ryy.",
                "...rrrY")))

        assertEquals(
        "Red plays next",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                ".......",
                "...ry..",
                "...ryy.",
                "..yrrrY")))

        assertEquals(
        "Yellow plays next",
        underTest.getGridStatus(
            listOf(
                ".......",
                ".......",
                ".......",
                "...ry..",
                "..Rryy.",
                "..yrrry")))

        assertEquals(
        "Draw",
        underTest.getGridStatus(
            listOf(
                "ryrYryr",
                "yryryrr",
                "ryryryy",
                "ryrrryr",
                "rrryryy",
                "yryryyy")))

    }
}