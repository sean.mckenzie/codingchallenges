# Update to Gitlab.com

# Coding Challenge 26

Sean Mckenzie

Rubix:

Clone the repo, and run the tests in Challenge26Test, class Rubix


# Coding Challenge 21

Sean Mckenzie

Sudoku:

Clone the repo, and run the tests in Challenge21Test, class Sudoku



# Coding Challenge 19

Sean Mckenzie

Shopping Trolley:

Clone the repo, and run the tests in Challenge19Test, class TrolleyTest



# Coding Challenge 18

Sean Mckenzie

Node & Linked List functions:
- getDescription(): to pretty print the linked list
- addToList(): to add a value to the list
- convertLinkedListStringToInt(): to convert a linked list of strings to a linked list of integers
- reverseLinkedList(): to take a linked list and reverse it 


Clone the repo, and run the tests in Challenge18Test, class NodeTest



# Coding Challenge 17

Sean Mckenzie

Pontoon 

Function checkPontoonWinner(a,b) accepts two arrays, first for a player, second for the dealer, and returns the winner (ie. “The player” or “The dealer”) and a description of the winning hand.

Each card will be represented by two characters, the first character representing “rank” and the second character representing “suit”. Rank can be any of 2,3,4,5,6,7,8,9,T,J,Q,K,A (T=Ten, J=Jack, Q=Queen, K=King, A=Ace). Suit can be any of C,D,H,S (Clubs, Diamonds, Hearts, Spades). So TH will be 10 of Hearts.

Winning hands (ordered by highest scoring) are:

    Pontoon - one card has a rank of ace and the other card is a picture card, i.e. has rank of J, Q or K.

    Five card Trick - there are five cards with a total value of 21 or less. Ace has value of one and picture cards have value of 10. The total value of the cards doesn’t matter as long as it is 21 or less.

    Highest total of 21 or less. Ace can have a value of one or eleven. e.g. two kings and an ace would have a value of 21 but an ace and a 3 would have a value of 14.

    Hands totalling more than 21 are “bust” and worth nothing

The player must have a higher scoring hand than the dealer to win, otherwise the dealer wins.

Clone the repo, and run the tests in Challenge17Test, class PontoonTest


# Coding Challenge 16

Sean Mckenzie

Roman Numerals

Function addTwoRomanNumerals(a,b) accepts two strings containing roman numerals and returns as string containing a roman numeral which is the sum of the two roman numerals


Clone the repo, and run the tests in Challenge15Test, class RomanNumeralTest




# Coding Challenge 14

Sean Mckenzie

Clone the repo, and run the tests in Challenge14Test, class myFoldTest

Part one is function called myFold which will take the following as parameters: a list or array of Integers, a string and a function of the type (String, Int) -> String. myFold will return a string

Part 2 - added version of function to handle different data types

# Coding Challenge 13

Sean Mckenzie

Clone the repo, and run the tests in Challenge13Test

Function calculateJourneyTime(fullStoreList) will return the time in seconds for the entire journey, passing in a String of the shop data



# Coding Challenge 5 - Connect 4

Sean Mckenzie

Clone the repo, and run the tests in Challenge5Test